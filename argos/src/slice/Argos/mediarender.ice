// -*- mode: c++; coding: utf-8 -*-

#include <Hesperia/AVStreams.ice>
#include <Hesperia/PropertyService.ice>

module Media {
  interface Render extends
	AVStreams::MMDevice,
	/* The interface PropertySetDef is not necessary because MMDevice
	   extends it. It is kept because clarity between the MediaServer
	   properties and the MMDevice ones */
    PropertyService::PropertySetDef {};
};
