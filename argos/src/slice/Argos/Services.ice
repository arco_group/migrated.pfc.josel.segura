// -*- mode: c++; coding: utf-8 -*-

#include <Hesperia/DUO.ice>
#include <Hesperia/AVStreams.ice>
#include <Hesperia/PropertyService.ice>

module Media {
  interface Source extends
    DUO::Container::R,
    AVStreams::MMDevice {};

  interface Sink extends
	AVStreams::MMDevice {};
};
