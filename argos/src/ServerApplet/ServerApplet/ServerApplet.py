#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os

import logging
from loggingx import ColorFormatter, NotifyHandler

logging.basicConfig(level=logging.NOTSET, # all the logs to a file
                    format='%(asctime)s %(levelname)s %(message)s',
                    filename='/tmp/serverapplet_%s.log' % os.getpid(),
                    filemode='w')

import gobject
import pygtk
pygtk.require('2.0')
import gtk
import gnomeapplet

# FIXME!
logging.warning('On devel: adding the devel path to sys.path')
sys.path.append('/home/joseluis/uni/pfc/argos/src/ServerApplet/ServerApplet')

from ServerCore import ServerCore, ServerNotFoundException

class ServerApplet(gnomeapplet.Applet):
    '''This class inherits from Applet. It contains a gtk.Button with
    an stock image (media-play or media-stop). When it shows the play
    button and it is pressed, a Ice object is added to a object
    adapter and a thread announcing it through an IceStorm topic is
    launched'''

    menu = '''
<popup name="button3">
  <menuitem name="Item 2" verb="Preferences" label="Preferences"
      pixtype="stock" pixname="gtk-preferences"/>

  <menuitem name="Item 1" verb="About" label="About..."
      pixtype="stock" pixname="gtk-about"/>
</popup>
'''
    def __init__(self):
        '''Only call to the inherited class constructor. When this
        class is used as an applet, the constructor is not called.
        '''

        logging.debug('Server Applet created using the constructor')
        gnomeapplet.Applet.__init__(self)
        self.args = sys.argv

    def init(self):
        '''This method should be the constructor. It has the
        initialization of the contained objects and the status of the
        applet '''

        logging.debug('Initializing Server Applet')
        self._uLogger = logging.getLogger('Server Applet')
        self._uLogger.addHandler(NotifyHandler())

        self._running = False

        self._image_publishing = gtk.Image()
        self._image_publish = gtk.Image()

        self._image_publishing.set_from_stock(gtk.STOCK_NO,
                                           gtk.ICON_SIZE_BUTTON)

        self._image_publish.set_from_stock(gtk.STOCK_YES,
                                              gtk.ICON_SIZE_BUTTON)

        logging.debug('Creating the button and adding the image')
        button = gtk.Button()

        button.add(self._image_publishing)
        button.connect('clicked', self._on_click_button)
        button.connect('button-press-event', self._on_button_press)

        self.setup_menu(self.menu, [("About", self._about_cb),
                                    ("Preferences", self._pref_cb)])

        self.add(button)
        self.show_all()

    def quit(self):
        self.hide()
        ServerCore().quit()

    def _about_cb(self, event, data=None):
        'Shows an "about" window'
        logging.debug('Loading about dialog')

        about = gtk.AboutDialog()
        about.set_name("Argos Server Applet")
        about.set_version('0.5')
        about.run()
        about.destroy()

    def _pref_cb(self, event, data=None):
        'Open the preferences dialog'
        logging.debug('Loading preferences dialog')

        builder = gtk.Builder()
        builder.add_from_file('res/preferences.xml')
        dialog = builder.get_object('preferences_dialog')

        entries = (builder.get_object('entry_locator'),
                   builder.get_object('entry_service'))


        config_values = ServerCore().getConfigFile()
        entries[0].set_text(config_values[0])
        entries[1].set_text(config_values[1])

        dialog.connect('response', self._pref_response, entries)
        dialog.run()
        dialog.destroy()

    def _pref_response(self, dialog, response_id, user_param):
        if response_id == gtk.RESPONSE_OK:
            # Need to store the values for proxies
            logging.info('Default Locator: %s' % user_param[0].get_text())
            logging.info('Service Proxy: %s' % user_param[1].get_text())

            try:
                ServerCore().createConfigFile(user_param[0].get_text(),
                                              user_param[1].get_text())

            except ServerNotFoundException, exc:
                self._uLogger.error("Server Applet\n" + str(exc))

    def _on_button_press(self, widget, event, data=None):
        '''Callbcack to allow the applet capture the right button click
        to show the menu
        '''
        # this allows dragging applet around panel and friends
        if event.button != 1:
            widget.stop_emission('button_press_event')
        return False


    def _on_click_button(self, button):
        "Callback of the applet's button"

        if not self._running:
            try:
                ServerCore().register()
                button.set_image(self._image_publish)
                self._running = True
                logging.debug('Server Service started')
                self._uLogger.info('Registered\nServer Applet registered successfully')

            except ServerNotFoundException, exc:
                self._uLogger.error("Server Applet\n" + str(exc))

        else:
            ServerCore().unregister()
            button.set_image(self._image_publishing)
            self._running = False
            logging.debug('Server Service stopped')
            self._uLogger.info('Unregistered\nServer Applet unregistered successfully')


def server_applet_factory(applet, iid):
    'Bonobo factory function'
    applet.init()
    return True

gtk.gdk.threads_init()

if '--window' not in sys.argv:
    logging.info('Starting ServerAppler as applet (normal mode)')

    # registering types for apple execution
    gobject.type_register(ServerApplet)
    gnomeapplet.bonobo_factory("OAFIID:GNOME_ArgosServerApplet_Factory",
                               ServerApplet.__gtype__,
                               "media server applet", "0", server_applet_factory)

else:
    # configure StreamHandler to get debug messages
    try:
        console = logging.StreamHandler()
        console.setFormatter(ColorFormatter('%(levelname)s [%(name)s]: %(message)s'))
        logging.getLogger().addHandler(console)

        logging.info('Starting ServerApplet as window (debug mode)')

        main_window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        main_window.set_decorated(False)
        main_window.set_size_request(320, 320)
        main_window.connect("destroy", gtk.main_quit)

        app = ServerApplet()
        server_applet_factory(app, None)
        app.reparent(main_window)
        main_window.show()

        gtk.main()


        app.quit()
        sys.exit(0)

    except KeyboardInterrupt:
        app.quit()
        sys.exit(0)
