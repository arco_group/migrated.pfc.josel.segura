# -*- coding: utf-8 -*-

import os
import logging
import subprocess
import time

import gst.rtspserver
import netifaces
import Ice
import pyarco.Net

ARGOS_SLICE_DIR = '/usr/local/share/argos/slice'

def loadSlice(cmd_line):
    Ice.loadSlice('--all {0}'.format(cmd_line))

loadSlice('-I/usr/share/Ice/slice -I/usr/share/hesperia/slice'
          ' {0}/Argos/MMDevCreatorService.ice'.format(ARGOS_SLICE_DIR))
loadSlice('-I/usr/share/Ice/slice -I/usr/share/hesperia/slice'
          ' /usr/share/hesperia/slice/Hesperia/PropertyService.ice')

import Argos
import PropertyService
import P

def getIPs():
    ifaces = [ x for x in netifaces.interfaces()
               if not x == 'lo' and
               netifaces.ifaddresses(x).has_key(netifaces.AF_INET)]

    retval = []

    for iface in ifaces:
        retval += [ x['addr'] for x in netifaces.ifaddresses(iface)[netifaces.AF_INET] ]

    return retval

class GstRTSPServer(Argos.DelegatedApp):
    def __init__(self):
        self._l = logging.getLogger('GST')

        self.identity = '{0}_serverapplet'.format(os.environ['USER'])

        self._attached = False
        self._rtsp = gst.rtspserver.Server()
        self._factory = gst.rtspserver.MediaFactory()

        port = str(pyarco.Net.getFreePort())
        self._rtsp.set_property('service', port)

        path = '/screen'
        launch = '( ximagesrc ! ffmpegcolorspace ! ffenc_mpeg4 ! ' \
            'rtpmp4vpay name=pay0 pt=96 )'
        
        self._factory.set_launch(launch)
        mapping = self._rtsp.get_media_mapping()
        mapping.add_factory(path, self._factory)
        
        self._config = PropertyService.Property()
        self._config.propertyName = 'endpoint'
        self._config.propertyValue = \
            P.StringT('rtsp://{0}:{1}{2}'.format(getIPs()[0], port, path))

    # Ice interface
    def configure(self, configuration, context=None):
        return

    def getConfiguration(self, context=None):
        return self._config

    def play(self, context):
        self._l.info('Play!')
        
        if not self._attached:
            self._rtsp.attach()
            self._attached = True

    def stop(self, context=None):
        self._l.info('Stop')
        
