# -*- coding: utf-8; mode: python -*-

import os
import logging

import gconf
import Ice
from pyarco.Pattern import Singleton

from GstreamerServer import GstRTSPServer

ARGOS_SLICE_DIR = '../../slice'

def loadSlice(cmd_line):
    Ice.loadSlice('--all %s' % cmd_line)

loadSlice('-I/usr/share/Ice/slice -I/usr/share/hesperia/slice'
          ' %s/Argos/MMDevCreatorService.ice' % ARGOS_SLICE_DIR)

import Argos

class ServerNotFoundException(Exception):
    def __init__(self, service_name, str_proxy):
        self.__name      = service_name
        self.__str_proxy = str_proxy

    def __str__(self):
        return "Can't connect to %s service on \"%s\"" % (self.__name,
                                                      self.__str_proxy)

class ServerCore:
    __metaclass__ = Singleton

    CONFIG_FILE = os.path.join(os.environ['HOME'], '.argos',
                               'serverapplet_ice.cfg')

    def __init__(self):
        # logging initialization
        self._logger  = logging.getLogger('Ice')

        self._ic      = None
        self._adapter = None
        self._mmdc    = None

        self._registered = False

        self._logger.debug('Server Core initialized')

        self._server = GstRTSPServer()

        self._loadConfig()

    def _loadConfig(self):
        if self._ic:
            self._logger.debug('Already initialized. Cleaning')

            if self._registered:
                self._mmdc.delServer(self._serverPrx)

            self._adapter.deactivate()
            self._ic.shutdown()

        self._logger.debug('Initializing communicator... and so on')
        self._ic = Ice.initialize(['--Ice.Config={0}'.format(
                    ServerCore.CONFIG_FILE)])

        self._adapter = self._ic.createObjectAdapterWithEndpoints( \
            'ServerApplet', 'default')

        self._adapter.activate()

        self._serverPrx = Argos.DelegatedAppPrx.checkedCast( \
            self._adapter.add(self._server,
                              self._ic.stringToIdentity( \
                    self._server.identity)))

        self._logger.debug('Servant added to object adapter')

        # on-line: get property -> convert to proxy -> casting to the
        # desired interface
        mmdc_str = \
            self._ic.getProperties().getProperty('Argos.MMDeviceCreator')

        self._mmdc = self._ic.stringToProxy(mmdc_str)


    def createConfigFile(self, def_locator, service):
        'Create the Ice configuration file'

        self._logger.debug('Writing configuration file')
        f = open(ServerCore.CONFIG_FILE, 'w')

        f.write('Ice.Default.Locator = %s\n' % def_locator)
        f.write('Argos.MMDeviceCreator = %s\n' % service)

        f.close()

        self._logger.info('Configuration file saved at %s' %
                           ServerCore.CONFIG_FILE)

        self._loadConfig()

    def getConfigFile(self):
        assert self._ic != None
        locator = self._ic.getProperties().getProperty('Ice.Default.Locator')
        mmcs    = self._ic.getProperties().getProperty('Argos.MMDeviceCreator')

        return locator, mmcs

    def register(self):
        self._logger.debug('Registering the servant on MMDeviceCreatorService')

        try:
            self._mmdc = Argos.MMDeviceCreatorPrx.checkedCast(self._mmdc)
            self._mmdc.addServer(self._serverPrx, {})
            self._registered = True

        except Ice.ConnectionRefusedException, exc:
            self._logger.error(str(exc))
            raise ServerNotFoundException('MMDeviceCreator', str(self._mmdc))

    def unregister(self):
        if self._registered:
            self._logger.debug('Unregistering')
            self._mmdc.delServer(self._serverPrx)
            self._server.stop()
            self._registered = False


    def quit(self):
        self._logger.debug('Stopping server, unregistering from service and '
                            'deactivating the object adapter')
        #self.__render.stop()
        self.unregister()
        self._adapter.deactivate()
        self._ic.shutdown()
