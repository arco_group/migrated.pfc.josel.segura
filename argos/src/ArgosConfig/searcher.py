# -*- coding: utf-8; mode: python -*-

import threading
import time
import logging

import Ice
import IceGrid
from pyarco.Pattern import Observable
from loggingx import ColorFormatter

from ice_utils import IceCore
import widgets
import globs

try:
    Ice.loadSlice('--all -I%s -I%s %s/Hesperia/AVStreams.ice' %
                  (Ice.getSliceDir(), globs.HESPERIA_SLICE_DIR,
                   globs.HESPERIA_SLICE_DIR))

except Exception, e:
    logging.info('Slice load failed. See the error log')
    logging.fatal(e)

import AVStreams

class Searcher(threading.Thread, Observable):
    'This class search for AVStreams::MMDevice connected to the IceGrid/Registry'

    def __init__(self):
        threading.Thread.__init__(self)
        Observable.__init__(self, topics=['search'])

        self._l = logging.getLogger('Searcher')

        # threading.Thread: mark this thread as daemon
        # http://docs.python.org/library/threading.html#thread-objects
        self.daemon = True

        self.__core = IceCore()

        prop = self.__core.getProperties()

        self._interval = prop.getPropertyAsIntWithDefault('finder.interval', 60)

        self.__model = widgets.Store(str, object)
        self.__conn = []

        obj = self.__core.stringToProxy('IceGrid/Query')
        self.__queryPrx = IceGrid.QueryPrx.checkedCast(obj)

        self._l.info('Retrieved IceGrid/Query: %s' % self.__queryPrx)


    def getModel(self):
        return self.__model

    def run(self):
        while True:
            self._l.debug('Searching for MMDevices')

            found = \
                self.__queryPrx.findAllObjectsByType(AVStreams.MMDevice.ice_staticId())

            for obj in found:
                try:
                    identity = obj.ice_getIdentity().name
                    ice_ids  = obj.ice_ids()

                    mmdev = AVStreams.MMDevicePrx.checkedCast(obj)

                    self.__model.append([identity, mmdev])

                except Ice.NoEndpointException:
                    self._l.info('The object {0} is not accesible'.format(obj))

                except Ice.ObjectNotExistException:
                    self._l.info('The object {0} is not accesible'.format(
                            obj))

            self.notify('search', found)
            time.sleep(self._interval)
