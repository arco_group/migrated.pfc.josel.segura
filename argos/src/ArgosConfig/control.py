# -*- coding: utf-8; mode: python -*-

import logging

import Ice
from pyarco.Pattern import Singleton, Observable

from ice_utils import IceCore
import widgets

Ice.loadSlice("--all -I%s -I%s %s/AVStreams.ice" % \
                  (Ice.getSliceDir(), '/usr/share/hesperia/slice',
                  '/usr/share/hesperia/slice/Hesperia'))

import AVStreams

class StreamCtrlI(AVStreams.StreamCtrl):
    def __init__(self):
        AVStreams.StreamCtrl.__init__(self)
        self._l = logging.getLogger('StrCtrl')

        self.prx = AVStreams.StreamCtrlPrx.checkedCast(
            IceCore().addWithUUID(self))

        self._sEpA = None
        self._sEpB = None

    def start(self, flow_spec, context=None):
        assert self._sEpA and self._sEpB

        self._sEpA.start(flow_spec)
        self._l.debug('StreamEndPointA started')
        self._sEpB.start(flow_spec)
        self._l.debug('StreamEndPointB started')

    def stop(self, flow_spec, context=None):
        assert self._sEpA and self._sEpB

        self._sEpA.stop(flow_spec)
        self._l.debug('StreamEndPointA stopped')
        self._sEpB.stop(flow_spec)
        self._l.debug('StreamEndPointB stopped')

    def bindDevs(self, mmdev_a, mmdev_b, req_qos, flow_spec, context=None):
        self._l.info('Binding devs')
        assert mmdev_a and mmdev_b

        self._sEpA, vdev_a, retval, name_a, got_qos = \
            mmdev_a.createA(self.prx, req_qos, "", flow_spec)
        self._l.info('Stream Endpoint A retrieved: %s' % self._sEpA)

        self._sEpB, vdev_b, retval, name_b, got_qos = \
            mmdev_b.createB(self.prx, req_qos, "", flow_spec)
        self._l.info('Stream Endpoint B retrieved: %s' % self._sEpB)

        # unicast connection
        retval_a, got_qos = vdev_a.setPeer(self.prx, vdev_b,
                                           req_qos, flow_spec)
        self._l.debug('VDevA -> setPeer done')

        retval_b, got_qos = vdev_b.setPeer(self.prx, vdev_a,
                                           req_qos, flow_spec)
        self._l.debug('VDevB -> setPeer done')
        self._l.info('Binded!')

        return retval_a and retval_b, got_qos

class StreamCtrlIMCast(AVStreams.StreamCtrl):
    def __init__(self):
        AVStreams.StreamCtrl.__init__(self)

        self._l = logging.getLogger('StrCMul')
        self.prx = AVStreams.StreamCtrlPrx.checkedCast(
            IceCore().addWithUUID(self))

        self.__mcast = None
        self._sEpA = None
        self._sEpsB = []

        # Multicast source
        mcast = MCastConfigIfI()

        self._l.info('Created MCastConfigIf')

        self.__mcast = AVStreams.MCastConfigIfPrx.checkedCast(
            IceCore().addWithUUID(mcast))

        self._l.debug('MCastConfigIf {0} created'.format(self.__mcast))
        

    def bindDevs(self, mmdev_a, mmdev_b, req_qos, flow_spec, context=None):
        assert not all([mmdev_a, mmdev_b]) and any([mmdev_a, mmdev_b])

        if mmdev_a != None:
            return self._bind_source(mmdev_a, req_qos, flow_spec), []

        elif mmdev_b != None:
            return self._bind_sink(mmdev_b, req_qos, flow_spec), []

        else: # imposible
            return False, None
            
        

    def _bind_source(self, source, req_qos, flow_spec):
        self._l.info('Binding a multicast source')

        # Create the StrEPA and VDevA
        self._sEpA, vdev_a, retval, name_a, got_qos = \
            source.createA(self.prx, req_qos, "", flow_spec)

        retval = vdev_a.setMCastPeer(self.prx, self.__mcast,
                                     req_qos, flow_spec)
        return retval

    def _bind_sink(self, sink, req_qos, flow_spec):
        if self.__mcast != None: # Somebody connect a multicast
            self._l.info('Binding a multicast sink to %s' %
                         self.__mcast.ice_getIdentity())
            
            sEpB, vdev_b, retval, name_a, got_qos = \
                sink.createB(self.prx, req_qos, "", flow_spec)
            
            self._sEpsB.append(sEpB)
            
            # source before
            retval, got_qos = self.__mcast.setPeer(vdev_b,
                                                   req_qos, flow_spec)
            
            if retval:
                return self._sEpA.connectLeaf(sEpB, req_qos,
                                              flow_spec)

            else:
                return retval, got_qos

        else:
            self._l.warning('Trying to connect an inexistent multicast source')
            return False, None


class MCastConfigIfI(AVStreams.MCastConfigIf):
    def __init__(self):
        self._l = logging.getLogger('Cotrol::McastConfig')

        self._prop = None

    def setPeer(self, peer, reqQos, flowSpec, context):
        gotQos = []
        retval = False

        if peer.ice_isA('::AVStreams::VDev'):
            vdev = AVStreams.VDevPrx.uncheckedCast(peer)
            
        else:
            self._l.debug("It's not a VDev!")
            return False, gotQos

        if self._prop:
            vdev.configure(self._prop)
            retval = True

        return retval, gotQos
            


    def configure(self, configuration, context):
        self._l.info('Configuration received')

        self._prop = configuration
        self._l.debug(self._prop)


class Control(object, Observable):
    __metaclass__ = Singleton

    def __init__(self):
        self._l = logging.getLogger('Control')
        Observable.__init__(self, topics=['added', 'deleted'], logger=self._l)

        # FIXME: Error in pyarco.Pattern.Observable logger.propagate=0
        self._l.propagate = 1
        self._active = {}
        self.multicast_model = widgets.Store(str, object)

    def connect_unicast(self, aParty, bParty):
        strCtrl = StreamCtrlI()
        
        strCtrlPrx = AVStreams.StreamCtrlPrx.checkedCast(
            IceCore().addWithUUID(strCtrl))
        retval, got_qos = strCtrlPrx.bindDevs(aParty, bParty, [], [])

        self._active[strCtrlPrx.ice_getIdentity()] = strCtrlPrx
        self.notify('added', (strCtrlPrx, aParty, bParty))

    def create_multi(self, aParty):
        strCtrl = StreamCtrlIMCast()

        strCtrlPrx = AVStreams.StreamCtrlPrx.checkedCast(
            IceCore().addWithUUID(strCtrl))

        retval, got_qos = strCtrlPrx.bindDevs(aParty, None, [], [])

        if retval:
            self._active[strCtrlPrx.ice_getIdentity()] = strCtrlPrx
            self.multicast_model.append([aParty.ice_getIdentity().name, strCtrlPrx])
            self.notify('added', (strCtrlPrx, aParty, None))

    def connect_multi(self, streamCtrl, bParty):
        retval, got_qos = streamCtrl.bindDevs(None, bParty, [], [])
        
        if retval:
            self._active[streamCtrl.ice_getIdentity()] = streamCtrl
            self.notify('added', (streamCtrl, None, bParty))
        

        
