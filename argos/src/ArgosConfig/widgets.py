# -*- coding: utf-8; mode: python -*-

import logging

import gtk
import Ice

logger = logging.getLogger('UI')

class BuilderWrapper(object):
    '''Wrapper to use gtk.Builder from your class. The GTK defined
    objects on Builder can be referenced using the "b_" preffix.
    Don't forget to initialize this class using its constructor
    __init__
    '''
    def __init__(self, ui_file):
        '''Constructor:
        \tui_file: the file with the UI description in Builder format
        '''
        self.__xml = gtk.Builder()
        self.__xml.add_from_file(ui_file)

        self.get_object = self.__xml.get_object

    def __getattr__(self, name):
        try:
            return object.__getattribute__(self, name)

        except AttributeError, e:
            if not name.startswith('b_'): raise

            try:
                retval = self.__xml.get_object(name[2:])
            except AttributeError:
                raise AttributeError('You must call '
                                     'BuilderWrapper.__init__ on your'
                                     ' derived class')

            if not retval:
                raise AttributeError('There is no widget called %s',
                                     name[2:])

            setattr(self, name, retval)
            return retval

class Store(gtk.ListStore):
    class InvalidRowException(Exception): pass
    class DupRowOnModelException(Exception): pass

    '''A class derived from gtk.ListStore that doesn't allo two equals
    rows on the model
    '''

    def __init__(self, *types):
        gtk.ListStore.__init__(self, *types)
        self.__objs = []


    def append(self, row=None):
        logger.info('Append: %s' % row)

        if row[1] in self.__objs:
            return

        self.__objs.append(row[1])
        gtk.ListStore.append(self, row)

    def prepend(self, row=None):

        if row[1] in self.__objs:
            return


    def remove(self, it):
        obj = self.get_value(it, 1)
        self.__objs.remove(obj)
        gtk.ListStore.remove(it)

    def insert(self, position, row=None):
        raise NotImplementedError()

    def insert_before(self, sibling, row=None):
        raise NotImplementedError()

    def insert_after(self, sibling, row=None):
        raise NotImplementedError()

class ConnectionRow(gtk.HBox):
    'Class that defines a row on the control tab of the UI'

    def __init__(self, streamControl, aParty, bParty):
        gtk.HBox.__init__(self)

        self.__strCtrl = streamControl
        self.__status = False #True if playing, False stopped

        if aParty:
            self.pack_start(gtk.Label(str(aParty)))
        else:
            self.pack_start(gtk.Label('Multicast'))

        self.pack_start(gtk.Arrow(gtk.ARROW_RIGHT, gtk.SHADOW_NONE))

        if bParty:
            self.pack_start(gtk.Label(str(bParty)))
        else:
            self.pack_start(gtk.Label('Multicast'))

        self.pack_start(gtk.VSeparator(), expand=False)

        play_img = gtk.Image()
        play_img.set_from_stock(gtk.STOCK_MEDIA_PLAY, gtk.ICON_SIZE_BUTTON)
        self.__play = gtk.Button()
        self.__play.add(play_img)
        self.pack_start(self.__play, expand=False)
        self.__play.connect('clicked', self.onPlay)

        stop_img = gtk.Image()
        stop_img.set_from_stock(gtk.STOCK_MEDIA_STOP, gtk.ICON_SIZE_BUTTON)
        self.__stop = gtk.Button()
        self.__stop.add(stop_img)
        self.__stop.connect('clicked', self.onStop)
        self.__stop.set_sensitive(False)
        self.pack_start(self.__stop, expand=False)

        delete_img = gtk.Image()
        delete_img.set_from_stock(gtk.STOCK_DELETE, gtk.ICON_SIZE_BUTTON)
        self.__delete = gtk.Button()
        self.__delete.add(delete_img)
        self.__delete.connect('clicked', self._onDelete)
        self.pack_start(self.__delete, expand=False)

    def onPlay(self, button):
        button.set_sensitive(False)
        self.__stop.set_sensitive(True)
        self.__strCtrl.start([])

    def onStop(self, button):
        button.set_sensitive(False)
        self.__play.set_sensitive(True)
        self.__strCtrl.stop([])


    def _onDelete(self, button):
        self.get_parent().remove(self)
        # FIXME: Is needed some work wirth a StreamCtrl to "unbind" MMDevices?


