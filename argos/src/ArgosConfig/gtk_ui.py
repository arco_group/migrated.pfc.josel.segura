#!/usr/bin/env python2.6
# -*- coding: utf-8; mode: python -*-

import logging

import gtk
import gtk.glade
from loggingx import NotifyHandler

import widgets
from control import Control

class MainWindow(widgets.BuilderWrapper):
    def __init__(self, searcher):
        widgets.BuilderWrapper.__init__(self, 'res/gui.glade')

        self._l = logging.getLogger('UI')
        self._l.info('Launching UI')
        self._notify = logging.getLogger('Argos Config')
        self._notify.addHandler(NotifyHandler())

        self.b_root.connect('destroy', gtk.main_quit)
        
        self._mmdev_model = searcher.getModel()
        
        unicast_tab = UnicastTab()
        multicast_tab = MulticastTab()
        active = ActiveTab()
        
        Control().attach(active.new_active, 'added')
        
        unicast_tab.set_model(self._mmdev_model)
        multicast_tab.set_models(self._mmdev_model, Control().multicast_model)

        self.b_scroll_unicast.add_with_viewport(unicast_tab.get_content())
        self.b_scroll_multi.add_with_viewport(multicast_tab.get_content())
        self.b_scroll_active.add_with_viewport(active.get_content())

        self.b_root.show_all()

class UnicastTab(widgets.BuilderWrapper):
    def __init__(self):
        widgets.BuilderWrapper.__init__(self, 'res/unicast.glade')

        self._l = logging.getLogger('Uni_UI')

        self.b_source_tv.connect('cursor-changed', self._check_selections)
        self.b_sink_tv.connect('cursor-changed', self._check_selections)

        tvc = gtk.TreeViewColumn('Name')
        cell = gtk.CellRendererText()
        tvc.pack_start(cell)
        tvc.add_attribute(cell, 'text', 0)

        self.b_source_tv.append_column(tvc)

        tvc = gtk.TreeViewColumn('Name')
        cell = gtk.CellRendererText()
        tvc.pack_start(cell)
        tvc.add_attribute(cell, 'text', 0)

        self.b_sink_tv.append_column(tvc)

        self.b_connect_btn.connect('clicked', self._unicast_connect)

        self._l.info('Unicast UI created')

    def get_content(self):
        return self.b_root

    def set_model(self, model):
        self._l.debug('Model added')
        # filtering the model to have two different views
        self._sources_model = model.filter_new()
        self._sources_model.set_visible_func(self._model_filter_func, '::Media::Source')

        self._sinks_model   = model.filter_new()
        self._sinks_model.set_visible_func(self._model_filter_func, '::Media::Sink')

        self.b_source_tv.set_model(self._sources_model)
        self.b_sink_tv.set_model(self._sinks_model)

      
    def _model_filter_func(self, model, iterator, data):
        mmdev = model.get_value(iterator, 1)
        if not mmdev: return False
        
        return mmdev.ice_isA(data)

    def _check_selections(self, treeview):
        source_selected = self.b_source_tv.get_selection().count_selected_rows() > 0
        sink_selected   = self.b_sink_tv.get_selection().count_selected_rows() > 0

        self.b_connect_btn.set_sensitive(source_selected and sink_selected)

    def _unicast_connect(self, button):
        self._l.info('Connecting...')

        model, it = self.b_source_tv.get_selection().get_selected()
        if not it:
            self._l.info('Bad source selection, try again')
            return

        source = model[it][1]

        model, it = self.b_sink_tv.get_selection().get_selected()
        if not it:
            self._l.info('Bad sink selection, try again')
            return

        sink = model[it][1]

        #streamControl = control.StreamCtrlI()
        Control().connect_unicast(source, sink)

class MulticastTab(widgets.BuilderWrapper):
    def __init__(self):
        widgets.BuilderWrapper.__init__(self, 'res/multicast.glade')

        self._l = logging.getLogger('Multi_UI')

        cell = gtk.CellRendererText()
        self.b_combo_source.pack_start(cell, True)
        self.b_combo_source.add_attribute(cell, 'text', 0)

        cell = gtk.CellRendererText()
        self.b_combo_sink.pack_start(cell, True)
        self.b_combo_sink.add_attribute(cell, 'text', 0)

        cell = gtk.CellRendererText()
        self.b_combo_multi.pack_start(cell, True)
        self.b_combo_multi.add_attribute(cell, 'text', 0)
  
        self.b_combo_source.connect('changed', self._check_selections)
        self.b_combo_multi.connect('changed', self._check_selections_multiconnect)
        self.b_combo_sink.connect('changed', self._check_selections_multiconnect)

        self.b_create_multicast_btn.connect('clicked', self._create_multi)
        self.b_connect_multicast_btn.connect('clicked', self._connect_multi)

        self._l.info('Multicast tab created')

    def get_content(self):
        return self.b_root

    def set_models(self, model_mmdev, model_multi):
        self._sources_model = model_mmdev.filter_new()
        self._sources_model.set_visible_func(self._model_filter_func,
                                             '::Media::Source')

        self._sinks_model = model_mmdev.filter_new()
        self._sinks_model.set_visible_func(self._model_filter_func,
                                             '::Media::Sink')

        self.b_combo_source.set_model(self._sources_model)
        self.b_combo_sink.set_model(self._sinks_model)

        self.b_combo_multi.set_model(model_multi)

    def _check_selections(self, combobox):
        is_selected = combobox.get_active() != -1

        self.b_create_multicast_btn.set_sensitive(is_selected)

    def _check_selections_multiconnect(self, combobox):
        is_selected_multisrc = self.b_combo_multi.get_active() != -1
        is_selected_sink = self.b_combo_sink.get_active() != -1

        self.b_connect_multicast_btn.set_sensitive(is_selected_sink and
                                                   is_selected_multisrc)

    def _model_filter_func(self, model, iterator, data):
        mmdev = model.get_value(iterator, 1)
        if not mmdev: return False
        
        return mmdev.ice_isA(data)

    def _create_multi(self, button):
        self._l.info('Creating multicast A')

        it = self.b_combo_source.get_active_iter()
        selected = self.b_combo_source.get_model()[it][1]

        self._l.debug('Multicast creation. Selected source: {0}'.format(selected))

        multi = Control().create_multi(selected)
        # retval, got_qos = multi.bindDevs(selected, None, [], [], [])
        # bindDevs A->None and get the MCastConfig to store it

        # self.b_combo_multi.get_model()

    def _connect_multi(self, button):
        self._l.info('Connecting a multicast with a sink')

        it = self.b_combo_multi.get_active_iter()
        selected = self.b_combo_multi.get_model()[it][1]

        self._l.debug('Multi connect: selected multi: {0}'.format(selected))

        it = self.b_combo_sink.get_active_iter()
        selected_sink = self.b_combo_sink.get_model()[it][1]

        self._l.debug('Multi connect: selected sink: {0}'.format(selected_sink))
        Control().connect_multi(selected, selected_sink)

class ActiveTab:
    def __init__(self):
        self._l = logging.getLogger('Act_UI')
        self._root = gtk.VBox()

    def get_content(self):
        return self._root

    def new_active(self, values):
        self._l.info('Received {0}'.format(values))
        strCtrlPrx, mmdev_a, mmdev_b = values
        row = widgets.ConnectionRow(strCtrlPrx, mmdev_a,
                                    mmdev_b)

        # FIXME: GTK fail! Must hide the container before adding a new 
        #   row element.
        self._root.hide()
        row.show_all()
        self._root.pack_start(row, expand=False)
        self._root.show_all()
