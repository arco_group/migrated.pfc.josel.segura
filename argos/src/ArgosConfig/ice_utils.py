# -*- coding: utf-8; mode: python -*-

import sys
import logging

import Ice
from loggingx import ColorFormatter
from pyarco.Pattern import Singleton

# console = logging.StreamHandler()
# console.setLevel(logging.INFO)
# console.setFormatter(ColorFormatter('%(levelname)s [%(name)s]: %(message)s'))
# logger.addHandler(console)

class IceCore(object):
    'A class with useful Ice-related stuff'

    __metaclass__ = Singleton

    def __init__(self, args=sys.argv[1:]):
        '''The args argument are a list of strings (the argument list
        by default). This list will be pased to Ice.initialize method
        to initialize the Ice Communicator
        '''
        self._l = logging.getLogger('IceC')
        self._l.info('IceCore initialization')

        self.__ic = Ice.initialize(args)

        self.__adapter = \
            self.__ic.createObjectAdapter('ConfiguratorAdapter')
        self.__adapter.activate()

        # method mapping
        self.identityToString = self.__ic.identityToString
        self.stringToIdentity = self.__ic.stringToIdentity
        self.proxyToString = self.__ic.proxyToString
        self.stringToProxy = self.__ic.stringToProxy
        self.propertyToProxy = self.__ic.propertyToProxy

        self.add = self.__adapter.add
        self.addWithUUID = self.__adapter.addWithUUID

        self.getProperties = self.__ic.getProperties


