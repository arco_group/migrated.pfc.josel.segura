#!/usr/bin/env python2.6
# -*- coding: utf-8; mode: python -*-

import sys
import os
import logging

import gobject
import pygtk
pygtk.require20()
import gtk

import Ice
from loggingx import ColorFormatter

from searcher import Searcher
from ice_utils import IceCore
import widgets
import gtk_ui

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)s %(message)s',
                    filename='/tmp/ArgosConfig_%s.log' % os.getpid(),
                    filemode='w')

console = logging.StreamHandler()
console.setLevel(logging.DEBUG)
console.setFormatter(ColorFormatter('%(levelname)s [%(name)s]: %(message)s'))
logging.getLogger().addHandler(console)

DEFAULT_CONFIG_FILE = '%s/.argos/ice_config' % os.environ['HOME']

if __name__ == '__main__':
    logging.info('Starting Argos Config Tool')

    args = sys.argv[:-1]

    ice_configs = [ x for x in args if x.startswith('--Ice.') ]

    if len(ice_configs) == 0:
        args.append('--Ice.Config=%s' % DEFAULT_CONFIG_FILE)

    try:

        ic = IceCore(args)
        searcher = Searcher()
        gui = gtk_ui.MainWindow(searcher)

        gobject.threads_init()
        searcher.start()
        gtk.main()
        sys.exit(0)

    except Ice.NoEndpointException, e:
        logging.error(e)
        sys.exit(1)

    except KeyboardInterrupt:
        logging.info('Exit: user')
        sys.exit(0)
