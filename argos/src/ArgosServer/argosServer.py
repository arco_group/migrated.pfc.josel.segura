#!/usr/bin/python2.6
# -*- coding: utf-8; mode: python -*-

import logging
import os, sys

from loggingx import ColorFormatter

from core import Core

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)s %(message)s',
                    filename='/tmp/argosServer_{0}.log'.format(os.getpid()),
                    filemode='w')

console = logging.StreamHandler()
console.setLevel(logging.DEBUG)
#console.setFormatter(ColorFormatter('%(levelname)s [%(name)s]: %(message)s'))
logging.getLogger().addHandler(console)

if __name__ == '__main__':
     core = Core(sys.argv[1:])
     logging.info('Exitting')
     sys.exit(0)
