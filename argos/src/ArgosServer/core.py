# -*- coding: utf-8; mode: python -*-

import sys
import logging

import Ice
import pyarco.Net
import gobject

import gstice_server
import rtsp_server

Ice.loadSlice('--all -I/usr/share/hesperia/slice -I{0} '
              '../slice/Argos/MMDevCreatorService.ice'.\
                  format(Ice.getSliceDir()))

import Argos

Ice.loadSlice('--all -I{0} '
              '/usr/share/hesperia/slice/Hesperia/PropertyType.ice'.\
                  format(Ice.getSliceDir()))
import P

Ice.loadSlice('--all -I/usr/share/hesperia/slice -I{0} '
              '/usr/share/hesperia/slice/Hesperia/PropertyService.ice'.\
                  format(Ice.getSliceDir()))
import PropertyService

class Core:
    def __init__(self, args):
        self.__log = logging.getLogger('CORE')

        self.__ic = Ice.initialize(args)
        properties = self.__ic.getProperties()

        self.__adapter = self.__ic.createObjectAdapter('ArgosServer.Adapter')
        self.__adapter.activate()

        transport = properties.getProperty('transport')

        if transport not in ['rtsp', 'gstice']:
            self.__log.error('Unknown transport')
            sys.exit(1)

        configuration = properties.getPropertiesForPrefix(transport + '.')
        configuration['source'] = properties.getPropertyWithDefault('source', 'videotestsrc')
        configuration['communicator'] = self.__ic

        if transport.lower() == 'rtsp':
            self.__server = rtsp_server.RTSPServerApp(**configuration)

        elif transport.lower() == 'gstice':
            self.__server = gstice_server.GstIceServer(**configuration)

        self.__server.prx = Argos.DelegatedAppPrx.uncheckedCast(
            self.__adapter.add(self.__server, self.__ic.stringToIdentity( \
                    '{0}.Server'.format(configuration['source']))))

        self.__log.info('Server proxy: {0}'.format(self.__server.prx))

        prx = self.__ic.stringToProxy(properties.getProperty('Argos.MMDeviceDeployer'))
        prx = Argos.MMDeviceCreatorPrx.checkedCast(prx)

        prx.addServer(self.__server.prx, {})

        try:
            gobject.threads_init()
            self._gloop = gobject.MainLoop()
            self._gloop.run()

            self.__ic.shutdown()
            self.__log.info('Shutdown')

        except KeyboardInterrupt:
            pass

        finally:
            if self.__server:
                try:
                    self.__server.stop()

                except:
                    pass

                self.__log.info('Deleting from MMDeviceCreator')
                prx.delServer(self.__server.prx)
                self.__adapter.deactivate()

