# -*- coding: utf-8; mode: python -*-

import logging
import time

import gobject
import gst.rtspserver
import netifaces
import Ice
import pyarco.Net

Ice.loadSlice('--all -I/usr/share/hesperia/slice -I{0} '
              '../slice/Argos/MMDevCreatorService.ice'.\
                  format(Ice.getSliceDir()))

import Argos

Ice.loadSlice('--all -I/usr/share/hesperia/slice -I{0} '
              '/usr/share/hesperia/slice/Hesperia/PropertyService.ice'.\
                  format(Ice.getSliceDir()))
import PropertyService
import P


def getIPs():
    ifaces = [ x for x in netifaces.interfaces()
               if not x == 'lo' and
               netifaces.ifaddresses(x).has_key(netifaces.AF_INET)]

    retval = []

    for iface in ifaces:
        retval += [ x['addr'] for x in netifaces.ifaddresses(iface)[netifaces.AF_INET] ]

    return retval

class RTSPServerApp(Argos.DelegatedApp):
    def __init__(self, source, **kwargs):
        self._logger = logging.getLogger('GstRTSPServer')
        self._logger.info('Loading GstRTSPServer')

        self._source = source

        self._port = kwargs.get('rtsp.port', pyarco.Net.getFreePort())
        self._path = kwargs.get('rtsp.path', source.split()[0])

        launch = '( {0} ! ffenc_mpeg4 ! rtpmp4vpay name=pay0 pt=96 )'.format(self._source)
        print launch
        self._server = gst.rtspserver.Server()
        self._server.set_property('service', str(self._port))

        self._factory = gst.rtspserver.MediaFactory()
        self._factory.set_launch(launch)

        mapping = self._server.get_media_mapping()
        mapping.add_factory(self._path, self._factory)

        self._server.attach()

    # Argos.DelegatedApp interface
    def getConfiguration(self, context):
        p = PropertyService.Property()
        p.propertyName = 'endpoint'
        p.propertyValue = \
            P.StringT('rtsp://{0}:{1}/{2}'.format(getIPs()[0],
                                                  self._port,
                                                  self._path))

        self._logger.debug('Configuration returned: {0}'.format(p))
        return p

    def configure(self, configuration, context):
        self._logger.debug('RTSPServerApp::configure')

    def play(self, context):
        self._logger.info('Play!')

        #self._server.attach()


    def stop(self, context=None):
        self._logger.info('Stop')

        # if self.__ps:
        #     self.__ps.terminate()



