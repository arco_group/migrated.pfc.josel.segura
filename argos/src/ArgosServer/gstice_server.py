# -*- coding: utf-8; mode: python -*-

import logging

import Ice
import gobject
import gst
import gstice

Ice.loadSlice('--all -I/usr/share/hesperia/slice -I{0} '
              '../slice/Argos/MMDevCreatorService.ice'.\
                  format(Ice.getSliceDir()))

import Argos

Ice.loadSlice('--all -I/usr/share/hesperia/slice -I{0} '
              '/usr/share/hesperia/slice/Hesperia/PropertyService.ice'.\
                  format(Ice.getSliceDir()))


def gst_change_state(element, state):
    element.set_state(state)
    return False

import PropertyService

class GstIceServer(Argos.DelegatedApp):
    def __init__(self, source, communicator, **kwargs):
        self._logger = logging.getLogger('GstIceServer')
        self._logger.info('Loading GstIceServer')
        self._source = source
        self._ic = communicator

        self._pipe = gst.Pipeline()

        videosrc = gst.parse_launch(source)

        encoder = gst.element_factory_make('ffenc_mpeg4')
        self._server = gstice.GstIceSink(communicator=communicator)

        #self._server.set_property('debug', True)
        self._server.set_property('timeout', 5000)
        self._pipe.add(videosrc, encoder, self._server)

        videosrc.link(encoder)
        encoder.link(self._server)

    # Argos.DelegatedApp interface
    def configure(self, configuration, context):
        self._logger.debug('Configuration received')

        receiver_proxy = configuration.propertyValue.value
        self._logger.debug('Receiver: {0}'.format(receiver_proxy))
        self._server.set_property('receiver_proxy', receiver_proxy)

    def getConfiguration(self, context):
        return PropertyService.Property()

    def play(self, context):
        self._logger.info('Play!')
        gobject.idle_add(gst_change_state, self._pipe, gst.STATE_PLAYING)


    def stop(self, context=None):
        self._logger.info('Stop!')
        if self._pipe.get_state()[1] == gst.STATE_PLAYING:
            gobject.idle_add(gst_change_state, self._pipe, gst.STATE_PAUSED)
