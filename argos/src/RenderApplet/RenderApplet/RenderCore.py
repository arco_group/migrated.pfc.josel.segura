# -*- coding: utf-8; mode: python -*-

import os
import logging

import Ice
from pyarco.Pattern import Singleton

from GstRTSPRender import GstRTSPRender
from GstIceRender import GstIceRender

ARGOS_SLICE_DIR = '/usr/local/share/argos/slice'

def loadSlice(cmd_line):
    Ice.loadSlice('--all {0}'.format(cmd_line))

loadSlice('-I/usr/share/Ice/slice -I/usr/share/hesperia/slice'
          ' {0}/Argos/MMDevCreatorService.ice'.format(ARGOS_SLICE_DIR))

import Argos

class ServerNotFoundException(Exception):
    def __init__(self, service_name, str_proxy):
        self.__name      = service_name
        self.__str_proxy = str_proxy

    def __str__(self):
        return "Can't connect to {0} service on \"{1}\"".format(
            self.__name, self.__str_proxy)

class RenderCore:
    __metaclass__ = Singleton

    CONFIG_FILE = os.path.join(os.environ['HOME'], '.argos', 'renderapplet_ice.cfg')

    def __init__(self):
        # logging initialization
        self.__logger  = logging.getLogger('Ice')

        self.__ic      = None
        self.__adapter = None
        self.__mmdc    = None

        self.__registered = False

        self.__logger.debug('Render Core initialized')

        # creating servant
        self.__render = None
        self.__loadConfig()

    def __loadConfig(self):
        if self.__ic:
            self.__logger.debug('Already initialized. Cleaning')

            if self.__registered:
                self.__mmdc.delRenderer(self.__renderPrx)

            self.__adapter.deactivate()
            self.__ic.shutdown()

        self.__logger.debug('Initializing communicator... and so on')
        self.__ic = Ice.initialize(['--Ice.Config={0}'.format(
                    RenderCore.CONFIG_FILE)])

        self.__adapter = self.__ic.createObjectAdapterWithEndpoints( \
            'RenderApplet', 'default')

        self.__adapter.activate()

        transport = self.__ic.getProperties().getProperty('transport')

        if transport == 'rtsp':
            self.__render = GstRTSPRender()

        elif transport == 'gstice':
            self.__render = GstIceRender(self.__ic)

        else:
            self.__logger.warning('This transport ({0}) is not '
                                  'supported'.format(transport))
            self.__render = None

        self.__renderPrx = Argos.DelegatedAppPrx.checkedCast( \
            self.__adapter.add(self.__render,
                               self.__ic.stringToIdentity( \
                    self.__render.identity)))

        self.__logger.debug('Servant added to object adapter')

        # on-line: get property -> convert to proxy -> casting to the
        # desired interface
        mmdc_str = \
            self.__ic.getProperties().getProperty('Argos.MMDeviceCreator')

        self.__mmdc = self.__ic.stringToProxy(mmdc_str)


    def createConfigFile(self, def_locator, service, transport):
        'Create the Ice configuration file'

        self.__logger.debug('Writing configuration file')
        f = open(RenderCore.CONFIG_FILE, 'w')

        f.write('Ice.Default.Locator = {0}\n'.format(def_locator))
        f.write('Argos.MMDeviceCreator = {0}\n'.format(service))
        f.write('transport = {0}\n'.format(transport.lower()))

        f.close()

        self.__logger.info('Configuration file saved at {0}'.format(
                RenderCore.CONFIG_FILE))

        self.__loadConfig()

    def getConfigFile(self):
        assert self.__ic != None
        locator = self.__ic.getProperties().getProperty('Ice.Default.Locator')
        mmcs    = self.__ic.getProperties().getProperty('Argos.MMDeviceCreator')
        str_method = self.__ic.getProperties().getPropertyWithDefault('transport', 'rtsp').upper()

        return locator, mmcs, str_method

    def register(self):
        self.__logger.debug('Registering the servant on MMDeviceCreatorService')
        try:
            self.__mmdc = Argos.MMDeviceCreatorPrx.checkedCast(self.__mmdc)
            self.__mmdc.addRenderer(self.__renderPrx, {})
            self.__registered = True

        except Ice.ConnectionRefusedException, exc:
            self.__logger.error(str(exc))
            raise ServerNotFoundException('MMDeviceCreator', str(self.__mmdc))

    def unregister(self):
        if self.__registered:
            self.__logger.debug('Unregistering')
            self.__mmdc.delRenderer(self.__renderPrx)
            self.__logger.debug('Unregistered')
            self.__render.stop()
            self.__registered = False


    def quit(self):
        self.__logger.debug('Stopping render, unregistering from service and '
                            'deactivating the object adapter')
        self.__render.stop()
        self.unregister()
        self.__adapter.deactivate()
        self.__ic.shutdown()
