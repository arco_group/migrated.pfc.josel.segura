# -*- coding: utf-8 -*-

import os
import time
import logging

import gobject
import gtk
import gst
import Ice

ARGOS_SLICE_DIR = '/usr/local/share/argos/slice'

def loadSlice(cmd_line):
    Ice.loadSlice('--all {0}'.format(cmd_line))

loadSlice('-I/usr/share/Ice/slice -I/usr/share/hesperia/slice'
          ' {0}/Argos/MMDevCreatorService.ice'.format(ARGOS_SLICE_DIR))
loadSlice('-I/usr/share/Ice/slice -I/usr/share/hesperia/slice'
          ' /usr/share/hesperia/slice/Hesperia/PropertyService.ice')

import Argos
import PropertyService

class GstRTSPRender(Argos.DelegatedApp):
    def __init__(self, *args):
        self._logger = logging.getLogger('GST')
        self._logger.setLevel(logging.NOTSET)
        self.identity = '{0}_renderapplet'.format(os.environ['USER'])

        # Visualization stuff
        self._window = None

        # Gstreamer stuff
        self._pipe = gst.Pipeline()
        self._pipe.get_bus().add_watch(self._bus_watch)

        # need to store the source because it will be configured on
        # Ice interruption
        self._src = gst.element_factory_make('rtspsrc')
        dec       = gst.element_factory_make('decodebin2')
        snk       = gst.element_factory_make('xvimagesink', 'the_sink')

        # configure the output element
        #snk.set_property('sync', False)
        snk.set_property('force-aspect-ratio', True)

        self._pipe.add(self._src, dec, snk)

        self._src.connect('pad-added', self._pad_added, dec)
        dec.connect('pad-added', self._pad_added, snk)

    def _bus_watch(self, bus, message):
        if message.type != gst.MESSAGE_ELEMENT:
            return True

        if message.structure.has_name('prepare-xwindow-id'):
            self._logger.info('Preparing XWindow')
            if self._window and self._window.window != None:
                message.src.set_xwindow_id(self._window.window.xid)

            else:
                self._logger.info('Can\'t use the window')

        return True

    def _pad_added(self, srcElement, pad, sinkElement):
        self._logger.debug('Connecting {0} -> {1}'.format(
                srcElement.get_name(), sinkElement.get_name()))

        srcElement.link(sinkElement)


    def _on_hide(self, widget):
        # the user data must be a Gst Element (XVimagesink)
        self._logger.debug('Window closed')
        self._pipe.set_state(gst.STATE_NULL)

    def _on_key_press(self, widget, event):
        if event.keyval != 102: # 'f' == 102
            return True

        if widget.window.get_state() == gtk.gdk.WINDOW_STATE_FULLSCREEN:
            widget.unfullscreen()

        else:
            widget.fullscreen()

        return True

    # Ice interface
    def configure(self, configuration, context=None):
        self._src.set_property('location', configuration.propertyValue.value)
        self._logger.info('RTSP source: {0}'.format(self._src.get_property(\
                    'location')))

        self._window = gtk.Window()
        self._window.set_size_request(640, 480)
        self._logger.debug('Window created')

        # GTK callbacks
        self._window.connect('hide', self._on_hide)
        self._window.connect('key-press-event', self._on_key_press)

    def getConfiguration(self, context=None):
        self._logger.debug('DelegatedApp::getConfiguration')
        return PropertyService.Property()

    def play(self, context):
        self._logger.info('play!')

        gobject.idle_add(self._window.show)
        ret = self._pipe.set_state(gst.STATE_PLAYING)
        self._logger.debug('Pipeline state changed')

        self._logger.debug('RTSP location property: {0}'.format(
                self._src.get_property('location')))

        while ret == gst.STATE_CHANGE_FAILURE:
            self._logger.error('Failed to play the stream: {0}'.format(ret))
            time.sleep(0.5)
            ret = self._pipe.set_state(gst.STATE_PLAYING)

    def stop(self, context=None):
        self._logger.info('Stop!')
        if self._pipe.get_state()[1] == gst.STATE_PLAYING:
            gobject.idle_add(self._pipe.set_state, gst.STATE_PAUSED)
            gobject.idle_add(self._window.hide)
