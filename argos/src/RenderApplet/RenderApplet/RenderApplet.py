#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os

import logging
from loggingx import ColorFormatter, NotifyHandler

logging.basicConfig(level=logging.NOTSET, # all the logs to a file
                    format='%(asctime)s %(levelname)s %(message)s',
                    filename='/tmp/renderapplet_{0}.log'.format(os.getpid()),
                    filemode='w')

import gobject
import glib
import pygtk
pygtk.require('2.0')
import gtk
import gnomeapplet

# FIXME!
#logging.warning('On devel: adding the devel path to sys.path')
#sys.path.append('/home/joseluis/uni/pfc/src/RenderApplet/RenderApplet')

from RenderCore import RenderCore, ServerNotFoundException

class RenderApplet(gnomeapplet.Applet):
    '''This class inherits from Applet. It contains a gtk.Button with
    an stock image (media-play or media-stop). When it shows the play
    button and it is pressed, a Ice object is added to a object
    adapter and a thread announcing it through an IceStorm topic is
    launched
    '''

    menu = '''
<popup name="button3">
  <menuitem name="Item 2" verb="Preferences" label="Preferences"
      pixtype="stock" pixname="gtk-preferences"/>

  <menuitem name="Item 1" verb="About" label="About..."
      pixtype="stock" pixname="gtk-about"/>
</popup>
'''
    def __init__(self):
        '''Only call to the inherited class constructor. When this
        class is used as an applet, the constructor is not called.
        '''
        logging.debug('Render Applet created using the constructor')
        gnomeapplet.Applet.__init__(self)
        self.args = sys.argv

    def init(self):
        '''This method should be the constructor. It has the
        initialization of the contained objects and the status of the
        program
        '''

        logging.debug('Initializing Render Applet')
        self.__uLogger = logging.getLogger('Render Applet')
        self.__uLogger.addHandler(NotifyHandler())

        self.__running = False
        self.image_play = gtk.Image()
        self.image_stop = gtk.Image()

        self.image_play.set_from_stock(gtk.STOCK_NO,
                                       gtk.ICON_SIZE_BUTTON)
        self.image_stop.set_from_stock(gtk.STOCK_YES,
                                       gtk.ICON_SIZE_BUTTON)

        logging.debug('Creating the button and adding the image')
        button = gtk.Button()

        button.add(self.image_play)
        button.connect('clicked', self.__on_click_button)
        button.connect('button-press-event', self.__on_button_press)

        self.setup_menu(self.menu, [("About", self.__about_cb),
                                    ("Preferences", self.__pref_cb)])

        self.add(button)
        self.show_all()

    def quit(self):
        self.hide()
        RenderCore().quit()

    def __about_cb(self, event, data=None):
        'Shows an "about" window'
        logging.debug('Loading about dialog')

        about = gtk.AboutDialog()
        about.set_name("Argos Render Applet")
        about.set_version('0.5')
        about.run()
        about.destroy()

    def __pref_cb(self, event, data=None):
        'Callback to show the preferences dialog'
        logging.debug('Loading preferences dialog')

        xml = 'res/preferences.xml'
        if not os.path.exists(xml):
            logging.info('loading XML from standard location')
            xml = '/usr/local/share/argos-render-applet/res/preferences.xml'

        try:
            builder = gtk.Builder()
            builder.add_from_file(xml)
            dialog = builder.get_object('preferences_dialog')

            combo = gtk.combo_box_new_text()
            builder.get_object('table1').attach(combo, 1, 2, 2, 3)

            combo.show()

            entries = (builder.get_object('entry_locator'),
                       builder.get_object('entry_service'),
                       combo,
                       )

            entries[2].append_text('RTSP')
            entries[2].append_text('GSTICE')

            config_values = RenderCore().getConfigFile()
            entries[0].set_text(config_values[0])
            entries[1].set_text(config_values[1])

            if config_values[2] == 'RTSP':
                entries[2].set_active(0)

            elif config_values[2] == 'GSTICE':
                entries[2].set_active(1)

            else:
                entries[2].set_active(-1)
            #entries[2].set_text(config_values[2])

            dialog.connect('response', self.__pref_response, entries)
            dialog.run()
            dialog.destroy()

        except glib.GError, ex:
            logging.error(str(ex))

    def __pref_response(self, dialog, response_id, user_param):
        if response_id == gtk.RESPONSE_OK:
            # Need to store the values for proxies
            logging.info('Default Locator: {0}'.format(
                    user_param[0].get_text()))
            logging.info('Service Proxy: {0}'.format(
                    user_param[1].get_text()))


            transport = user_param[2].get_model()[user_param[2].get_active_iter()][0]
            logging.info('Transport: {0}'.format(transport))

            try:
                RenderCore().createConfigFile(user_param[0].get_text(),
                                              user_param[1].get_text(),
                                              transport)

            except ServerNotFoundException, exc:
                self.__uLogger.error("Render Applet\n" + str(exc))

    def __on_button_press(self, widget, event, data=None):
        '''Callbcack to allow the applet capture the right button click
        to show the menu
        '''
        logging.debug('Button pressed')
        # this allows dragging applet around panel and friends
        if event.button != 1:
            widget.stop_emission('button_press_event')
        return False


    def __on_click_button(self, button):
        "Callback of the applet's button"

        if not self.__running:
            try:
                RenderCore().register()
                button.set_image(self.image_stop)
                self.__running = True
                logging.debug('Render Service started')
                self.__uLogger.info('Registered\nRender Applet registered successfully')

            except ServerNotFoundException, exc:
                self.__uLogger.error(str(exc))

        else:
            RenderCore().unregister()
            button.set_image(self.image_play)
            self.__running = False
            logging.debug('Render Service stopped')
            self.__uLogger.info('Unregistered\nRender Applet unregistered successfully')


def render_applet_factory(applet, iid):
    'Bonobo factory function'
    applet.init()
    return True

gtk.gdk.threads_init()

if '--window' not in sys.argv:
    logging.info('Starting RenderAppler as applet (normal mode)')

    # registering types for apple execution
    gobject.type_register(RenderApplet)
    gnomeapplet.bonobo_factory("OAFIID:GNOME_ArgosRenderApplet_Factory",
                               RenderApplet.__gtype__,
                               "media render applet", "0", render_applet_factory)


else:
    # configure StreamHandler to get debug messages
    try:
        console = logging.StreamHandler()
        console.setFormatter(ColorFormatter('%(levelname)s [%(name)s]: %(message)s'))
        logging.getLogger().addHandler(console)

        logging.info('Starting RenderApple as window (debug mode)')
        logging.debug('Command line: {0}'.format(sys.argv))

        main_window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        main_window.set_decorated(False)
        main_window.set_size_request(320, 320)
        main_window.connect("destroy", gtk.main_quit)

        app = RenderApplet()
        render_applet_factory(app, None)
        app.reparent(main_window)
        main_window.show()

        main_window.connect('destroy', gtk.main_quit)

        gtk.main()

        app.quit()
        sys.exit(0)

    except KeyboardInterrupt:
        app.quit()
        sys.exit(0)
