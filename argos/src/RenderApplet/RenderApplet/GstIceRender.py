# -*- coding: utf-8 -*-

import os
import logging
import time

import Ice
import gobject
import gst
import gstice
import gtk

ARGOS_SLICE_DIR = '/usr/local/share/argos/slice'

def loadSlice(cmd_line):
    Ice.loadSlice('--all {0}'.format(cmd_line))

loadSlice('-I/usr/share/Ice/slice -I/usr/share/hesperia/slice'
          ' {0}/Argos/MMDevCreatorService.ice'.format(ARGOS_SLICE_DIR))
loadSlice('-I/usr/share/Ice/slice -I/usr/share/hesperia/slice'
          ' /usr/share/hesperia/slice/Hesperia/PropertyService.ice')

import Argos
import PropertyService
import P

def gst_change_state(element, state):
    ret = element.set_state(state)
    return ret == gst.STATE_CHANGE_FAILURE

class GstIceRender(Argos.DelegatedApp):
    def __init__(self, communicator, *args):
        self._logger = logging.getLogger('GstIce')
        self._logger.setLevel(logging.NOTSET)
        self.identity = '{0}_renderapplet'.format(os.environ['USER'])

        # Visualization stuff
        self._window = None

        # Gstreamer
        self._pipe = gst.Pipeline()
        self._pipe.get_bus().add_watch(self._bus_watch)

        self._src = gstice.GstIceSrc('source', communicator)
        self._logger.debug('Used communicator: {0}'.format(communicator))

        dec  = gst.element_factory_make('ffdec_mpeg4')
        sink = gst.element_factory_make('xvimagesink', 'the_sink')

        # configure the output element
        sink.set_property('force-aspect-ratio', True)

        self._pipe.add(self._src, dec, sink)

        self._src.connect('pad-added', self._pad_added, dec)
        #dec.connect('pad-added', self._pad_added, sink)
        dec.link(sink)

    def _bus_watch(self, bus, message):
        if message.type != gst.MESSAGE_ELEMENT:
            return True

        if message.structure.has_name('prepare-xwindow-id'):
            self._logger.info('Preparing XWindow')
            if self._window and self._window.window:
                message.src.set_xwindow_id(self._window.window.xid)

            else:
                self._logger.info("Can't use the window yet")

        return True

    def _pad_added(self, element, pad, next):
        self._logger.debug('Connecting {0} -> {1}'.format(
                element.get_name(), next.get_name()))

        element.link(next)


    def _on_hide(self, widget):
        self._logger.debug('Window closed')
        self._logger.debug('Source state: {0}'.format(
                self._src.get_state()))

        gobject.idle_add(gst_change_state, self._pipe, gst.STATE_PAUSED)

    def _on_key_press(self, widget, event):
        # callback to capture 'F' button to fullscreen
        if event.keyval != 102: # 'f' == 102
            return True

        if widget.window.get_state() == gtk.gdk.WINDOW_STATE_FULLSCREEN:
            widget.unfullscreen()

        else:
            widget.fullscreen()

        return True

    # Ice Argos::DelegatedApp interface
    def configure(self, configuration, context):
        self._logger.info('Configuration received')

        if self._window:
            self._logger.debug('Destroying old window')
            self._window.destroy()
            gobject.idle_add(gst_change_state, self._pipe, gst.STATE_NULL)

        self._window = gtk.Window()
        self._window.set_size_request(640, 480)
        self._logger.debug('Window created')

        # GTK callbacks
        self._window.connect('hide', self._on_hide)
        self._window.connect('key-press-event', self._on_key_press)

    def getConfiguration(self, context):
        self._logger.info('Get configuration')

        retval = PropertyService.Property()
        retval.propertyName = 'receiver'
        retval.propertyValue = \
            P.StringT(self._src.get_property('receiver-proxy'))

        self._logger.info('Sent configuration: {0}'.format(retval))
        return retval

    def play(self, context):
        self._logger.info('Playing')

        gobject.idle_add(self._window.show)
        gobject.idle_add(gst_change_state, self._pipe, gst.STATE_PLAYING)

        self._logger.debug('Pipeline state changed')

    def stop(self, context=None):
        self._logger.info('Stop')

        gobject.idle_add(gst_change_state, self._pipe, gst.STATE_PAUSED)

        if self._window:
            gobject.idle_add(self._window.hide)


