#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import logging

import Ice
from loggingx import ColorFormatter
import gst
import gstice
print gstice.__file__

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)s %(message)s',
                    filename='/tmp/gstice-server_%s.log' % os.getpid(),
                    filemode='w')

console = logging.StreamHandler()
console.setFormatter(ColorFormatter('%(levelname)s [%(name)s]: %(message)s'))
logging.getLogger().addHandler(console)

class GstLauncher:
    def __init__(self, description, locator=None):
        self._log = logging.getLogger('Gstreamer')
        self._pipe = gst.Pipeline()

        self._log.debug("Description: '%s'" % description)
        self._bin = gst.parse_bin_from_description(description, False)
        self._sink = gstice.GstIceSink('sink')

        if locator:
            self._sink.set_property('default-locator', locator)

        self._pipe.add(self._bin, self._sink)

        # creation of the ghost pad signal
        it = self._bin.sorted()

        lastElement = it.next()
        del(it)

        src_pad = [ x for x in lastElement.get_pad_template_list() \
                        if x.direction == gst.PAD_SRC ][0]

        if src_pad.presence == gst.PAD_ALWAYS:
            pad = lastElement.get_static_pad(src_pad.name_template)
            ghost = gst.GhostPad(src_pad.name_template, pad)
            ghost.set_active(True)
            self._bin.add_pad(ghost)
            self._bin.link(self._sink)

        elif src_pad.presence == gst.PAD_SOMETIMES:
            lastElement.connect('pad-added', self._newPadOnBin, \
                                    src_pad.name_template, self._bin)
            self._bin.connect('pad-added', self._onBinPadAdded, self._sink)

        else: # PAD_REQUEST
            # Not implemented
            raise NotImplemented

        # connect the bin pad to my sink
        self._bin.connect('pad-added', self._onBinPadAdded, self._sink)

    def _newPadOnBin(self, element, pad, bin):
        self._log.debug('Creating the ghost pad of the bin')

        if pad.get_direction() == gst.PAD_SRC:
            gp = gst.GhostPad(pad.get_name(), pad)
            gp.set_active(True)
            bin.add_pad(gp)

    def _onBinPadAdded(self, element, pad, sink):
        self._log.debug('Connecting the bin to the sink')
        element.link(sink)

    def play(self):
        self._pipe.set_state(gst.STATE_PLAYING)

    def setReceiver(self, receiverPrx):
        self._sink.set_property('receiver-proxy', receiverPrx)


try:
    if __name__ == '__main__':
        logging.info('Starting Gstreamer-Ice Server')

        ic = Ice.initialize(sys.argv[1:])

        prop = ic.getProperties()

        launcher = GstLauncher(prop.getProperty('gst-launch.description'),
                               str(ic.getDefaultLocator()))
        #launcher.setReceiver(prop.getProperty('gstice.receiver'))

        import gobject
        gobject.idle_add(launcher.play)

        import IceStorm
        tpcManager = IceStorm.TopicManagerPrx.checkedCast(
            ic.stringToProxy('IceStorm/TopicManager @ IceStorm.TopicManager'))

        topic = tpcManager.retrieve('CANAL')
        pub = topic.getPublisher()
        launcher.setReceiver(str(pub))

        gobject.MainLoop().run()


except KeyboardInterrupt:
    logging.debug('Exit: Control+C')
    sys.exit(0)

