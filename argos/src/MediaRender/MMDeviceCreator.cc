// -*- mode: c++; coding: utf-8 -*-

#include <easyc++/logger.h>

#include <IceGrid/Locator.h>
#include <IceGrid/Registry.h>
#include <IceGrid/Admin.h>

#include "MMDeviceCreator.hh"
#include "MMDev.hh"

MMDeviceCreator::MMDeviceCreator(const Ice::CommunicatorPtr communicator,
								 const Ice::ObjectAdapterPtr adapter,
								 const PropertyService::PropertySetDefFactoryPrx& psf) :
  _ic(communicator), _adapter(adapter), _psf(psf) {

  Ice::PropertiesPtr properties = _ic->getProperties();

  _user =
	properties->getProperty("IceGrid.AdminSession.Username");

  _pass =
	properties->getProperty("IceGrid.AdminSession.Password");

  ec::debug() << "Admin Session username: " << _user << std::endl;
  ec::debug() << "Admin Session password: " << _pass << std::endl;

}

void
MMDeviceCreator::addRenderer(const Argos::RenderApplicationPrx& newRenderer,
						  const PropertyService::Properties& capabilities,
						  const Ice::Current& current) {

  Ice::Identity id = newRenderer->ice_getIdentity();

  MMDev* mmdev = new MMDev(newRenderer, capabilities, _psf);

  AVStreams::MMDevicePrx prx = AVStreams::MMDevicePrx::uncheckedCast
	(_adapter->add(mmdev, id));

  mmdev->setProxy(prx);

  IceGrid::LocatorPrx locator = IceGrid::LocatorPrx::checkedCast
	(_ic->getDefaultLocator());

  IceGrid::AdminSessionPrx session = (locator->getLocalRegistry())->
	createAdminSession(_user, _pass);

  IceGrid::AdminPrx adminPrx = session->getAdmin();

  adminPrx->addObjectWithType(prx, AVStreams::MMDevice::ice_staticId());

  session->destroy();

  // This prx should be registered on the IceGrid Registry
  ec::debug() << "Added object: " << prx << std::endl;

  _mmdevices[id] = mmdev;
  printAll();

}

void
MMDeviceCreator::delRenderer(const Argos::RenderApplicationPrx& renderer,
							 const Ice::Current& current) {

  Ice::Identity key = renderer->ice_getIdentity();

  /*
   * Unregistering the object from the Registry
   */

  IceGrid::LocatorPrx locator = IceGrid::LocatorPrx::checkedCast
	(_ic->getDefaultLocator());

  IceGrid::AdminSessionPrx session = (locator->getLocalRegistry())->
	createAdminSession(_user, _pass);

  IceGrid::AdminPrx adminPrx = session->getAdmin();

  adminPrx->removeObject(key);
  session->destroy();

  _adapter->remove(key);
  _mmdevices.erase(key);
  printAll();

}

void
MMDeviceCreator::printAll() {
  ec::info() << "Printing all elements" << std::endl;

  for(std::map<Ice::Identity, AVStreams::MMDevicePtr>::iterator	\
		it=_mmdevices.begin(); it != _mmdevices.end(); it++)

	ec::debug() << it->first.name << std::endl;

}

