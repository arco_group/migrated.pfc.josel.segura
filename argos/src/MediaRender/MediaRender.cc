// -*- mode: c++; coding: utf-8 -*-

#include <iostream>
#include <easyc++/logger.h>

#include "MediaRender.hh"
#include "MMDeviceCreator.hh"

using namespace std;

extern "C" {
  IceBox::Service*
  createMediaRender(Ice::CommunicatorPtr ic) {
	ec::Logger::root.set_level(ec::Logger::DEBUG);
	return new MediaRender();
  }
}

void
MediaRender::start(const string& name,
				   const Ice::CommunicatorPtr& ic,
				   const Ice::StringSeq& args) {

  ec::debug() << "[MediaRender] Starting service" << endl;

  Ice::PropertiesPtr properties = ic->getProperties();

  Ice::ObjectPrx objPrx =
	ic->stringToProxy(properties->getProperty("PropertySetFactory.Proxy"));

  _psf = PropertyService::PropertySetDefFactoryPrx::checkedCast(objPrx);

  ec::debug() << "[MediaRender] PropertySetDefFactory proxy: " << _psf << endl;

  try {
	_adapter = ic->createObjectAdapter("RenderAdapter");
	_localAdapter = ic->createObjectAdapter("RenderLocalAdapter");

	_adapter->activate();
	_localAdapter->activate();

	_deployer = new MMDeviceCreator(ic, _adapter, _psf);
	Ice::ObjectPrx p = _localAdapter->add(_deployer, ic->stringToIdentity("Deployer"));

	ec::debug() << p << std::endl;

  }

  catch(Ice::InitializationException e) {
	ec::fatal() << "[MediaRender] Initialization error" << e.what() << endl;
	exit(1);
  }
}

void
MediaRender::stop() {
  ec::debug() << "[MediaRender] Stopping..." << endl;

  if (_adapter)
	_adapter->deactivate();

  if (_localAdapter)
	_localAdapter->deactivate();
}
