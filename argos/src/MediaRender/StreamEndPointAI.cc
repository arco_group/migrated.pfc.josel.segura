/* -*- mode: c++; coding: utf-8 -*- */

#include "StreamEndPointAI.hh"

StreamEndPointAI::StreamEndPointAI(MMDev* mmdev,
								   PropertyService::PropertySetDefFactoryPrx psfactory) :
  StreamEndPointI::StreamEndPointI(psfactory),
  _mmdev(mmdev) {

  // Set PropertySetDef to PropertyWrapper
  setPropertySetDef(psfactory->createPropertySetDef());
  }

bool
StreamEndPointAI::multiconnect(const ::AVStreams::FlowSpec& req_spec,
				  const ::AVStreams::StreamQoS& req_qos,
				  ::AVStreams::FlowSpec& got_spec,
				  ::AVStreams::StreamQoS& got_qos,
				  const Ice::Current& current) {
  return false;
}

bool
StreamEndPointAI::connectLeaf(const ::AVStreams::StreamEndPointBPrx& the_ep,
				 const ::AVStreams::StreamQoS& req_qos,
				 const ::AVStreams::FlowSpec& the_flows,
				 ::AVStreams::StreamQoS& got_qos,
				 const Ice::Current& current) {
  return false;
}

void
StreamEndPointAI::disconnectLeaf(const ::AVStreams::StreamEndPointBPrx& the_ep,
				    const ::AVStreams::FlowSpec& the_spec,
				    const Ice::Current& current) {
}

