// -*- mode: c++; coding: utf-8 -*-

#include <easyc++/logger.h>

#include "MMDev.hh"
#include "StreamEndPointAI.hh"
#include "StreamEndPointBI.hh"
#include "StreamCtrlI.hh"
#include "VDevI.hh"

using namespace std;

MMDev::MMDev(const Argos::RenderApplicationPrx& app,
			 const PropertyService::Properties& capabilities,
			 const PropertyService::PropertySetDefFactoryPrx& psf) :
  _app(app),  _capabilities(capabilities), _psfactory(psf) {

  ec::debug() << "MMDev created" << endl;

}

void
MMDev::setProxy(const ::AVStreams::MMDevicePrx& prx) {
  _proxy = prx;
}

::AVStreams::MMDevicePrx
MMDev::getProxy() {
  return _proxy;
}

::AVStreams::StreamEndPointAPrx
MMDev::createA(const ::AVStreams::StreamCtrlPrx& the_requester,
						const ::AVStreams::StreamQoS& req_qos,
						const ::std::string& req_named_vdev,
						const ::AVStreams::FlowSpec& the_spec,
						::AVStreams::VDevPrx& the_vdev,
						bool& met_qos,
						::std::string& got_named_vdev,
						::AVStreams::StreamQoS& got_qos,
						const Ice::Current& current) {

  ec::info() << "--- MMDevice:createA" << endl;

  StreamEndPointAI* sep = new StreamEndPointAI(this, _psfactory);
  VDevI* vdev = new VDevI(_capabilities, _app, _psfactory);
  the_vdev = AVStreams::VDevPrx::uncheckedCast(current.adapter->addWithUUID(vdev));
  ec::debug() << "VDevA created: " << \
	current.adapter->getCommunicator()->proxyToString(the_vdev) << endl;

  met_qos = true;
  return AVStreams::StreamEndPointAPrx::uncheckedCast(current.adapter->addWithUUID(sep));
}

::AVStreams::StreamEndPointBPrx
MMDev::createB(const ::AVStreams::StreamCtrlPrx& the_requester,
						const ::AVStreams::StreamQoS& req_qos,
						const ::std::string& req_named_vdev,
						const ::AVStreams::FlowSpec& the_spec,
						::AVStreams::VDevPrx& the_vdev,
						bool& met_qos,
						::std::string& got_named_vdev,
						::AVStreams::StreamQoS& got_qos,
						const Ice::Current& current) {

  ec::info() << "--- MMDevice:createB" << endl;

  StreamEndPointBI* sep = new StreamEndPointBI(this, _psfactory);

  ec::debug() << "--- StreamEndpointB created" << endl;
  VDevI* vdev = new VDevI(_capabilities, _app, _psfactory);
  the_vdev = AVStreams::VDevPrx::uncheckedCast(current.adapter->addWithUUID(vdev));
  ec::debug() << "VDevB created: " <<									\
	current.adapter->getCommunicator()->proxyToString(the_vdev) << endl;

  met_qos = true;
  return AVStreams::StreamEndPointBPrx::uncheckedCast(current.adapter->addWithUUID(sep));
}

::AVStreams::StreamCtrlPrx
MMDev::bind(const ::AVStreams::MMDevicePrx& peer_device,
					 const ::AVStreams::StreamQoS& req_qos,
					 const ::AVStreams::FlowSpec& the_spec,
					 bool& met_qos,
					 ::AVStreams::StreamQoS& got_qos,
					 const Ice::Current& current) {

  ec::info() << "--- MMDevice:bind" << endl;
  StreamCtrlI* scontrol = new StreamCtrlI(_psfactory);

  AVStreams::StreamCtrlPrx prx =
	AVStreams::StreamCtrlPrx::uncheckedCast(
							  current.adapter->addWithUUID(scontrol));
  scontrol->bindDevs(AVStreams::MMDevicePrx::uncheckedCast(current.adapter->createProxy(current.id)),
					 peer_device, req_qos, the_spec, got_qos, current);
  return prx;
}

::AVStreams::StreamCtrlPrx
MMDev::bindMCast(const ::AVStreams::MMDevicePrx& first_peer,
						  const ::AVStreams::StreamQoS& req_qos,
						  const ::AVStreams::FlowSpec& the_spec,
						  bool& met_qos,
						  ::AVStreams::StreamQoS& got_qos,
						  const Ice::Current& current) {

  throw ::AVStreams::StreamOpFailed("MMDev::bindMCast not implemented!");
  return 0;
}

void
MMDev::destroy(const ::AVStreams::StreamEndPointPrx& the_ep,
			   const ::std::string& vdev_name,
			   const Ice::Current& current) {
  throw ::AVStreams::NotSupported();
}

::std::string
MMDev::addFDev(const ::Ice::ObjectPrx& the_fdev,
			   const Ice::Current& current) {
  throw ::AVStreams::NotSupported();
  return ::std::string();
}

::Ice::ObjectPrx
MMDev::getFDev(const ::std::string& flow_name,
			   const Ice::Current& current) {
  throw ::AVStreams::NotSupported();
  return NULL;
}

void
MMDev::removeFDev(const ::std::string& flow_name,
				  const Ice::Current& current) {
  throw ::AVStreams::NotSupported();
}
