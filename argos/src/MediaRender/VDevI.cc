/* -*- mode: c++; coding: utf-8 -*- */

#include <easyc++/logger.h>

#include "VDevI.hh"

using namespace std;

VDevI::VDevI(const PropertyService::Properties& capabilities,
			 const Argos::RenderApplicationPrx& the_app,
			 const PropertyService::PropertySetDefFactoryPrx& prx) :
  _renderCapabilities(capabilities), _the_app(the_app), _psfactory(prx) {}

bool
VDevI::setPeer(const ::AVStreams::StreamCtrlPrx& the_ctrl,
			   const ::AVStreams::VDevPrx& the_peer,
			   const ::AVStreams::StreamQoS& req_qos,
			   const ::AVStreams::FlowSpec& the_spec,
			   ::AVStreams::StreamQoS& got_qos,
			   const Ice::Current& ic) {

  ec::debug() << "+++ VDev::setPeer" << endl;
  PropertyService::Property config;

  _peer_dev = the_peer;

  return false;
}

bool
VDevI::setMCastPeer(const ::AVStreams::StreamCtrlPrx& the_ctrl,
					const ::AVStreams::MCastConfigIfPrx& a_mcast_config_if,
					const ::AVStreams::StreamQoS& req_qos,
					const ::AVStreams::FlowSpec& the_spec,
					::AVStreams::StreamQoS& got_qos,
					const Ice::Current& ic) {

  ec::debug() << "+++ VDev::setMCastPeer" << endl;;
  return false;
}

void
VDevI::configure(const ::PropertyService::Property& the_config_mesg,
				 const Ice::Current& current) {

  if(the_config_mesg.propertyName == "endpoint") {
	ec::debug() << "Configuration received: " << the_config_mesg.propertyValue
				<< std::endl;

	_the_app->configure(the_config_mesg);
  }

}

void
VDevI::setFormat(const ::std::string& arg1,
					const ::std::string& arg2,
					const Ice::Current& current) {}

void
VDevI::setDevParams(const ::std::string& arg1,
					   const ::PropertyService::Properties& arg2,
					   const Ice::Current& current) {}

bool
VDevI::modifyQoS(const ::AVStreams::StreamQoS& arg1,
					const ::AVStreams::FlowSpec& arg2,
					::AVStreams::StreamQoS& arg3,
					const Ice::Current& current) {
  return false;
}
