// -*- mode: c++; coding: utf-8 -*-

#ifndef MMDEV_CREATOR_HH
#define MMDEV_CREATOR_HH

#include <vector>

//#include <Ice/Ice.h>
//#include <IceStorm/IceStorm.h>

#include "Hesperia/ASDF.h"
#include "Hesperia/AVStreams.h"
#include "Argos/mediaRenderService.h"

class MMDeviceCreator : public Argos::SinkDeployer {
public:
  MMDeviceCreator(const Ice::CommunicatorPtr,
				  const Ice::ObjectAdapterPtr,
				  const PropertyService::PropertySetDefFactoryPrx&);

  virtual void addRenderer(const Argos::RenderApplicationPrx&,
						   const PropertyService::Properties&,
						   const Ice::Current&);
  virtual void delRenderer(const Argos::RenderApplicationPrx&,
						   const Ice::Current&);

private:
  Ice::CommunicatorPtr _ic;
  Ice::ObjectAdapterPtr _adapter;

  std::map<Ice::Identity, AVStreams::MMDevicePtr> _mmdevices;

  PropertyService::PropertySetDefFactoryPrx _psf;

  // Login parameters to IceGrid/Admin
  std::string _user;
  std::string _pass;

  void printAll();

};

#endif
