// -*- mode:c++; coding:utf-8 -*-

#ifndef __VDEVI_hh__
#define __VDEVI_hh__

#include <Hesperia/PropertyWrapper.h>
#include "Hesperia/AVStreams.h"
#include "Argos/mediaRenderService.h"

class VDevI : public ::AVStreams::VDev,
			  public PropertyService::PropertyWrapper {

public:
  VDevI(const PropertyService::Properties&,
		const Argos::RenderApplicationPrx&,
		const PropertyService::PropertySetDefFactoryPrx&);

  virtual bool setPeer(const ::AVStreams::StreamCtrlPrx&,
					   const ::AVStreams::VDevPrx&,
					   const ::AVStreams::StreamQoS&,
					   const ::AVStreams::FlowSpec&,
					   ::AVStreams::StreamQoS&,
					   const Ice::Current&);

  virtual bool setMCastPeer(const ::AVStreams::StreamCtrlPrx&,
							const ::AVStreams::MCastConfigIfPrx&,
							const ::AVStreams::StreamQoS&,
							const ::AVStreams::FlowSpec&,
							::AVStreams::StreamQoS&,
							const Ice::Current&);

  virtual void configure(const ::PropertyService::Property&,
						 const Ice::Current&);

  virtual void setFormat(const ::std::string&,
						 const ::std::string&,
						 const Ice::Current&);

  virtual void setDevParams(const ::std::string&,
							const ::PropertyService::Properties&,
							const Ice::Current&);

  virtual bool modifyQoS(const ::AVStreams::StreamQoS&,
						 const ::AVStreams::FlowSpec&,
						 ::AVStreams::StreamQoS&,
						 const Ice::Current&);

private:
  PropertyService::Properties _renderCapabilities;
  Argos::RenderApplicationPrx _the_app;
  PropertyService::PropertySetDefFactoryPrx _psfactory;
  AVStreams::VDevPrx _peer_dev;

};

#endif //__VDEVI_hh__


