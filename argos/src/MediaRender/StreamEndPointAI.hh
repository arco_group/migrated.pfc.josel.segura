/* -*- mode: c++; coding: utf-8 -*- */

#ifndef __STREAM_ENDPOINT_A_I_HH__
#define __STREAM_ENDPOINT_A_I_HH__

#include "StreamEndPointI.hh"

class StreamEndPointAI : virtual public ::AVStreams::StreamEndPointA,
						 public StreamEndPointI {
public:

  StreamEndPointAI(MMDev*, PropertyService::PropertySetDefFactoryPrx);

  virtual bool multiconnect(const ::AVStreams::FlowSpec&,
							const ::AVStreams::StreamQoS&,
							::AVStreams::FlowSpec&,
							::AVStreams::StreamQoS&,
							const Ice::Current&);

  virtual bool connectLeaf(const ::AVStreams::StreamEndPointBPrx&,
						   const ::AVStreams::StreamQoS&,
						   const ::AVStreams::FlowSpec&,
						   ::AVStreams::StreamQoS&,
						   const Ice::Current&);

  virtual void disconnectLeaf(const ::AVStreams::StreamEndPointBPrx&,
							  const ::AVStreams::FlowSpec&,
							  const Ice::Current&);

private:
  MMDev* _mmdev;
};

#endif // __STREAM_ENDPOINT_A_I_HH__
