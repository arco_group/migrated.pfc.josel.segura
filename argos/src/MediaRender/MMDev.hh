// -*- mode: c++; coding: utf-8 -*-

#ifndef __MMDEV_HH__
#define __MMDEV_HH__

#include <Ice/Ice.h>

#include <Argos/mediaRenderService.h>
#include <Hesperia/PropertyWrapper.h>

#include "mediarender.h"

class MMDev :
  public ::Media::Render,
  public PropertyService::PropertyWrapper {

public:
  MMDev(const Argos::RenderApplicationPrx&,
		const PropertyService::Properties&,
		const PropertyService::PropertySetDefFactoryPrx&);

  void setProxy(const ::AVStreams::MMDevicePrx&);
  ::AVStreams::MMDevicePrx getProxy();


  /* Media::Render inherits from AVStreams::MMDevice.
	 We need to implement functions from this interface */

  /* From AVStreams::MMDevice */
  ::AVStreams::StreamEndPointAPrx
  createA(const ::AVStreams::StreamCtrlPrx&,
		  const ::AVStreams::StreamQoS&,
		  const ::std::string&,
		  const ::AVStreams::FlowSpec&,
		  ::AVStreams::VDevPrx&,
		  bool&,
		  ::std::string&,
		  ::AVStreams::StreamQoS&,
		  const Ice::Current&);

  ::AVStreams::StreamEndPointBPrx
  createB(const ::AVStreams::StreamCtrlPrx&,
  		  const ::AVStreams::StreamQoS&,
  		  const ::std::string&,
  		  const ::AVStreams::FlowSpec&,
  		  ::AVStreams::VDevPrx&,
  		  bool&,
  		  ::std::string&,
  		  ::AVStreams::StreamQoS&,
  		  const Ice::Current&);

  ::AVStreams::StreamCtrlPrx
  bind(const ::AVStreams::MMDevicePrx&,
	   const ::AVStreams::StreamQoS&,
	   const ::AVStreams::FlowSpec&,
	   bool&,
	   ::AVStreams::StreamQoS&,
	   const Ice::Current&);

  ::AVStreams::StreamCtrlPrx
  bindMCast(const ::AVStreams::MMDevicePrx&,
			const ::AVStreams::StreamQoS&,
			const ::AVStreams::FlowSpec&,
			bool&,
			::AVStreams::StreamQoS&,
			const Ice::Current&);

  void destroy(const ::AVStreams::StreamEndPointPrx&,
			   const ::std::string&,
			   const Ice::Current&);

  ::std::string addFDev(const ::Ice::ObjectPrx&,
						const Ice::Current&);

  ::Ice::ObjectPrx getFDev(const ::std::string&,
						   const Ice::Current&);

  void removeFDev(const ::std::string&,
				  const Ice::Current&);

private:
  Argos::RenderApplicationPrx _app;

  PropertyService::Properties _capabilities;
  PropertyService::PropertySetDefFactoryPrx _psfactory;

  ::AVStreams::MMDevicePrx _proxy;

};

#endif // __MMDEV_HH__
