// -*- mode: c++; coding: utf-8 -*-

#ifndef __MEDIA_RENDER_H__
#define __MEDIA_RENDER_H__

#include <Ice/Ice.h>
#include <IceBox/IceBox.h>

#include "Hesperia/PropertyService.h"
#include "Argos/mediaRenderService.h"

class MediaRender : public IceBox::Service {
public:
  virtual void start(const std::string&, const Ice::CommunicatorPtr&,
					 const Ice::StringSeq&);

  void stop();

protected:
  Ice::ObjectAdapterPtr _adapter;
  Ice::ObjectAdapterPtr _localAdapter;

  Argos::SinkDeployerPtr _deployer;
  PropertyService::PropertySetDefFactoryPrx _psf;
};

#endif
