#!/usr/bin/python
# -*- coding: utf-8; mode: python -*-

import sys
import os
import logging

import Ice
from loggingx import ColorFormatter
import gobject

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)s %(message)s',
                    filename='/tmp/argosServer_{0}.log'.format(os.getpid()),
                    filemode='w')
console = logging.StreamHandler()
console.setLevel(logging.DEBUG)
console.setFormatter(ColorFormatter('%(levelname)s [%(name)s]: %(message)s'))
logging.getLogger().addHandler(console)

import factory

Ice.loadSlice('--all -I/usr/share/hesperia/slice -I{0} '
              '../slice/Argos/MMDevCreatorService.ice'.\
                  format(Ice.getSliceDir()))

import Argos

def main(args):
    l = logging.getLogger('Main')

    factory.Factory().load_plugins()

    ic = Ice.initialize(args)
    try:
        oa = ic.createObjectAdapter('ArgosServer.Adapter')

    except Ice.InitializationException:
        oa = ic.createObjectAdapterWithEndpoints('ArgosServer.Adapter',
                                                 'default')
    properties = ic.getProperties()

    transport = properties.getProperty('transport')

    if transport not in factory.Factory().get_loaded():
        l.error('Transport {0} is not loaded'.format(transport))
        ic.shutdown()
        sys.exit(1)

    plugin_config = properties.getPropertiesForPrefix(transport + '.')
    server = factory.Factory().create(transport, plugin_config)

    my_servant = Argos.DelegatedAppPrx.uncheckedCast(oa.addWithUUID(server))
    oa.activate()

    try:
        mmdev_creator = Argos.MMDeviceCreatorPrx.checkedCast(
            ic.propertyToProxy('Argos.DefaultMMDeviceCreator'))

    except Ice.Exception:
        l.error("Can't retrieve MMDeviceCreator service "
                "proxy. Quitting")

        oa.deactivate()
        ic.shutdown()
        sys.exit(1)

    if mmdev_creator == None:
        l.error("Can't retrieve MMDeviceCreator service "
                "proxy. Quitting")
        l.error(properties.getProperty('Argos.DefaultMMDeviceCreator'))

        oa.deactivate()
        ic.shutdown()
        sys.exit(1)

    l.info('MMDeviceCreator retrieved')
    mmdev_creator.addServer(my_servant, {})
    l.info('Delegated App registered')

    try:
        gobject.threads_init()
        loop = gobject.MainLoop()
        loop.run()

    except:
        pass

    finally:
        mmdev_creator.delServer(my_servant)
        oa.deactivate()
        ic.shutdown()

if __name__ == '__main__':
    main(sys.argv[1:])
    sys.exit(0)
