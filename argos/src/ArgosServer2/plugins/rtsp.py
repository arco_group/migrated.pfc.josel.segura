# -*- coding: utf-8 -*-

import logging

import gst.rtspserver
import Ice
import netifaces

import factory

Ice.loadSlice('--all -I/usr/share/hesperia/slice -I{0} '
              '../slice/Argos/MMDevCreatorService.ice '
              '/usr/share/hesperia/slice/Hesperia/PropertyService.ice'.\
                  format(Ice.getSliceDir()))

import Argos
import PropertyService
import P

def getIPs():
    ifaces = [ x for x in netifaces.interfaces()
               if not x == 'lo' and
               netifaces.ifaddresses(x).has_key(netifaces.AF_INET)]

    retval = []

    for iface in ifaces:
        retval += [ x['addr'] for x in netifaces.ifaddresses(iface)[netifaces.AF_INET] ]

    return retval

class Rtsp(Argos.DelegatedApp):
    name = 'RTSP'

    def __init__(self, config):
        self._l = logging.getLogger(Rtsp.name)

        self._l.info('Creating a RTSP server')

        self._attached = False
        self._rtsp = gst.rtspserver.Server()
        self._factory = gst.rtspserver.MediaFactory()

        port = config.get('RTSP.port', 8554)
        self._rtsp.set_property('service', port)

        try:
            path = config['RTSP.path']
            if path[0] != '/':
                path = '/' + path

            launch = config['RTSP.launch']

            self._factory.set_launch(launch)
            mapping = self._rtsp.get_media_mapping()
            mapping.add_factory(path, self._factory)

            self._config = PropertyService.Property()
            self._config.propertyName = 'endpoint'
            self._config.propertyValue = \
                P.StringT('rtsp://{0}:{1}{2}'.format(
                    getIPs()[0], port, path))

        except KeyError, key:
            # self._l.error('Missing configuration parameter: {0}'.format(key))
            raise KeyError

    def getConfiguration(self, context):
        self._l.debug('Configuration requested')
        self._l.info(self._config)

        return self._config

    def configure(self, config, context):
        self._l.debug('Configuration received')

    def play(self, context):
        self._l.debug('Play requested')

        if not self._attached:
            self._rtsp.attach()
            self._attached = True

    def stop(self, context):
        self._l.debug('Stop requested')

def load():
    factory.Factory().register(Rtsp)

