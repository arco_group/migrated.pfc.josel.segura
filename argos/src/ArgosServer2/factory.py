# -*- coding: utf-8; mode: python -*-

import logging
import os
import sys

from pyarco.Pattern import Singleton

class Factory(object):
    __metaclass__ = Singleton

    def __init__(self):
        self._l = logging.getLogger('Factory')
        self._plugins = {}

    def get_loaded(self):
        return self._plugins.keys()

    def load_plugins(self):
        plugins_path = 'plugins'
        files = set([ fname[:-3] for fname in os.listdir(plugins_path) if \
                          fname.endswith(".py") ] )

        files = files - set(['__init__'])

        if not plugins_path in sys.path:
            sys.path.append(plugins_path)

        for fname in files:
            mod = __import__(fname)

            if hasattr(mod, 'load'):
                self._l.debug('Loading {0}'.format(fname))
                mod.load()

    def register(self, cls):
        self._l.info('Registered {0}'.format(cls.name))

        self._plugins[cls.name] = cls

    def create(self, name, params=None):
        try:
            if params:
                return self._plugins[name](params)

            return self._plugins[name]()

        except KeyError:
            return None
