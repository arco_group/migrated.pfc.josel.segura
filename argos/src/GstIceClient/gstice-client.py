#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import logging

from loggingx import ColorFormatter
import Ice
import gobject
import gst
import gstice

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)s %(message)s',
                    filename='/tmp/gstice-server_%s.log' % os.getpid(),
                    filemode='w')

console = logging.StreamHandler()
#console.setLevel(logging.INFO)
console.setFormatter(ColorFormatter('%(levelname)s [%(name)s]: %(message)s'))
logging.getLogger().addHandler(console)

class GstLauncher:
    def __init__(self, description):
        self._log = logging.getLogger('Gstreamer')
        self._pipe = gst.Pipeline()

        self._log.debug("Description: '%s'" % description)

        self._src = gstice.GstIceSrc('src')

        self._log.info(self._src.get_property('receiver-proxy'))
        self._bin = gst.parse_bin_from_description(description, False)

        # self.__bin = gst.element_factory_make('autovideosink')
        self._pipe.add(self._src, self._bin)

        first_element = list(self._bin.sorted())[-1]

        sink_pad = [ x for x in first_element.get_pad_template_list() \
                         if x.direction == gst.PAD_SINK ][0]

        if sink_pad.presence == gst.PAD_ALWAYS:
            pad = first_element.get_static_pad(sink_pad.name_template)
            ghost = gst.GhostPad(sink_pad.name_template, pad)
            ghost.set_active(True)
            self._bin.add_pad(ghost)

        self._src.connect('pad-added', self._on_pad_added, self._bin)

    def _on_pad_added(self, element, pad, other):
        element.link(other)

    def play(self):
        self._pipe.set_state(gst.STATE_PLAYING)

    def receiver_proxy(self):
        return self._src.get_property('receiver-proxy')

try:
    if __name__ == '__main__':
        logging.info('Starting Gstreamer-Ice Client')

        ic = Ice.initialize(sys.argv[1:])

        prop = ic.getProperties()
        launcher = GstLauncher(prop.getProperty('gst-launch.description'))

        launcher.play()

        import IceStorm
        tpcManager = IceStorm.TopicManagerPrx.checkedCast(
            ic.stringToProxy('IceStorm/TopicManager @ IceStorm.TopicManager'))

        topic = tpcManager.retrieve('CANAL')


        prx = ic.stringToProxy(launcher.receiver_proxy())

        topic.subscribe({}, prx)
        gobject.MainLoop().run()


except KeyboardInterrupt:
    topic.unsubscribe(prx)
    logging.debug('Exit: Control+C')
    sys.exit(0)
