// -*- mode: c++; coding: utf-8 -*-

#ifndef __MMDEV_CREATOR_SERV_H__
#define __MMDEV_CREATOR_SERV_H__

#include <IceBox/IceBox.h>
#include <Ice/Ice.h>

// #include "Argos/MMDevCreatorService.h"
#include "MMDeviceCreator.hh"

class MMDeviceCreatorService : public IceBox::Service {
public:
  virtual void start(const std::string&, const Ice::CommunicatorPtr&,
					 const Ice::StringSeq&);

  void stop();

protected:
  Ice::ObjectAdapterPtr _adapter;
  Ice::ObjectAdapterPtr _localAdapter;

  MMDeviceCreator* _deployer;
  PropertyService::PropertySetDefFactoryPrx _psf;
};

#endif
