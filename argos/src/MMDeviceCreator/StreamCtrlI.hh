// -*- mode:c++; coding:utf-8 -*-


#ifndef STREAMCONTROLI_HH
#define STREAMCONTROLI_HH

#include <Hesperia/PropertyWrapper.h>
// #include <Ice/Ice.h>

#include "Hesperia/AVStreams.h"

// #include "Hesperia/PropertyService.h"

class StreamCtrlI : virtual public ::AVStreams::StreamCtrl,
		    virtual public ::AVStreams::BasicStreamCtrl,
		    virtual public PropertyService::PropertyWrapper {
public:

  StreamCtrlI(PropertyService::PropertySetDefFactoryPrx psfactory);
  ~StreamCtrlI();

  bool bindDevs(const ::AVStreams::MMDevicePrx&,
		const ::AVStreams::MMDevicePrx&,
		const ::AVStreams::StreamQoS&,
		const ::AVStreams::FlowSpec&,
		::AVStreams::StreamQoS&,
		const Ice::Current&);

  bool bind(const ::AVStreams::StreamEndPointAPrx&,
	    const ::AVStreams::StreamEndPointBPrx&,
	    const ::AVStreams::StreamQoS&,
	    const ::AVStreams::FlowSpec&,
	    ::AVStreams::StreamQoS&,
	    const Ice::Current&);

  void unbindParty(const ::AVStreams::StreamEndPointPrx&,
		   const ::AVStreams::FlowSpec&,
		   const Ice::Current&);

  void unbind(const Ice::Current&);

  void stop(const ::AVStreams::FlowSpec&,
	    const Ice::Current&);

  void start(const ::AVStreams::FlowSpec&,
	     const Ice::Current&);

  void destroy(const ::AVStreams::FlowSpec&,
	       const Ice::Current&);

  bool modifyQoS(const ::AVStreams::FlowSpec&,
		 const ::AVStreams::StreamQoS&,
		 ::AVStreams::StreamQoS&,
		 const Ice::Current&);

  void pushEvent(const ::PropertyService::Property&,
		 const Ice::Current&);

  void setFPStatus(const ::AVStreams::FlowSpec&,
		   const ::std::string&,
		   const ::AVStreams::SFPStatus&,
		   const Ice::Current&);

  ::Ice::ObjectPrx getFlowConnection(const ::std::string&,
				     const Ice::Current&);

  void setFlowConnection(const ::std::string&,
			 const ::Ice::ObjectPrx&,
			 const Ice::Current&);

  ::AVStreams::VDevPrx _vdev_a;
  ::AVStreams::VDevPrx _vdev_b;
  ::AVStreams::StreamEndPointAPrx _ste_a;
  ::AVStreams::StreamEndPointBPrx _ste_b;
};

#endif
