/* -*- mode: c++; coding: utf-8 -*- */

#ifndef __STREAMENDPOINT_I_HH__
#define __STREAMENDPOINT_I_HH__

#include <Hesperia/PropertyWrapper.h>

#include "Hesperia/AVStreams.h"
#include "MMDev.hh"

class StreamEndPointI : virtual public ::AVStreams::StreamEndPoint,
						virtual public PropertyService::PropertyWrapper {

public:

  StreamEndPointI(Argos::DelegatedAppPrx,
				  PropertyService::PropertySetDefFactoryPrx psfactory);

  virtual void stop(const ::AVStreams::FlowSpec&,
					const Ice::Current&);

  virtual void start(const ::AVStreams::FlowSpec&,
					 const Ice::Current&);

  virtual void destroy(const ::AVStreams::FlowSpec&,
					   const Ice::Current&);

  virtual bool connect(const ::AVStreams::StreamEndPointPrx&,
					   const ::AVStreams::StreamQoS&,
					   const ::AVStreams::FlowSpec&,
					   ::AVStreams::StreamQoS&,
					   const Ice::Current&);

  virtual bool requestConnection(const ::AVStreams::StreamEndPointPrx&,
								 bool,
								 const ::AVStreams::FlowSpec&,
								 const ::AVStreams::StreamQoS&,
								 ::AVStreams::FlowSpec&,
								 ::AVStreams::StreamQoS&,
								 const Ice::Current&);

  virtual bool modifyQoS(const ::AVStreams::FlowSpec&,
						 const ::AVStreams::StreamQoS&,
						 ::AVStreams::StreamQoS&,
						 const Ice::Current&);

  virtual bool setProtocolRestriction(const ::AVStreams::ProtocolSpec&,
									  const Ice::Current&);

  virtual void disconnect(const ::AVStreams::FlowSpec&,
						  const Ice::Current&);

  virtual void setFPStatus(const ::AVStreams::FlowSpec&,
						   const ::std::string&,
						   const ::AVStreams::SFPStatus&,
						   const Ice::Current&);

  virtual ::Ice::ObjectPrx getFep(const ::std::string&,
								  const Ice::Current&);

  virtual ::std::string addFep(const ::Ice::ObjectPrx&,
							   const Ice::Current&);

  virtual void removeFep(const ::std::string&,
						 const Ice::Current&);

  virtual void setNegotiator(const ::AVStreams::NegotiatorPrx&,
							 const Ice::Current&);

  virtual void setKey(const ::std::string&,
					  const ::AVStreams::Key&,
					  const Ice::Current&);

  virtual void setSourceId(::Ice::Long,
						   const Ice::Current&);

private:
  Argos::DelegatedAppPrx _represented;
  MMDev* _mmdev;

};

#endif // __STREAMENDPOINT_I_HH__
