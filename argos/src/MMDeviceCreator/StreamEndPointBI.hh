/* -*- mode: c++; coding: utf-8 -*- */

#ifndef __STREAM_ENDPOINT_B_I_HH__
#define __STREAM_ENDPOINT_B_I_HH__

#include "Hesperia/AVStreams.h"
#include "StreamEndPointI.hh"

class StreamEndPointBI : virtual public AVStreams::StreamEndPointB,
						 public StreamEndPointI {
public:

  StreamEndPointBI(MMDev*, Argos::DelegatedAppPrx, PropertyService::PropertySetDefFactoryPrx);

  virtual bool multiconnect(const AVStreams::FlowSpec&,
							const AVStreams::StreamQoS&,
							AVStreams::FlowSpec&,
							AVStreams::StreamQoS&,
							const Ice::Current&);

private:
  MMDev* _mmdev;
};

#endif // __STREAM_ENDPOINT_B_I_HH__
