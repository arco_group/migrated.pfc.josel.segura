// -*- mode: c++; coding: utf-8 -*-

#ifndef __MMDEV_HH__
#define __MMDEV_HH__

#include <Hesperia/PropertyWrapper.h>

#include "Argos/MMDevCreatorService.h"
#include "Hesperia/AVStreams.h"

class MMDev : virtual public AVStreams::MMDevice,
	      public PropertyService::PropertyWrapper {

public:
  MMDev(const Argos::DelegatedAppPrx&,
		const PropertyService::Properties&,
		const PropertyService::PropertySetDefFactoryPrx&);

  void setProxy(const AVStreams::MMDevicePrx&);
  AVStreams::MMDevicePrx getProxy();


  /* Media::Render inherits from AVStreams::MMDevice.
	 We need to implement functions from this interface */

  /* From AVStreams::MMDevice */
  virtual AVStreams::StreamEndPointAPrx
  createA(const AVStreams::StreamCtrlPrx&,
		  const AVStreams::StreamQoS&,
		  const std::string&,
		  const AVStreams::FlowSpec&,
		  AVStreams::VDevPrx&,
		  bool&,
		  std::string&,
		  AVStreams::StreamQoS&,
		  const Ice::Current&);

  virtual AVStreams::StreamEndPointBPrx
  createB(const AVStreams::StreamCtrlPrx&,
  		  const AVStreams::StreamQoS&,
  		  const std::string&,
  		  const AVStreams::FlowSpec&,
  		  AVStreams::VDevPrx&,
  		  bool&,
  		  std::string&,
  		  AVStreams::StreamQoS&,
  		  const Ice::Current&);

  virtual AVStreams::StreamCtrlPrx
  bind(const AVStreams::MMDevicePrx&,
	   const AVStreams::StreamQoS&,
	   const AVStreams::FlowSpec&,
	   bool&,
	   AVStreams::StreamQoS&,
	   const Ice::Current&);

  virtual AVStreams::StreamCtrlPrx
  bindMCast(const AVStreams::MMDevicePrx&,
			const AVStreams::StreamQoS&,
			const AVStreams::FlowSpec&,
			bool&,
			AVStreams::StreamQoS&,
			const Ice::Current&);

  virtual void destroy(const AVStreams::StreamEndPointPrx&,
			   const std::string&,
			   const Ice::Current&);

  virtual std::string addFDev(const Ice::ObjectPrx&,
						const Ice::Current&);

  virtual Ice::ObjectPrx getFDev(const std::string&,
						   const Ice::Current&);

  virtual void removeFDev(const std::string&,
				  const Ice::Current&);

private:
  Argos::DelegatedAppPrx _represented;

  PropertyService::Properties _capabilities;
  PropertyService::PropertySetDefFactoryPrx _psfactory;

  AVStreams::MMDevicePrx _proxy;

};

#endif // __MMDEV_HH__
