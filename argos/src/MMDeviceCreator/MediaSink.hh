// -*- mode: c++; coding: utf-8 -*-

#ifndef __MEDIASINK_HH__
#define __MEDIASINK_HH__

#include <Hesperia/PropertyWrapper.h>

#include "Argos/Services.h"
#include "Argos/MMDevCreatorService.h"

#include "MMDev.hh"

class MediaSink :
  public Media::Sink,
  public MMDev {

public:
  MediaSink(const Argos::DelegatedAppPrx&,
			  const PropertyService::Properties&,
			  const PropertyService::PropertySetDefFactoryPrx&);

};

#endif
