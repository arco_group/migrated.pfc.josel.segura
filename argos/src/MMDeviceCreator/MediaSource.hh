// -*- mode: c++; coding: utf-8 -*-

#ifndef __MEDIASOURCE_HH__
#define __MEDIASOURCE_HH__

#include <Hesperia/PropertyWrapper.h>

#include "Argos/Services.h"
#include "Argos/MMDevCreatorService.h"

#include "MMDev.hh"

class MediaSource :
  public Media::Source,
  public MMDev {

  DUO::ObjectPrxDict _contained;

public:
  MediaSource(const Argos::DelegatedAppPrx&,
			  const PropertyService::Properties&,
			  const DUO::ObjectPrxDict&,
			  const PropertyService::PropertySetDefFactoryPrx&);

  MediaSource(const Argos::DelegatedAppPrx&,
			  const PropertyService::Properties&,
			  const PropertyService::PropertySetDefFactoryPrx&);

  virtual DUO::ObjectPrxDict list(const Ice::Current&);

};

#endif
