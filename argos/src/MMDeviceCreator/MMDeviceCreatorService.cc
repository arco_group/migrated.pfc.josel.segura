// -*- mode: c++; coding: utf-8 -*-

#include <ec/logger.h>

#include "MMDeviceCreatorService.hh"
#include "MMDeviceCreator.hh"

using namespace std;

extern "C" {
  IceBox::Service*
  createService(Ice::CommunicatorPtr ic) {
	ec::Logger::root.set_level(ec::Logger::DEBUG);
	return new MMDeviceCreatorService();
  }
}

void
MMDeviceCreatorService::start(const string& name,
				   const Ice::CommunicatorPtr& ic,
				   const Ice::StringSeq& args) {

  ec::debug() << "[MMDevice Creator] Starting service" << endl;

  Ice::PropertiesPtr properties = ic->getProperties();

  Ice::ObjectPrx objPrx =
	ic->stringToProxy(properties->getProperty("PropertySetFactory.Proxy"));

  _psf = PropertyService::PropertySetDefFactoryPrx::checkedCast(objPrx);

  ec::debug() << "[MMDevice Creator] PropertySetDefFactory proxy: " << _psf << endl;

  try {
	_adapter = ic->createObjectAdapter("MMDeviceCreatorAdapter");
	_adapter->activate();

	_deployer = new MMDeviceCreator(ic, _adapter, _psf);
	Ice::ObjectPrx p = _adapter->add(_deployer, ic->stringToIdentity("MMDeviceCreator"));
	ec::debug() << p << std::endl;

  }

  catch(Ice::InitializationException e) {
	ec::fatal() << "[MMDevice Creator] Initialization error" << e.what() << endl;
	exit(1);
  }
}

void
MMDeviceCreatorService::stop() {
  ec::debug() << "[MMDevice Creator] Stopping..." << endl;

  _deployer->removeAll();

  if (_adapter)
	_adapter->deactivate();

  if (_localAdapter)
	_localAdapter->deactivate();
}
