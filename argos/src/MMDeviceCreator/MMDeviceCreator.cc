// -*- mode: c++; coding: utf-8 -*-

#include <Ice/Ice.h>
#include <ec/logger.h>

#include <IceGrid/Locator.h>
#include <IceGrid/Admin.h>
#include <IceGrid/Registry.h>


#include "MMDeviceCreator.hh"
#include "MediaSource.hh"
#include "MediaSink.hh"

MMDeviceCreator::MMDeviceCreator(const Ice::CommunicatorPtr communicator,
								 const Ice::ObjectAdapterPtr adapter,
								 const PropertyService::PropertySetDefFactoryPrx& psf) :
  _ic(communicator), _adapter(adapter), _psf(psf) {

  Ice::PropertiesPtr properties = _ic->getProperties();

  _user =
	properties->getProperty("Registry.Username");

  _pass =
	properties->getProperty("Registry.Password");

}

void
MMDeviceCreator::addServer(const Argos::DelegatedAppPrx& theApp,
			   const PropertyService::Properties& capabilities,
			   const Ice::Current& context) {

  Ice::Identity id = theApp->ice_getIdentity();

  MediaSource* ms = new MediaSource(theApp, capabilities, _psf);

  Media::SourcePrx prx = Media::SourcePrx::uncheckedCast
	(_adapter->add(ms, id));

  IceGrid::LocatorPrx locator = IceGrid::LocatorPrx::checkedCast
	(_ic->getDefaultLocator());

  IceGrid::AdminSessionPrx session = (locator->getLocalRegistry())->
	createAdminSession(_user, _pass);

  IceGrid::AdminPrx adminPrx = session->getAdmin();

  adminPrx->addObjectWithType(prx, AVStreams::MMDevice::ice_staticId());

  session->destroy();

  ec::debug() << "Added object: " << prx << std::endl;

  _mmdevices[id] = ms;

}


void
MMDeviceCreator::addServerWithContainer( \
	const Argos::DelegatedAppPrx& theApp,
	const DUO::ObjectPrxDict& objects,
	const PropertyService::Properties& capabilities,
	const Ice::Current& context) {

  Ice::Identity id = theApp->ice_getIdentity();

  MediaSource* ms = new MediaSource(theApp, capabilities, objects, _psf);

  Media::SourcePrx prx = Media::SourcePrx::uncheckedCast
	(_adapter->add(ms, id));

  IceGrid::LocatorPrx locator = IceGrid::LocatorPrx::checkedCast
	(_ic->getDefaultLocator());

  IceGrid::AdminSessionPrx session = (locator->getLocalRegistry())->
	createAdminSession(_user, _pass);

  IceGrid::AdminPrx adminPrx = session->getAdmin();

  adminPrx->addObjectWithType(prx, AVStreams::MMDevice::ice_staticId());

  session->destroy();

  ec::debug() << "Added object: " << prx << std::endl;

  _mmdevices[id] = ms;

}

void
MMDeviceCreator::delServer(const Argos::DelegatedAppPrx& renderer,
						   const Ice::Current& current) {

  Ice::Identity key = renderer->ice_getIdentity();

  /*
   * Unregistering the object from the Registry
   */

  IceGrid::LocatorPrx locator = IceGrid::LocatorPrx::checkedCast
	(_ic->getDefaultLocator());

  IceGrid::AdminSessionPrx session = (locator->getLocalRegistry())->
	createAdminSession(_user, _pass);

  IceGrid::AdminPrx adminPrx = session->getAdmin();

  adminPrx->removeObject(key);
  session->destroy();

  _adapter->remove(key);
  _mmdevices.erase(key);

}



void
MMDeviceCreator::addRenderer(const Argos::DelegatedAppPrx& newRenderer,
							 const PropertyService::Properties& capabilities,
							 const Ice::Current& current) {

  Ice::Identity id = newRenderer->ice_getIdentity();

  MediaSink* ms = new MediaSink(newRenderer, capabilities, _psf);

  Media::SinkPrx prx = Media::SinkPrx::uncheckedCast
	(_adapter->add(ms, id));

  ms->setProxy(prx);

  IceGrid::LocatorPrx locator = IceGrid::LocatorPrx::checkedCast
	(_ic->getDefaultLocator());

  IceGrid::AdminSessionPrx session = (locator->getLocalRegistry())->
	createAdminSession(_user, _pass);

  IceGrid::AdminPrx adminPrx = session->getAdmin();

  adminPrx->addObjectWithType(prx, AVStreams::MMDevice::ice_staticId());

  session->destroy();

  // This prx should be registered on the IceGrid Registry
  ec::debug() << "Added object: " << prx << std::endl;

  _mmdevices[id] = ms;

}

void
MMDeviceCreator::addRendererWithContainer(const Argos::DelegatedAppPrx& newRenderer,
										  const DUO::ObjectPrxDict& objs,
										  const PropertyService::Properties& capabilities,
										  const Ice::Current& current) {

  this->addRenderer(newRenderer, capabilities, current);
}

void
MMDeviceCreator::delRenderer(const Argos::DelegatedAppPrx& renderer,
							 const Ice::Current& current) {

  Ice::Identity key = renderer->ice_getIdentity();

  /*
   * Unregistering the object from the Registry
   */

  IceGrid::LocatorPrx locator = IceGrid::LocatorPrx::checkedCast
	(_ic->getDefaultLocator());

  IceGrid::AdminSessionPrx session = (locator->getLocalRegistry())->
	createAdminSession(_user, _pass);

  IceGrid::AdminPrx adminPrx = session->getAdmin();

  adminPrx->removeObject(key);
  session->destroy();

  _adapter->remove(key);
  _mmdevices.erase(key);

  ec::debug() << "Removed object for key: " << key.name << std::endl;

}

void MMDeviceCreator::removeAll() {
  ec::info() << "Removing all the MMDevices registered on the Registry" << std::endl;

  IceGrid::LocatorPrx locator = IceGrid::LocatorPrx::checkedCast
	(_ic->getDefaultLocator());

  IceGrid::AdminSessionPrx session = (locator->getLocalRegistry())->
	createAdminSession(_user, _pass);

  IceGrid::AdminPrx adminPrx = session->getAdmin();

  for(std::map<Ice::Identity, AVStreams::MMDevicePtr>::iterator \
		it=_mmdevices.begin(); it != _mmdevices.end(); it++) {

	Ice::Identity id = it->first;

	adminPrx->removeObject(id);
	_adapter->remove(id);
  }

  session->destroy();

}

