// -*- mode: c++; coding: utf-8 -*-

#include "MediaSource.hh"

MediaSource::MediaSource(const Argos::DelegatedAppPrx& app,
			 const PropertyService::Properties& capabilities,
			 const DUO::ObjectPrxDict& objects,
			 const PropertyService::PropertySetDefFactoryPrx& psf) :

  MMDev(app, capabilities, psf), _contained(objects) {

  setPropertySetDef(psf->createPropertySetDef());
}

MediaSource::MediaSource(const Argos::DelegatedAppPrx& app,
			 const PropertyService::Properties& capabilities,
			 const PropertyService::PropertySetDefFactoryPrx& psf) :

  MMDev(app, capabilities, psf) {

  setPropertySetDef(psf->createPropertySetDef());
}

DUO::ObjectPrxDict
MediaSource::list(const Ice::Current&) {
  return _contained;

}
