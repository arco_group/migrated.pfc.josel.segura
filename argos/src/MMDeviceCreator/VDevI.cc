/* -*- mode: c++; coding: utf-8 -*- */

#include <ec/logger.h>

#include "VDevI.hh"

using namespace std;

VDevI::VDevI(const PropertyService::Properties& capabilities,
			 const Argos::DelegatedAppPrx& the_app,
			 const PropertyService::PropertySetDefFactoryPrx& psf) :
  _renderCapabilities(capabilities), _the_app(the_app) {

  setPropertySetDef(psf->createPropertySetDef());
}

bool
VDevI::setPeer(const ::AVStreams::StreamCtrlPrx& the_ctrl,
			   const ::AVStreams::VDevPrx& the_peer,
			   const ::AVStreams::StreamQoS& req_qos,
			   const ::AVStreams::FlowSpec& the_spec,
			   ::AVStreams::StreamQoS& got_qos,
			   const Ice::Current& ic) {

  ec::debug() << "+++ VDev::setPeer" << endl;

  _peer_dev = the_peer;

  PropertyService::Property config;

  ec::debug() << "Reading configuration from delegated application" << endl;
  config = _the_app->getConfiguration();

  ec::debug() << "Calling other VDev configure" << endl;
  _peer_dev->configure(config);

  return true;
}

bool
VDevI::setMCastPeer(const ::AVStreams::StreamCtrlPrx& the_ctrl,
					const ::AVStreams::MCastConfigIfPrx& a_mcast_config_if,
					const ::AVStreams::StreamQoS& req_qos,
					const ::AVStreams::FlowSpec& the_spec,
					::AVStreams::StreamQoS& got_qos,
					const Ice::Current& ic) {

  ec::debug() << "+++ VDev::setMCastPeer" << endl;;

  _mc = a_mcast_config_if;

  PropertyService::Property config = _the_app->getConfiguration();
  _mc->configure(config);


  return false;
}

void
VDevI::configure(const ::PropertyService::Property& the_config_mesg,
		 const Ice::Current& current) {

  ec::debug() << "+++ VDev::configure" << endl;
  ec::debug() << "Configuration received: " << the_config_mesg.propertyName \
			  << " = " << the_config_mesg.propertyValue << std::endl;

  _the_app->configure(the_config_mesg);
}

void
VDevI::setFormat(const ::std::string& arg1,
					const ::std::string& arg2,
					const Ice::Current& current) {}

void
VDevI::setDevParams(const ::std::string& arg1,
					   const ::PropertyService::Properties& arg2,
					   const Ice::Current& current) {}

bool
VDevI::modifyQoS(const ::AVStreams::StreamQoS& arg1,
					const ::AVStreams::FlowSpec& arg2,
					::AVStreams::StreamQoS& arg3,
					const Ice::Current& current) {
  return false;
}
