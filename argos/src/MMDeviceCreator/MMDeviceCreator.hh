// -*- mode: c++; coding: utf-8 -*-

#ifndef MMDEV_CREATOR_HH
#define MMDEV_CREATOR_HH

#include "Argos/MMDevCreatorService.h"
#include "Hesperia/AVStreams.h"

class MMDeviceCreator : public Argos::MMDeviceCreator {
public:
  MMDeviceCreator(const Ice::CommunicatorPtr,
				  const Ice::ObjectAdapterPtr,
				  const PropertyService::PropertySetDefFactoryPrx&);

  virtual void addServer(const Argos::DelegatedAppPrx&,
						 const PropertyService::Properties&,
						 const Ice::Current&);

  virtual void addServerWithContainer(const Argos::DelegatedAppPrx&,
									  const DUO::ObjectPrxDict&,
									  const PropertyService::Properties&,
									  const Ice::Current&);

  virtual void delServer(const Argos::DelegatedAppPrx&,
						 const Ice::Current&);

  virtual void addRenderer(const Argos::DelegatedAppPrx&,
						   const PropertyService::Properties&,
						   const Ice::Current&);

  virtual void addRendererWithContainer(const Argos::DelegatedAppPrx&,
										const DUO::ObjectPrxDict&,
										const PropertyService::Properties&,
										const Ice::Current&);

  virtual void delRenderer(const Argos::DelegatedAppPrx&,
						   const Ice::Current&);

  /* Non-published methods */
  void removeAll();

private:
  Ice::CommunicatorPtr _ic;
  Ice::ObjectAdapterPtr _adapter;

  std::map<Ice::Identity, AVStreams::MMDevicePtr> _mmdevices;

  PropertyService::PropertySetDefFactoryPrx _psf;

  // Login parameters to IceGrid/Admin
  std::string _user;
  std::string _pass;

};

#endif
