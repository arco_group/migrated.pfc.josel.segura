/* -*- mode: c++; coding: utf-8 -*- */

#include "StreamEndPointAI.hh"

StreamEndPointAI::StreamEndPointAI(MMDev* mmdev,
								   Argos::DelegatedAppPrx delegated,
								   PropertyService::PropertySetDefFactoryPrx psfactory) :
  StreamEndPointI::StreamEndPointI(delegated, psfactory),
  _mmdev(mmdev) { }

bool
StreamEndPointAI::multiconnect(const AVStreams::FlowSpec& req_spec,
				  const AVStreams::StreamQoS& req_qos,
				  AVStreams::FlowSpec& got_spec,
				  AVStreams::StreamQoS& got_qos,
				  const Ice::Current& current) {
  return false;
}

bool
StreamEndPointAI::connectLeaf(const AVStreams::StreamEndPointBPrx& the_ep,
				 const AVStreams::StreamQoS& req_qos,
				 const AVStreams::FlowSpec& the_flows,
				 AVStreams::StreamQoS& got_qos,
				 const Ice::Current& current) {

  _mcastConnectedLeafs[the_ep->ice_getIdentity()] = the_ep;

  return true;
}

void
StreamEndPointAI::disconnectLeaf(const AVStreams::StreamEndPointBPrx& the_ep,
				    const AVStreams::FlowSpec& the_spec,
				    const Ice::Current& current) {

  std::map<Ice::Identity, AVStreams::StreamEndPointBPrx>::iterator it =	\
	_mcastConnectedLeafs.find(the_ep->ice_getIdentity());

  if (it != _mcastConnectedLeafs.end())
	_mcastConnectedLeafs.erase(it);
}

