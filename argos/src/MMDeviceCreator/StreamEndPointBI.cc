/* -*- mode: c++; coding: utf-8 -*- */

#include "StreamEndPointBI.hh"

StreamEndPointBI::StreamEndPointBI(MMDev* mmdev,
								   Argos::DelegatedAppPrx delegated,
								   PropertyService::PropertySetDefFactoryPrx psfactory) :
  StreamEndPointI::StreamEndPointI(delegated, psfactory),
  _mmdev(mmdev) { }

bool
StreamEndPointBI::multiconnect(const ::AVStreams::FlowSpec& req_spec,
				  const ::AVStreams::StreamQoS& req_qos,
				  ::AVStreams::FlowSpec& got_spec,
				  ::AVStreams::StreamQoS& got_qos,
				  const Ice::Current& current) {
  return false;
}

