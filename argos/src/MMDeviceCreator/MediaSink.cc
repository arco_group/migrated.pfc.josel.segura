// -*- mode: c++; coding: utf-8 -*-

#include "MediaSink.hh"

MediaSink::MediaSink(const Argos::DelegatedAppPrx& app,
		     const PropertyService::Properties& capabilities,
		     const PropertyService::PropertySetDefFactoryPrx& psf) :
  MMDev(app, capabilities, psf) {

  setPropertySetDef(psf->createPropertySetDef());
}
