#!/usr/bin/python
# -*- mode: python; coding: utf-8 -*-

import sys
import Ice

HESPERIA_SLICE_DIR = '/usr/share/hesperia/slice'

Ice.loadSlice('--all -I%s -I%s %s/Hesperia/AVStreams.ice' % (Ice.getSliceDir(),
                                                             HESPERIA_SLICE_DIR,
                                                             HESPERIA_SLICE_DIR))

Ice.loadSlice('--all -I%s -I%s %s/Hesperia/PropertyService.ice' % (
        Ice.getSliceDir(), HESPERIA_SLICE_DIR, HESPERIA_SLICE_DIR))

import AVStreams
from PropertyService import Property

class StreamCtrl(AVStreams.StreamCtrl):
    def bindDevs(self, aParty, bParty, reqQos, theFlows, current=None):
        sep_a, vdev_a, metQosA, namedVDevA, gotQosA = aParty.createA(
            self.prx, reqQos, "", theFlows)

        sep_b, vdev_b, metQosB, namedVDevB, gotQosB = bParty.createB(
            self.prx, reqQos, "", theFlows)

        try:
            retval, got_qos = vdev_a.setPeer(self.prx, vdev_b, reqQos,
                                             theFlows)
        except Exception, e:
            raise e

        try:
            retval, got_qos = vdev_b.setPeer(self.prx, vdev_a, reqQos,
                                             theFlows)
        except Exception, e:
            raise e

        return (True, None)


class MMDevice(AVStreams.MMDevice):
    def createA(self, the_requester, req_qos, req_named_vdev, the_spec, current=None):
        sep = StreamEndPointA(self)
        vdev = VDev()
        base = current.adapter.addWithUUID(vdev)
        vdev_prx = AVStreams.VDevPrx.uncheckedCast(base)
        sep_prx = AVStreams.StreamEndPointAPrx.uncheckedCast(current.adapter.addWithUUID(sep))

        return (sep_prx, vdev_prx, True, "Test.VDev", None)

    def createB(self, the_requester, req_qos, req_named_vdev, the_spec, current=None):
        sep = StreamEndPointB(self)
        vdev = VDev(self)
        base = current.adapter.addWithUUID(vdev)
        vdev_prx = AVStreams.VDevPrx.uncheckedCast(base)
        sep_prx = AVStreams.StreamEndPointBPrx.uncheckedCast(current.adapter.addWithUUID(sep))

        return (sep_prx, vdev_prx, True, "Test.VDev", None)


class StreamEndPoint(AVStreams.StreamEndPoint):
    def __init__(self, dev):
        self.mmdev = dev


    def start(self, the_spec, current=None):
        self.mmdev.play()


    def stop(self, the_spec, current=None):
        self.mmdev.stop()


class StreamEndPointA(AVStreams.StreamEndPointA, StreamEndPoint):
    def __init__(self, dev):
        StreamEndPoint.__init__(self, dev)


class StreamEndPointB(AVStreams.StreamEndPointB, StreamEndPoint):
    def __init__(self, dev):
        StreamEndPoint.__init__(self, dev)


class VDev(AVStreams.VDev):
    def __init__(self):
        AVStreams.VDev.__init__(self)

    def setPeer(self, the_ctrl, the_peer_dev, req_qos, the_spec, current=None):
        self.peer = the_peer_dev

        p = Property()
        self.peer.configure(p)

        return (True, None)

    def configure(self, the_config_mesg, current=None):
        print the_config_mesg.propertyName, '->', the_config_mesg.propertyValue.value


class MMDevConnectionTester(Ice.Application):
    def __init__(self):
        Ice.Application.__init__(self)

    def run(self, args):
        ic = self.communicator()
        adapter = ic.createObjectAdapterWithEndpoints('TestAdapter', 'default')
        adapter.activate()

        mmdev = MMDevice()
        mmdev.prx = AVStreams.MMDevicePrx.checkedCast(adapter.addWithUUID(mmdev))

        other_mmdev = AVStreams.MMDevicePrx.checkedCast(ic.stringToProxy(
                args[1]))

        strCtrl = StreamCtrl()
        strCtrl.prx = AVStreams.StreamCtrlPrx.checkedCast(adapter.addWithUUID(strCtrl))

        result, other = strCtrl.bindDevs(mmdev.prx, other_mmdev, [], [])

        # not needed
        #ic.waitForShutdown()

app = MMDevConnectionTester()

try:
    app.main(sys.argv)

except:
    sys.exit(1)

sys.exit(0)
