#!/usr/bin/python
# -*- mode: python; coding: utf-8 -*-

import sys
import Ice

HESPERIA_SLICE_DIR = '/usr/share/hesperia/slice'

Ice.loadSlice('--all -I%s %s/Hesperia/DUO.ice' % (Ice.getSliceDir(),
                                                  HESPERIA_SLICE_DIR))
import DUO

class ContainerListTester(Ice.Application):
    def __init__(self):
        Ice.Application.__init__(self)

    def run(self, args):
        ic = self.communicator()
        adapter = ic.createObjectAdapterWithEndpoints('TestAdapter', 'default')
        adapter.activate()

        container = DUO.Container.RPrx.checkedCast((ic.stringToProxy(args[1])))

        lista = container.list()
        print lista

        return

app = ContainerListTester()
app.main(sys.argv)

sys.exit(0)
