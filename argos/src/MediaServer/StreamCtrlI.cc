// -*- mode: c++; coding: utf-8 -*-

#include <ec/logger.h>

#include "StreamCtrlI.h"
#include <Hesperia/PropertyWrapper.h>

using namespace std;

StreamCtrlI::StreamCtrlI(PropertyService::PropertySetDefFactoryPrx psfactory) {
  ec::info() << "--- StreamCtrl:create" << endl;

  // Set PropertySetDef to PropertyWrapper
  setPropertySetDef(psfactory->createPropertySetDef());

  _vdev_a = 0;
  _vdev_b = 0;
  _ste_a = 0;
  _ste_b = 0;
}

bool
StreamCtrlI::bindDevs(const ::AVStreams::MMDevicePrx& a_party,
					  const ::AVStreams::MMDevicePrx& b_party,
					  const ::AVStreams::StreamQoS& req_qos,
					  const ::AVStreams::FlowSpec& the_flows,
					  ::AVStreams::StreamQoS& got_qos,
					  const Ice::Current& current) {

  ec::info() << "--- StreamCtrl:bindDevs" << endl;
  bool retval = false;
  string name_a, name_b; // Not used
  ::AVStreams::StreamCtrlPrx this_prx;

  /*
   * Create peers
   */
  if (_vdev_a == 0) {
    this_prx = AVStreams::StreamCtrlPrx::checkedCast(current.adapter->addWithUUID(this));
    _ste_a = a_party->createA(this_prx, req_qos, name_a ,the_flows, _vdev_a, retval, name_a, got_qos);
	ec::info() << "--- StreamCtrlI:bindDevs aParty created!" << endl;
  }

  if (_vdev_b == 0 && retval) {
    _ste_b = b_party->createB(this_prx, req_qos, name_b, the_flows, _vdev_b, retval, name_b, got_qos);
	ec::info() << "--- StreamCtrlI:bindDevs bParty created!" << endl;
  }

  /*
   *Configure Peers
   */
  if (_vdev_a->setPeer(this_prx, _vdev_b, req_qos, the_flows, got_qos)) {
    // if _vdev_b is compatible with _vdev_a
  }

  ec::info() << "--- VDevA setPeer to VDevB" << endl;

  if (_vdev_b->setPeer(this_prx, _vdev_a, req_qos, the_flows, got_qos)) {
    // If _devA is compatible with _vdev_b
  }

  ec::info() << "--- VDevB setPeer to VDevA" << endl;

  return retval;
}

// Not used
bool
StreamCtrlI::bind(const ::AVStreams::StreamEndPointAPrx& a_party,
		  const ::AVStreams::StreamEndPointBPrx& b_party,
		  const ::AVStreams::StreamQoS& req_qos,
		  const ::AVStreams::FlowSpec& the_flows,
		  ::AVStreams::StreamQoS& got_qos,
		  const Ice::Current& current) {

  throw ::AVStreams::StreamOpFailed("StreamCtrI::bind not implemented");
  return false;
}

// Not used
void
StreamCtrlI::unbindParty(const ::AVStreams::StreamEndPointPrx& the_ep,
			 const ::AVStreams::FlowSpec& the_spec,
			 const Ice::Current& current) {

  throw ::AVStreams::StreamOpFailed("StreamCtrI::unBindParty not implemented");
}

// Not used
void
StreamCtrlI::unbind(const Ice::Current& current) {

  throw ::AVStreams::StreamOpFailed("StreamCtrI::unBind not implemented");
}


void
StreamCtrlI::stop(const ::AVStreams::FlowSpec& f_spec,
		  const Ice::Current& current) {

  // Debug
  ec::debug() << "--- StreamCtrl:stop" << endl;

  _ste_a->stop(f_spec);
  _ste_b->stop(f_spec);
}

void
StreamCtrlI::start(const ::AVStreams::FlowSpec& f_spec,
		   const Ice::Current& current) {

  // Debug
  ec::debug() << "--- StreamCtrl:start" << endl;

  _ste_b->start(f_spec);
  _ste_a->start(f_spec);
}

void
StreamCtrlI::destroy(const ::AVStreams::FlowSpec& f_spec,
		     const Ice::Current& current) {

}

// Not used
bool
StreamCtrlI::modifyQoS(const ::AVStreams::FlowSpec& the_spec,
		       const ::AVStreams::StreamQoS& req_qos,
		       ::AVStreams::StreamQoS& got_qos,
		       const Ice::Current& current) {

  throw ::AVStreams::QoSRequestFailed("StreamCtrl::modifyQoS not implemented!");
  return false;
}

void
StreamCtrlI::pushEvent(const ::PropertyService::Property& the_event,
                                       const Ice::Current& current) {
}

void
StreamCtrlI::setFPStatus(const ::AVStreams::FlowSpec& the_spec,
			 const ::std::string& fp_name,
			 const ::AVStreams::SFPStatus& fp_settings,
			 const Ice::Current& current) {
}

// Not used
::Ice::ObjectPrx
StreamCtrlI::getFlowConnection(const ::std::string& flow_name,
			       const Ice::Current& current) {

  throw ::AVStreams::NoSuchFlow();
  return 0;
}

// Not used
void
StreamCtrlI::setFlowConnection(const ::std::string& flow_name,
			       const ::Ice::ObjectPrx& flow_connection,
			       const Ice::Current& current) {

  throw ::AVStreams::NoSuchFlow();
}
