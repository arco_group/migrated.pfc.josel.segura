// -*- mode: c++; coding: utf-8 -*-

#include <ec/logger.h>

#include "MMDevContainer.h"
#include "StreamEndPointAI.h"
#include "StreamEndPointBI.h"
#include "StreamCtrlI.h"

#include "Factory.h"
#include "axis/AxisFactory.h"

using namespace std;

MMDevContainer::MMDevContainer(const map<string, string>& config,
			       const Ice::ObjectAdapterPtr& adapter,
			       PropertyService::PropertySetDefFactoryPrx prx):
  _adapter(adapter), _psfactory(prx), _config(config) {

  ec::info() << "MMDevContainer created" << endl;

  setPropertySetDef(_psfactory->createPropertySetDef());

  if (_config["plugin"].compare("axis") == 0) {
    ec::info() << "Retrieved Axis factory" << endl;
    _factory = AxisFactory::getInstance();
  }
  
  _container = _factory->createContainer(config, _adapter, _psfactory);
  ec::info() << "Container retrieved from Factory" << endl;

}

AVStreams::StreamEndPointAPrx
MMDevContainer::createA(const AVStreams::StreamCtrlPrx& the_requester,
			const AVStreams::StreamQoS& req_qos,
			const std::string& req_named_vdev,
			const AVStreams::FlowSpec& the_spec,
			AVStreams::VDevPrx& the_vdev,
			bool& met_qos,
			std::string& got_named_vdev,
			AVStreams::StreamQoS& got_qos,
			const Ice::Current& current) {

  ec::info() << "--- MMDevice:createA" << endl;

  StreamEndPointAI* sep = new StreamEndPointAI(this, _psfactory);

  AVStreams::VDevPtr vdev = _factory->createVDev(_config, _psfactory);
  the_vdev = AVStreams::VDevPrx::checkedCast(current.adapter->addWithUUID(vdev));
  ec::debug() << "VDevA created: " << \
    current.adapter->getCommunicator()->proxyToString(the_vdev) << endl;

  met_qos = true;
  return AVStreams::StreamEndPointAPrx::uncheckedCast(current.adapter->addWithUUID(sep));
}

AVStreams::StreamEndPointBPrx
MMDevContainer::createB(const AVStreams::StreamCtrlPrx& the_requester,
			const AVStreams::StreamQoS& req_qos,
			const std::string& req_named_vdev,
			const AVStreams::FlowSpec& the_spec,
			AVStreams::VDevPrx& the_vdev,
			bool& met_qos,
			std::string& got_named_vdev,
			AVStreams::StreamQoS& got_qos,
			const Ice::Current& current) {

  ec::info() << "--- MMDevice:createB" << endl;

  StreamEndPointBI* sep = new StreamEndPointBI(this, _psfactory);
  // Factory factory::make_vdev(_config, _psfactory);
  // the_vdev = AVStreams::VDevPrx::uncheckedCast(current.adapter->addWithUUID(vdev));
  ec::debug() << "VDevB created: " << \
    current.adapter->getCommunicator()->proxyToString(the_vdev) \
	      << endl;

  met_qos = true;
  return AVStreams::StreamEndPointBPrx::uncheckedCast(current.adapter->addWithUUID(sep));
}

AVStreams::StreamCtrlPrx
MMDevContainer::bind(const AVStreams::MMDevicePrx& peer_device,
		     const AVStreams::StreamQoS& req_qos,
		     const AVStreams::FlowSpec& the_spec,
		     bool& met_qos,
		     AVStreams::StreamQoS& got_qos,
		     const Ice::Current& current) {

  ec::info() << "--- MMDevice:bind" << endl;
  StreamCtrlI* scontrol = new StreamCtrlI(_psfactory);
  AVStreams::StreamCtrlPrx prx =
    AVStreams::StreamCtrlPrx::uncheckedCast(
					    current.adapter->addWithUUID(scontrol));
  scontrol->bindDevs(AVStreams::MMDevicePrx::uncheckedCast(current.adapter->createProxy(current.id)),
		     peer_device, req_qos, the_spec, got_qos, current);
  return prx;
}

AVStreams::StreamCtrlPrx
MMDevContainer::bindMCast(const AVStreams::MMDevicePrx& first_peer,
			  const AVStreams::StreamQoS& req_qos,
			  const AVStreams::FlowSpec& the_spec,
			  bool& met_qos,
			  AVStreams::StreamQoS& got_qos,
			  const Ice::Current& current) {

  throw AVStreams::StreamOpFailed("MMDevContainer::bindMCast not implemented!");
  return 0;
}

void
MMDevContainer::destroy(const AVStreams::StreamEndPointPrx& the_ep,
			const std::string& vdev_name,
			const Ice::Current& current) {
  throw AVStreams::NotSupported();
}

std::string
MMDevContainer::addFDev(const Ice::ObjectPrx& the_fdev,
			const Ice::Current& current) {
  throw AVStreams::NotSupported();
  return std::string();
}

Ice::ObjectPrx
MMDevContainer::getFDev(const std::string& flow_name,
			const Ice::Current& current) {
  throw AVStreams::NotSupported();
  return NULL;
}

void
MMDevContainer::removeFDev(const std::string& flow_name,
			   const Ice::Current& current) {
  throw AVStreams::NotSupported();
}

DUO::ObjectPrxDict
MMDevContainer::list(const Ice::Current& current) {
  ec::debug() << "list():" << endl;
  
  return _container->list();
}
