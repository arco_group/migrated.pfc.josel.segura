// -*- coding: utf-8 -*-

#include <iostream>

#include <Ice/Ice.h>
#include <ec/logger.h>

#include "v4l2MediaServer.hh"

using namespace std;

extern "C" {
  IceBox::Service*
  createAxisMediaServer(Ice::CommunicatorPtr ic) {
    return new V4L2MediaServer(ic);
  }
}

V4L2MediaServer::V4L2MediaServer(const Ice::CommunicatorPtr& ic) {
  ec::info() << "*** Creating V4L2 Media Server" << endl;
}

void
V4L2MediaServer::start(const std::string& name,
					   const Ice::CommunicatorPtr& ic,
					   const Ice::StringSeq& args) {
  ec::info() << "*** Starting V4L2 MediaServer" << endl;

  Ice::PropertiesPtr properties = ic->getProperties();

  try {
    _adapter = ic->createObjectAdapter("V4L2MediaServerAdapter");
	_adapter->activate();
  }

  catch (IceUtil::Exception e) {
	ec::fatal() << e.what() << endl;
	exit(1);
  }

  map<string,string> config;

  config.insert(make_pair("ip", properties->getPropertyWithDefault("V4L2MediaServer.Device", "/dev/video0")));

  _psf = PropertyService::PropertySetDefFactoryPrx::
	checkedCast(ic->stringToProxy(properties->getProperty("PropertySetFactory.Proxy")));

  string identity = "V4L2";

  _mmdevcont = new MMDevContainer("v4l2", config, _psf);
  Ice::ObjectPrx prx = _adapter->add(_mmdevcont,
								   ic->stringToIdentity(identity));

  /* Use the prx object to register it on the Registry and put it as well-known object
   * See MediaRender for reference
   */

}
