/* -*- mode: c++; coding: utf-8 -*- */

#include <iostream>
#include <sstream>
#include <map>

#include "VDevFactory.hh"

// Media factory/driver
class V4L2VDev : public VDevFactory {
private:
  class dummy {};

  V4L2VDev(const dummy& d);
  V4L2VDev(const std::map<std::string, std::string>&,
		   PropertyService::PropertySetDefFactoryPrx);

  static V4L2VDev v4l2_proto;
  PropertyService::PropertySetDefFactoryPrx _psfactory;

  // Input vars
  std::string _device;
  std::string _input;

  std::map<std::string, std::string> _endpoints;


public:
  ~V4L2VDev();

  // Pattern prototype
  VDevFactory* clone(const std::map<std::string, std::string>&,
					 PropertyService::PropertySetDefFactoryPrx);

  std::string getName();

  // AVStreams::VDev
  virtual bool setPeer(const ::AVStreams::StreamCtrlPrx&,
		       const ::AVStreams::VDevPrx&,
		       const ::AVStreams::StreamQoS&,
		       const ::AVStreams::FlowSpec&,
		       ::AVStreams::StreamQoS&,
		       const Ice::Current&);

  virtual bool setMCastPeer(const ::AVStreams::StreamCtrlPrx&,
			    const ::AVStreams::MCastConfigIfPrx&,
			    const ::AVStreams::StreamQoS&,
			    const ::AVStreams::FlowSpec&,
			    ::AVStreams::StreamQoS&,
			    const Ice::Current&);

  virtual void configure(const ::PropertyService::Property&,
			 const Ice::Current&);

  virtual void setFormat(const ::std::string&,
			 const ::std::string&,
			 const Ice::Current&);

  virtual void setDevParams(const ::std::string&,
			    const ::PropertyService::Properties&,
			    const Ice::Current&);

  virtual bool modifyQoS(const ::AVStreams::StreamQoS&,
			 const ::AVStreams::FlowSpec&,
			 ::AVStreams::StreamQoS&,
			 const Ice::Current&);
};
