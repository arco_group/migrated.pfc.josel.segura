// -*- coding: utf-8 -*-

#include "MediaServer.hh"
#include "MMDevContainer.hh"

class V4L2MediaServer : public MediaServer {
public:
  V4L2MediaServer(const Ice::CommunicatorPtr &);

  void start(const std::string&, const Ice::CommunicatorPtr&,
			 const Ice::StringSeq&);

private:
  MMDevContainer* _mmdevcont;

};
