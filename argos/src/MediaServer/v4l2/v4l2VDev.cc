/* -*- mode: c++; coding: utf-8 -*- */

#include <ec/logger.h>
#include "v4l2VDev.hh"

using namespace std;

/*************************************
 * VDev factory/driver
 *************************************/
V4L2VDev::V4L2VDev(const dummy& d) {
  VDevFactory::reg("v4l2", this);
}

V4L2VDev V4L2VDev::v4l2_proto(*new dummy());

V4L2VDev::V4L2VDev(const std::map<std::string, std::string>& config,
				   PropertyService::PropertySetDefFactoryPrx prx):
  _psfactory(prx) {

  _device = config.find("device")->second;
  _input  = config.find("input")->second;
}

V4L2VDev::~V4L2VDev() {
  VDevFactory::unreg("v4l2");
}

// Prototype (for factory)
VDevFactory*
V4L2VDev::clone(const map<string, string>& config,
				PropertyService::PropertySetDefFactoryPrx prx) {
  return new V4L2VDev(config, prx);
}

/* AVStreams VDev functions */
bool V4L2VDev::setPeer(const ::AVStreams::StreamCtrlPrx& the_ctrl,
					   const ::AVStreams::VDevPrx& the_peer,
					   const ::AVStreams::StreamQoS& req_qos,
					   const ::AVStreams::FlowSpec& the_spec,
					   ::AVStreams::StreamQoS& got_qos,
					   const Ice::Current& ic) {

  ec::debug() << "+++ VDev::setPeer" << endl;

  // Set the transcode vars
  std::string vCodec   = "mp4v";
  std::string vBitRate = "4096";
  std::string fps      = "25";
  std::string keyint   = "25";
  std::string aCodec   = "mpga";
  std::string aBitRate = "256";

  std::string transcode = "'#transcode{vcodec=" + vCodec + ",vb=" + vBitRate + \
	",fps=" + fps + ",keyint=" + keyint + ",acodec=" + aCodec + ",ab=" + aBitRate + \
	",deinterlace}:rtp{sdp=rtsp://0.0.0.0:" + port + "/video0.sdp}'";


  PropertyService::Property config;

  _peer_dev = the_peer;

  config.propertyName = "endpoint";
  config.propertyValue = new ::P::StringT(_endpoints["rtsp"]);

  _peer_dev->configure(config);

  return false;
}

bool V4L2VDev::setMCastPeer(
			   const ::AVStreams::StreamCtrlPrx& the_ctrl,
			   const ::AVStreams::MCastConfigIfPrx& a_mcast_config_if,
			   const ::AVStreams::StreamQoS& req_qos,
			   const ::AVStreams::FlowSpec& the_spec,
			   ::AVStreams::StreamQoS& got_qos,
			   const Ice::Current& ic) {

  ec::debug() << "+++ VDev::setMCastPeer" << endl;;
  return false;
}

void
V4L2VDev::configure(const ::PropertyService::Property& property,
					const Ice::Current& current) {}

void
V4L2VDev::setFormat(const ::std::string& arg1,
					const ::std::string& arg2,
					const Ice::Current& current) {}

void
V4L2VDev::setDevParams(const ::std::string& arg1,
					   const ::PropertyService::Properties& arg2,
					   const Ice::Current& current) {}

bool
V4L2VDev::modifyQoS(const ::AVStreams::StreamQoS& arg1,
					const ::AVStreams::FlowSpec& arg2,
					::AVStreams::StreamQoS& arg3,
					const Ice::Current& current) {}
