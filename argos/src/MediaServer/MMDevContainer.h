// -*- mode: c++; coding: utf-8 -*-

#ifndef __MMDEVCONTAINER_H__
#define __MMDEVCONTAINER_H__

#include <Ice/Ice.h>
#include <Hesperia/PropertyWrapper.h>

#include "Argos/Services.h"

#include "Factory.h"


class MMDevContainer :
  public Media::Source,
  public PropertyService::PropertyWrapper {

public:
  MMDevContainer(const std::map<std::string, std::string>&,
		 const Ice::ObjectAdapterPtr&,
		 PropertyService::PropertySetDefFactoryPrx);

  /* Media::Server inherits from AVStreams::MMDevice and from
     DUO::Container::R. We need to implement functions from this
     interfaces */

  /* From AVStreams::MMDevice */
  AVStreams::StreamEndPointAPrx
  createA(const AVStreams::StreamCtrlPrx&,
	  const AVStreams::StreamQoS&,
	  const std::string&,
	  const AVStreams::FlowSpec&,
	  AVStreams::VDevPrx&,
	  bool&,
	  std::string&,
	  AVStreams::StreamQoS&,
	  const Ice::Current&);

  AVStreams::StreamEndPointBPrx
  createB(const AVStreams::StreamCtrlPrx&,
	  const AVStreams::StreamQoS&,
	  const std::string&,
	  const AVStreams::FlowSpec&,
	  AVStreams::VDevPrx&,
	  bool&,
	  std::string&,
	  AVStreams::StreamQoS&,
	  const Ice::Current&);

  AVStreams::StreamCtrlPrx
  bind(const AVStreams::MMDevicePrx&,
       const AVStreams::StreamQoS&,
       const AVStreams::FlowSpec&,
       bool&,
       AVStreams::StreamQoS&,
       const Ice::Current&);

  AVStreams::StreamCtrlPrx
  bindMCast(const AVStreams::MMDevicePrx&,
	    const AVStreams::StreamQoS&,
	    const AVStreams::FlowSpec&,
	    bool&,
	    AVStreams::StreamQoS&,
	    const Ice::Current&);

  void destroy(const AVStreams::StreamEndPointPrx&,
	       const std::string&,
	       const Ice::Current&);

  std::string addFDev(const Ice::ObjectPrx&,
			const Ice::Current&);

  Ice::ObjectPrx getFDev(const std::string&,
			   const Ice::Current&);

  void removeFDev(const std::string&,
		  const Ice::Current&);

  /* From DUO::Container::R */
  DUO::ObjectPrxDict list(const Ice::Current&);

private:
  Ice::ObjectAdapterPtr _adapter;
  PropertyService::PropertySetDefFactoryPrx _psfactory;

  std::map<std::string, std::string> _config;


  Factory *_factory;

  DUO::Container::RPtr _container;
  DUO::ObjectPrxDict _elements;

};

#endif // __MMDEVCONTAINER_HH__
