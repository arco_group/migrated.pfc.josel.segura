// -*- coding: utf-8; mode: c++ -*-

#ifndef AXISHTTPAPI_H__
#define AXISHTTPAPI_H__

#include <curl/curl.h>
#include <map>
#include <string>
#include <vector>

class AxisHTTPAPI {

public:
  AxisHTTPAPI();
  AxisHTTPAPI(const std::map<std::string, std::string>&);
  float getPan();
  float getTilt();
  int getZoom();
  bool getAutoFocus();
  bool getAutoIris();

  void setPan(float);
  void setTilt(float);
  void setZoom(int);
  void setAutoFocus(bool);
  void setAutoIris(bool);
  void goHome();

  std::vector<unsigned char> getSnapshot();
  std::vector<unsigned char> getSnapshot(const std::map<std::string,std::string>&);

private:
  CURL* rebuildContext();
  std::string queryPTZ(const std::string&);
  void sendPTZ(const std::string&);

  std::string _base_url; // http://[user:password@]hostname/

  CURL* _curlContext;
  char* _ptzFilename;
  char* _snapshotFilename;

};


#endif
