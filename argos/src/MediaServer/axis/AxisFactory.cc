// -*- mode: c++; coding: utf-8

#include <Ice/Ice.h>
#include <ec/logger.h>


#include "AxisFactory.h"
#include "AxisVDev.h"
#include "AxisContainer.h"

using namespace std;
using namespace PropertyService;

AxisFactory *AxisFactory::_instance = 0;

AxisFactory* AxisFactory::getInstance() {
  if (_instance == 0)
    AxisFactory::_instance = new AxisFactory;

  return AxisFactory::_instance;
}

AVStreams::VDevPtr AxisFactory::createVDev(const map<string, string>& config,
					   PropertySetDefFactoryPrx ps) {
  AVStreams::VDevPtr vdev = new AxisVDev(config, ps);
  return vdev;
  
}

DUO::Container::RPtr AxisFactory::createContainer(const map<string, string>& config,
						  const Ice::ObjectAdapterPtr& adapter,
						  PropertySetDefFactoryPrx ps) {

  ec::info() << "Creating Axis Container" << endl;
  DUO::Container::RPtr container = new AxisContainer(config, adapter, ps);
  return container;
}

