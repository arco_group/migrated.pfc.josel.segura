/* -*- mode: c++; coding: utf-8 -*- */

#ifndef _AXISCONTAINER_H_
#define _AXISCONTAINER_H_

#include <Ice/Ice.h>
#include <IceStorm/IceStorm.h>

#include <Hesperia/PropertyWrapper.h>

#include "AxisHTTPAPI.h"

#include "media.h"

class AxisContainer : public DUO::Container::R {

public:
  AxisContainer(const std::map<std::string,std::string>&,
		const Ice::ObjectAdapterPtr&,
		PropertyService::PropertySetDefFactoryPrx&);

  virtual DUO::ObjectPrxDict list(const Ice::Current&);

private:
  DUO::ObjectPrxDict _objs;
  
};

class Snapshot : public Media::IByteSeq::R,
		 public PropertyService::PropertyWrapper {
public:
  Snapshot(const AxisHTTPAPI&, PropertyService::PropertySetDefPrx&);

  virtual Ice::ByteSeq get(const Ice::Current&);

private:
  AxisHTTPAPI _axis_command;
};

/*
 * Pan servant class
 */
class Pan : public Media::IFloat::RW,
	    public PropertyService::PropertyWrapper {

public:
  Pan(const AxisHTTPAPI&, const IceStorm::TopicPrx&, const Ice::ObjectPrx&,
      PropertyService::PropertySetDefPrx&);

  Ice::Float get(const Ice::Current&);
  void set(Ice::Float, const Ice::Identity&, const Ice::Current&);

  Ice::ObjectPrx getCb(const Ice::Current&);
  IceStorm::TopicPrx getTopic(const Ice::Current&);

  void setCbTopic(const Ice::ObjectPrx&, const IceStorm::TopicPrx&,
		  const Ice::Current&);

private:
  AxisHTTPAPI _axis_command;
  IceStorm::TopicPrx _topic;
  DUO::IFloat::WPrx _pub;

  float _value;
};


/*
 * Tilt servant class
 */
class Tilt : public Media::IFloat::RW,
	     public PropertyService::PropertyWrapper {

public:
  Tilt(const AxisHTTPAPI&, const IceStorm::TopicPrx&, const Ice::ObjectPrx&,
       PropertyService::PropertySetDefPrx&);

  Ice::Float get(const Ice::Current&);
  void set(Ice::Float, const Ice::Identity&, const Ice::Current&);

  Ice::ObjectPrx getCb(const Ice::Current&);
  IceStorm::TopicPrx getTopic(const Ice::Current&);

  void setCbTopic(const Ice::ObjectPrx&, const IceStorm::TopicPrx&,
		  const Ice::Current&);

private:
  AxisHTTPAPI _axis_command;
  IceStorm::TopicPrx _topic;
  DUO::IFloat::WPrx _pub;

  float _value;
};

/*
 * Zoom servant class
 */
class Zoom : public Media::IInt::RW,
	     public PropertyService::PropertyWrapper {

public:
  Zoom(const AxisHTTPAPI&, const IceStorm::TopicPrx&, const Ice::ObjectPrx&,
       PropertyService::PropertySetDefPrx&);

  Ice::Int get(const Ice::Current&);
  void set(Ice::Int, const Ice::Identity&, const Ice::Current&);

  Ice::ObjectPrx getCb(const Ice::Current&);
  IceStorm::TopicPrx getTopic(const Ice::Current&);

  void setCbTopic(const Ice::ObjectPrx&, const IceStorm::TopicPrx&,
		  const Ice::Current&);

private:
  AxisHTTPAPI _axis_command;
  IceStorm::TopicPrx _topic;
  DUO::IInt::WPrx _pub;

  int _value;
};

/*
 * AutoFocus servant class
 */
class AutoFocus : public Media::IBool::RW,
		  public PropertyService::PropertyWrapper {

public:
  AutoFocus(const AxisHTTPAPI&, const IceStorm::TopicPrx&, const Ice::ObjectPrx&,
	    PropertyService::PropertySetDefPrx&);

  bool get(const Ice::Current&);
  void set(bool, const Ice::Identity&, const Ice::Current&);

  Ice::ObjectPrx getCb(const Ice::Current&);
  IceStorm::TopicPrx getTopic(const Ice::Current&);

  void setCbTopic(const Ice::ObjectPrx&, const IceStorm::TopicPrx&,
		  const Ice::Current&);

private:
  AxisHTTPAPI _axis_command;
  IceStorm::TopicPrx _topic;
  DUO::IBool::WPrx _pub;

  bool _value;
};

/*
 * AutoIris servant class
 */
class AutoIris : public Media::IBool::RW,
		 public PropertyService::PropertyWrapper {

public:
  AutoIris(const AxisHTTPAPI&, const IceStorm::TopicPrx&, const Ice::ObjectPrx&,
	   PropertyService::PropertySetDefPrx&);

  bool get(const Ice::Current&);
  void set(bool, const Ice::Identity&, const Ice::Current&);

  Ice::ObjectPrx getCb(const Ice::Current&);
  IceStorm::TopicPrx getTopic(const Ice::Current&);

  void setCbTopic(const Ice::ObjectPrx&, const IceStorm::TopicPrx&,
		  const Ice::Current&);

private:
  AxisHTTPAPI _axis_command;
  IceStorm::TopicPrx _topic;
  DUO::IBool::WPrx _pub;

  bool _value;
};

/*
 * Home servant class
 */
class Home : public Media::Pulse::W,
	     public PropertyService::PropertyWrapper {
public:
  Home(const AxisHTTPAPI&, const IceStorm::TopicPrx&, const Ice::ObjectPrx&,
       PropertyService::PropertySetDefPrx&);

  void set(const Ice::Identity&, const Ice::Current&);

  Ice::ObjectPrx getCb(const Ice::Current&);
  IceStorm::TopicPrx getTopic(const Ice::Current&);

  void setCbTopic(const Ice::ObjectPrx&, const IceStorm::TopicPrx&,
		  const Ice::Current&);

private:
  AxisHTTPAPI _axis_command;
  IceStorm::TopicPrx _topic;
  DUO::Pulse::WPrx _pub;

};

#endif /* _AXISCONTAINER_H_ */
