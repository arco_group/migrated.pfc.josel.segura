/* -*- mode: c++; coding: utf-8 -*- */

#ifndef _AXISVDEV_H_
#define _AXISVDEV_H_

#include <iostream>
#include <sstream>
#include <map>

#include <Hesperia/AVStreams.h>
#include <Hesperia/PropertyWrapper.h>

class AxisVDev : public AVStreams::VDev,
		 public PropertyService::PropertyWrapper {

public:
  AxisVDev(const std::map<std::string, std::string>&,
	   PropertyService::PropertySetDefFactoryPrx);
  
  std::string getName();

  // AVStreams::VDev
  virtual bool setPeer(const ::AVStreams::StreamCtrlPrx&,
		       const ::AVStreams::VDevPrx&,
		       const ::AVStreams::StreamQoS&,
		       const ::AVStreams::FlowSpec&,
		       ::AVStreams::StreamQoS&,
		       const Ice::Current&);

  virtual bool setMCastPeer(const ::AVStreams::StreamCtrlPrx&,
			    const ::AVStreams::MCastConfigIfPrx&,
			    const ::AVStreams::StreamQoS&,
			    const ::AVStreams::FlowSpec&,
			    ::AVStreams::StreamQoS&,
			    const Ice::Current&);

  virtual void configure(const ::PropertyService::Property&,
			 const Ice::Current&);

  virtual void setFormat(const ::std::string&,
			 const ::std::string&,
			 const Ice::Current&);

  virtual void setDevParams(const ::std::string&,
			    const ::PropertyService::Properties&,
			    const Ice::Current&);

  virtual bool modifyQoS(const ::AVStreams::StreamQoS&,
			 const ::AVStreams::FlowSpec&,
			 ::AVStreams::StreamQoS&,
			 const Ice::Current&);

private:
  AVStreams::VDevPrx _peer_dev;

  PropertyService::PropertySetDefFactoryPrx _psfactory;
  std::string _ip;
  std::map<std::string, std::string> _endpoints;

};

#endif /* _AXISVDEV_H_ */
