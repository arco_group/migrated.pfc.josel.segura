/* -coding: utf-8; mode: c++ -*- */

#include <iostream>
#include <string>
#include <map>
#include "AxisHTTPAPI.hh"

int
main(int argc, char** argv) {
  std::map<std::string, std::string> config;

  if (argc != 4)
	return 1;

  config["ip"] = argv[1];
  config["user"] = argv[2];
  config["pwd"] = argv[3];

  AxisHTTPAPI api(config);

  printf("pan=%.4f\r\n", api.getPan());
  printf("tilt=%.4f\r\n", api.getTilt());
  printf("zoom=%d\r\n", api.getZoom());

  bool autofocus = api.getAutoFocus();
  bool autoiris = api.getAutoIris();

  std::cout << "autofocus=";

  if (autofocus)
	std::cout << "on\r" << std::endl;

  else
	std::cout << "off\r" << std::endl;

  std::cout << "autoiris=";

  if (autoiris)
	std::cout << "on\r" << std::endl;

  else
	std::cout << "off\r" << std::endl;

  return 0;
}

