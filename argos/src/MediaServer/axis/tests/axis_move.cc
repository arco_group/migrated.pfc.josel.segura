/* -coding: utf-8; mode: c++ -*- */

#include <iostream>
#include <string>
#include <map>
#include "AxisHTTPAPI.hh"

#include <stdlib.h>

int
main(int argc, char** argv) {
  /* args:
	  [1]: IP
	  [2]: user
	  [3]: password
	  [4]: variable to be modified
	  [5]: value
   */

  std::map<std::string, std::string> config;

  if (argc != 6)
	return 1;

  config["ip"] = argv[1];
  config["user"] = argv[2];
  config["pwd"] = argv[3];

  AxisHTTPAPI api(config);

  std::string variable = argv[4];

  if (variable == "pan")
	api.setPan(atof(argv[5]));

  else if (variable == "tilt")
	api.setTilt(atof(argv[5]));

  else if (variable == "zoom")
	api.setZoom(atoi(argv[5]));

  else if (variable == "autofocus")
	api.setAutoFocus(argv[5]);

  else if (variable == "autoiris")
	api.setAutoIris(argv[5]);

  else
	return 1;

  return 0;
}
