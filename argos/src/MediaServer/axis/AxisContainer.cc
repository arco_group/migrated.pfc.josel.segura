/* -*- mode: c++; coding: utf-8 -*- */

#include <ec/logger.h>

#include "AxisContainer.h"

using namespace std;

/***************************
 * Container factory/driver
 **************************/

AxisContainer::AxisContainer(const map<string,string>& config,
			     const Ice::ObjectAdapterPtr& adapter,
			     PropertyService::PropertySetDefFactoryPrx& ps) {
  AxisHTTPAPI api(config);

  Ice::CommunicatorPtr ic = adapter->getCommunicator();

  /*
   * Get the IceStorm::TopicManager to set the topics and callbacks of the active objects
   * in the container
   */
  IceStorm::TopicManagerPrx tMgr = IceStorm::TopicManagerPrx::checkedCast \
    (ic->stringToProxy(config.find("topic_mgr")->second));

  IceStorm::TopicPrx topic;
  Ice::ObjectPrx pub;

  /* Recycable var to keep the property set def passed to any
   * PropertySetDef object using PropertyWrapper */
  PropertyService::PropertySetDefPrx psd;

  string id = config.find("id")->second;

  psd = ps->createPropertySetDef();

  Ice::ObjectPtr obj = new Snapshot(api, psd);
  _objs["snapshot"] =  adapter->addWithUUID(obj);

  /* Creating pan */
  psd   = ps->createPropertySetDef();

  try {
    topic = tMgr->create(id + ".pan");
  }
  catch(IceStorm::TopicExists e) {
    topic = tMgr->retrieve(id + ".pan");
  }

  pub   = topic->getPublisher();
  obj = new Pan(api, topic, pub, psd);
  _objs["pan"] = adapter->addWithUUID(obj);

  /* Creating tilt */
  psd   = ps->createPropertySetDef();

  try {
    topic = tMgr->create(id + ".tilt");
  }
  catch(IceStorm::TopicExists e) {
    topic = tMgr->retrieve(id + ".tilt");
  }

  pub   = topic->getPublisher();
  obj = new Tilt(api, topic, pub, psd);
  _objs["tilt"] = adapter->addWithUUID(obj);

  /* Creating zoom */
  psd   = ps->createPropertySetDef();

  try {
    topic = tMgr->create(id + ".zoom");
  }
  catch(IceStorm::TopicExists e) {
    topic = tMgr->retrieve(id + ".zoom");
  }

  pub   = topic->getPublisher();
  obj = new Zoom(api, topic, pub, psd);
  _objs["zoom"] = adapter->addWithUUID(obj);

  /* Creating auto-focus */
  psd   = ps->createPropertySetDef();

  try {
    topic = tMgr->create(id + ".autofocus");
  }
  catch(IceStorm::TopicExists e) {
    topic = tMgr->retrieve(id + ".autofocus");
  }

  pub   = topic->getPublisher();
  obj = new AutoFocus(api, topic, pub, psd);
  _objs["autofocus"] = adapter->addWithUUID(obj);


}

DUO::ObjectPrxDict
AxisContainer::list(const Ice::Current& current) {
  return _objs;
}

/*
 * Snapshot methods
 */
Snapshot::Snapshot(const AxisHTTPAPI& axis_command, PropertyService::PropertySetDefPrx& psd) :
  PropertyService::PropertyWrapper(psd), _axis_command(axis_command) {
  ec::debug() << "Snapshot configured" << endl;

}

Ice::ByteSeq
Snapshot::get(const Ice::Current& current) {
  return Ice::ByteSeq(_axis_command.getSnapshot());
}

/*
 * Pan methods
 */
Pan::Pan(const AxisHTTPAPI& axis_command, const IceStorm::TopicPrx& topic,
	 const Ice::ObjectPrx& publisher, PropertyService::PropertySetDefPrx& psd) :
  _topic(topic), PropertyService::PropertyWrapper(psd), _axis_command(axis_command) {
  ec::debug() << "Pan configured" << endl;

  _pub = DUO::IFloat::WPrx::uncheckedCast(publisher);
  _value = 0.0;


}

Ice::Float
Pan::get(const Ice::Current& current) {
  _value = _axis_command.getPan();
  return Ice::Float(_value);
}

void
Pan::set(Ice::Float v, const Ice::Identity& id, const Ice::Current& current) {
  ec::debug() << "pan value changed by " /*<< id*/ << " to value = " << v << endl;

  _axis_command.setPan(v);
  _value = _axis_command.getPan();

  /* Active behaviour */
  _pub->set(_value, current.id);
}

Ice::ObjectPrx
Pan::getCb(const Ice::Current& current) {
  return _pub;
}

IceStorm::TopicPrx
Pan::getTopic(const Ice::Current& current) {
  return _topic;
}

void
Pan::setCbTopic(const Ice::ObjectPrx& cb, const IceStorm::TopicPrx& topic,
		const Ice::Current& current) {

  _topic = topic;

  if (cb) /* set a topic and its publisher */
    _pub = DUO::IFloat::WPrx::checkedCast(cb);

  else
    _pub = DUO::IFloat::WPrx::uncheckedCast(_topic->getPublisher());

}

/*
 * Tilt methods
 */
Tilt::Tilt(const AxisHTTPAPI& axis_command, const IceStorm::TopicPrx& topic,
	   const Ice::ObjectPrx& publisher, PropertyService::PropertySetDefPrx& psd) :
  _topic(topic), PropertyService::PropertyWrapper(psd), _axis_command(axis_command)
{
  ec::debug() << "Tilt configured" << endl;

  _pub = DUO::IFloat::WPrx::uncheckedCast(publisher);
  _value = 0.0;
}

Ice::Float
Tilt::get(const Ice::Current& current) {
  _value = _axis_command.getTilt();
  return Ice::Float(_value);
}

void
Tilt::set(Ice::Float v, const Ice::Identity& id, const Ice::Current& current) {
  ec::debug() << "Tilt value changed by " /*<< id*/ << " to value = " << v << endl;

  _axis_command.setTilt(v);

  _value = _axis_command.getTilt();
  _pub->set(_value, current.id);

}

Ice::ObjectPrx
Tilt::getCb(const Ice::Current& current) {
  return _pub;
}

IceStorm::TopicPrx
Tilt::getTopic(const Ice::Current& current) {
  return _topic;
}

void
Tilt::setCbTopic(const Ice::ObjectPrx& publisher, const IceStorm::TopicPrx& topic,
		 const Ice::Current& current) {
  _topic = topic;

  if (publisher) /* set a topic and its publisher */
    _pub = DUO::IFloat::WPrx::checkedCast(publisher);

  else
    _pub = DUO::IFloat::WPrx::uncheckedCast(_topic->getPublisher());

}

/*
 * Zoom methods
 */
Zoom::Zoom(const AxisHTTPAPI& axis_command, const IceStorm::TopicPrx& topic,
	   const Ice::ObjectPrx& publisher, PropertyService::PropertySetDefPrx& psd) :
  _topic(topic), PropertyService::PropertyWrapper(psd), _axis_command(axis_command)
{
  ec::debug() << "Zoom configured" << endl;

  _pub = DUO::IInt::WPrx::uncheckedCast(publisher);
  _value = 0;
}

Ice::Int
Zoom::get(const Ice::Current& current) {
  _value = _axis_command.getZoom();
  return Ice::Int(_value);
}

void
Zoom::set(Ice::Int v, const Ice::Identity& id, const Ice::Current& current) {
  _axis_command.setZoom(v);

  _value = _axis_command.getZoom();
  _pub->set(_value, current.id);

}

Ice::ObjectPrx
Zoom::getCb(const Ice::Current& current) {
  return _pub;
}

IceStorm::TopicPrx
Zoom::getTopic(const Ice::Current& current) {
  return _topic;
}

void
Zoom::setCbTopic(const Ice::ObjectPrx& publisher, const IceStorm::TopicPrx& topic,
		 const Ice::Current& current) {
  _topic = topic;

  if (publisher) /* set a topic and its publisher */
    _pub = DUO::IInt::WPrx::checkedCast(publisher);

  else
    _pub = DUO::IInt::WPrx::uncheckedCast(_topic->getPublisher());

}

/*
 * Autofocus methods
 */
AutoFocus::AutoFocus(const AxisHTTPAPI& api, const IceStorm::TopicPrx& topic,
		     const Ice::ObjectPrx& publisher, PropertyService::PropertySetDefPrx& psd) :
  _axis_command(api), _topic(topic), PropertyService::PropertyWrapper(psd)

{
  ec::debug() << "AutoIris object configured" << endl;

  _pub = DUO::IBool::WPrx::uncheckedCast(publisher);
  _value = false;

}

bool
AutoFocus::get(const Ice::Current& current) {
  _value = _axis_command.getAutoFocus();
  return _value;
}

void
AutoFocus::set(bool v, const Ice::Identity& id, const Ice::Current& current) {
  _axis_command.setAutoFocus(v);

  _value = _axis_command.getAutoFocus();
  _pub->set(_value, current.id);
}

Ice::ObjectPrx
AutoFocus::getCb(const Ice::Current& current) {
  return _pub;
}

IceStorm::TopicPrx
AutoFocus::getTopic(const Ice::Current& current) {
  return _topic;
}

void
AutoFocus::setCbTopic(const Ice::ObjectPrx& publisher, const IceStorm::TopicPrx& topic,
		      const Ice::Current& current) {

  _topic = topic;

  if (publisher) /* set a topic and its publisher */
    _pub = DUO::IBool::WPrx::checkedCast(publisher);

  else
    _pub = DUO::IBool::WPrx::uncheckedCast(_topic->getPublisher());
}

/* AutoIris methods */
AutoIris::AutoIris(const AxisHTTPAPI& api, const IceStorm::TopicPrx& topic,
		   const Ice::ObjectPrx& publisher, PropertyService::PropertySetDefPrx& psd) :
  _axis_command(api), _topic(topic), PropertyService::PropertyWrapper(psd)
{
  _pub = DUO::IBool::WPrx::uncheckedCast(publisher);
  _value = false;
}

bool
AutoIris::get(const Ice::Current& current) {
  _value = _axis_command.getAutoIris();
  return _value;
}

void
AutoIris::set(bool v, const Ice::Identity& id, const Ice::Current& current) {
  _axis_command.setAutoIris(v);

  _value = _axis_command.getAutoIris();
  _pub->set(_value, current.id);
}

Ice::ObjectPrx
AutoIris::getCb(const Ice::Current& current) {
  return _pub;
}

IceStorm::TopicPrx
AutoIris::getTopic(const Ice::Current& current) {
  return _topic;
}

void
AutoIris::setCbTopic(const Ice::ObjectPrx& publisher, const IceStorm::TopicPrx& topic,
		     const Ice::Current& current) {
  _topic = topic;

  if (publisher) /* set a topic and its publisher */
    _pub = DUO::IBool::WPrx::checkedCast(publisher);

  else
    _pub = DUO::IBool::WPrx::uncheckedCast(_topic->getPublisher());
}

/*
 * Home methods
 */
Home::Home(const AxisHTTPAPI& api, const IceStorm::TopicPrx& topic,
	   const Ice::ObjectPrx& publisher, PropertyService::PropertySetDefPrx& psd) :

  _axis_command(api), _topic(topic), PropertyService::PropertyWrapper(psd) {

  _pub = DUO::Pulse::WPrx::uncheckedCast(publisher);
}

void
Home::set(const Ice::Identity& id, const Ice::Current& current) {
  _axis_command.goHome();
  _pub->set(current.id);
}

Ice::ObjectPrx
Home::getCb(const Ice::Current& current) {
  return _pub;
}

IceStorm::TopicPrx
Home::getTopic(const Ice::Current& current) {
  return _topic;
}

void
Home::setCbTopic(const Ice::ObjectPrx& publisher, const IceStorm::TopicPrx& topic,
		 const Ice::Current& current) {

  _topic = topic;

  if (publisher)
    _pub = DUO::Pulse::WPrx::uncheckedCast(publisher);

  else
    _pub = DUO::Pulse::WPrx::uncheckedCast(_topic->getPublisher());
}
