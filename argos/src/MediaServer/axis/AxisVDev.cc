/* -*- mode: c++; coding: utf-8 -*- */

#include <ec/logger.h>
#include "AxisVDev.h"

using namespace std;

AxisVDev::AxisVDev(const std::map<std::string, std::string>& config,
		   PropertyService::PropertySetDefFactoryPrx prx) {

  _ip = config.find("ip")->second;

  string gen = config.find("user")->second + ":" + config.find("pwd")->second + "@";

  _endpoints["rtsp"]  = "rtsp://" + gen + _ip + "/mpeg4/1/media.amp";
  _endpoints["mjpeg"] = "http://" + gen + _ip + "/axis-cgi/mjpg/video.cgi";

}

/* AVStreams VDev functions */
bool AxisVDev::setPeer(const AVStreams::StreamCtrlPrx& the_ctrl,
		       const AVStreams::VDevPrx& the_peer,
		       const AVStreams::StreamQoS& req_qos,
		       const AVStreams::FlowSpec& the_spec,
		       AVStreams::StreamQoS& got_qos,
		       const Ice::Current& ic) {

  ec::debug() << "+++ VDev::setPeer" << endl;
  PropertyService::Property config;

  _peer_dev = the_peer;

  config.propertyName = "endpoint";
  config.propertyValue = new P::StringT(_endpoints["rtsp"]);

  _peer_dev->configure(config);

  return true;
}

bool AxisVDev::setMCastPeer(const AVStreams::StreamCtrlPrx& the_ctrl,
			    const AVStreams::MCastConfigIfPrx& a_mcast_config_if,
			    const AVStreams::StreamQoS& req_qos,
			    const AVStreams::FlowSpec& the_spec,
			    AVStreams::StreamQoS& got_qos,
			    const Ice::Current& ic) {

  ec::debug() << "+++ VDev::setMCastPeer" << endl;

  PropertyService::Property config;

  config.propertyName = "endpoint";
  config.propertyValue = new P::StringT(_endpoints["rtsp"]);

  a_mcast_config_if->configure(config);
  return true;
}

void
AxisVDev::configure(const PropertyService::Property& property,
		    const Ice::Current& current) {}

void
AxisVDev::setFormat(const std::string& arg1,
		    const std::string& arg2,
		    const Ice::Current& current) {}

void
AxisVDev::setDevParams(const std::string& arg1,
		       const PropertyService::Properties& arg2,
		       const Ice::Current& current) {}

bool
AxisVDev::modifyQoS(const AVStreams::StreamQoS& arg1,
		    const AVStreams::FlowSpec& arg2,
		    ::AVStreams::StreamQoS& arg3,
		    const Ice::Current& current) {
  return false;
}
