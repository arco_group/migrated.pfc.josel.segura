// -*- coding: utf-8; mode: c++ -*-

#include "AxisHTTPAPI.h"

#include <fstream>
#include <ec/logger.h>
#include <ec/string.h>

using namespace std;

AxisHTTPAPI::AxisHTTPAPI() {
  _curlContext = NULL;
}

AxisHTTPAPI::AxisHTTPAPI(const map<string, string>& config) {
  string auth = config.find("user")->second + ":" +
	config.find("pwd")->second + "@";

  if (config.find("user")->second == "" || config.find("pwd")->second == "")
  	auth = "";

  _base_url = "http://" + auth + config.find("ip")->second + "/axis-cgi/";
  _ptzFilename = tempnam("/tmp", "ptz.");
  _snapshotFilename = tempnam("/tmp", "snap.");


  /* cURL stuff */
  curl_global_init(CURL_GLOBAL_ALL);
  _curlContext = curl_easy_init();

  if (_curlContext == NULL) {
	ec::error() << "Error at creating cURL context" << endl;
    throw "Error accessing Axis camera";
  }

  ec::info() << "cURL context created!" << endl;

  curl_easy_setopt(_curlContext, CURLOPT_NOSIGNAL, 1);
  curl_easy_setopt(_curlContext, CURLOPT_TIMEOUT, 5);
  curl_easy_setopt(_curlContext, CURLOPT_CONNECTTIMEOUT, 5);
  curl_easy_setopt(_curlContext, CURLOPT_NOPROGRESS, 1);
  //curl_easy_setopt(_curlContext, CURLOPT_WRITEHEADER, stderr);
}

CURL*
AxisHTTPAPI::rebuildContext() {
  curl_easy_cleanup(_curlContext);
  curl_global_cleanup();

  curl_global_init(CURL_GLOBAL_ALL);

  CURL* curlContext = curl_easy_init();
  if (curlContext == NULL) {
	ec::error() << "Error at creating cURL context" << endl;
    throw "Error creating server";
  }

  curl_easy_setopt(curlContext, CURLOPT_NOSIGNAL, 1);
  curl_easy_setopt(curlContext, CURLOPT_TIMEOUT, 5);
  curl_easy_setopt(curlContext, CURLOPT_CONNECTTIMEOUT, 5);
  curl_easy_setopt(curlContext, CURLOPT_NOPROGRESS, 1);
  curl_easy_setopt(curlContext, CURLOPT_WRITEHEADER, stderr);

  return curlContext;
}

string
AxisHTTPAPI::queryPTZ(const string& key) {
  if (_curlContext == NULL)
	throw "Misconfigured API. Do you use the right constructor?";

  CURLcode retCode;

  FILE* f = fopen(_ptzFilename, "w");

  curl_easy_setopt(_curlContext, CURLOPT_WRITEDATA, f);
  curl_easy_setopt(_curlContext, CURLOPT_URL, (_base_url + "com/ptz.cgi?query=position").c_str());

  retCode = curl_easy_perform(_curlContext);

  if (retCode != CURLE_OK) {
	ec::error() << "******** in performing: position" << endl;
	ec::error() << "Error from cURL: " << curl_easy_strerror(retCode) << endl;
	ec::error() << "Trying to rebuild the context" << endl;
    _curlContext = rebuildContext();
    throw "Unable to connect. Service temporaly unrecheable.";
  }

  fclose(f);

  ifstream file(_ptzFilename, ifstream::in);

  char line[20];

  while ( ! file.eof() ) {

	file >> line;
	string l(line);

	vector<string> v(ec::string::split(l, "="));

	if (v.size() == 2) {
	  if ( key == v[0] ) {
		file.close();
		return v[1];
	  }
	}
  }

  file.close();
  throw "Key not found";
}

void
AxisHTTPAPI::sendPTZ(const std::string& axis_command) {
  if (_curlContext == NULL)
	throw "Misconfigured API. Do you use the right constructor?";

  CURLcode retCode;

  curl_easy_setopt(_curlContext, CURLOPT_URL, (_base_url + "com/ptz.cgi?" +
											   axis_command).c_str());

  retCode = curl_easy_perform(_curlContext);

  if (retCode != CURLE_OK) {
	ec::error() << "******** in performing: " <<  + "position" << endl;
	ec::error() << "Error from cURL: " << curl_easy_strerror(retCode) << endl;
	ec::error() << "Trying to rebuild the context" << endl;
    _curlContext = rebuildContext();
    throw "Unable to connect. Service temporaly unrecheable.";
  }
}

vector<unsigned char>
AxisHTTPAPI::getSnapshot() {
  return getSnapshot(map<string,string>());
}

vector<unsigned char>
AxisHTTPAPI::getSnapshot(const map<string,string>& properties) {
  if (_curlContext == NULL)
	throw "Misconfigured API. Do you use the right constructor?";

  vector<unsigned char> retval;
  CURLcode retCode;

  string send("?"); // send = "?variable1=value1&variable2=value2&...&variableN=valueN
  for(map<string,string>::const_iterator it = properties.begin();
  	  it != properties.end(); it++)

  	send += it->first + "=" + it->second + "&";

  // delete the last "&" added
  send.erase(--send.end());

  /* Write the image on a file using CURLOPT_WRITEDATA
   * then, read it using a ifstream and write onto a vector
   * it can be done better (using CURLOPT_WRITEFUNCTION) but this
   * way is not so bad...
   */

  FILE *f = fopen(_snapshotFilename, "w");

  curl_easy_setopt(_curlContext, CURLOPT_WRITEDATA, f);
  curl_easy_setopt(_curlContext, CURLOPT_URL, (_base_url + "jpg/image.cgi" + send).c_str());
  retCode = curl_easy_perform(_curlContext);

  if (retCode != CURLE_OK) {
  	ec::error() << "******** in performing: " <<  + "position" << endl;
  	ec::error() << "Error from cURL: " << curl_easy_strerror(retCode) << endl;
  	ec::error() << "Trying to rebuild the context" << endl;
    _curlContext = rebuildContext();
    throw "Unable to connect. Service temporaly unrecheable.";
  }

  fclose(f);

  ifstream file(_snapshotFilename, ifstream::in);

  unsigned char buf[1];

  while (! file.eof()) {
  	file >> buf;
  	retval.push_back(*buf);
  }

  file.close();

  return retval;
}

float
AxisHTTPAPI::getPan() {
  float retval;

  try {
	retval = atof(queryPTZ("pan").c_str());
  }

  catch (string e) {
	ec::error() << e << endl;
	retval = 0.0;
  }

  return retval;
}

float
AxisHTTPAPI::getTilt() {
  float retval;

   try {
	 retval = atof(queryPTZ("tilt").c_str());
  }

  catch (string e) {
	ec::error() << e << endl;
	retval = 0.0;
  }

  return retval;
}

int
AxisHTTPAPI::getZoom() {
  int retval;

  try {
	retval = atoi(queryPTZ("zoom").c_str());
  }

  catch (string e) {
	ec::error() << e << endl;
	retval = 0;
  }

  return retval;
}

bool
AxisHTTPAPI::getAutoFocus() {
  bool retval;

  try {
	string val(queryPTZ("autofocus"));

	if ( val == "on" )
	  retval = true;

	else //if ( val == "off" )
	  retval = false;

  }

  catch (string e) {
	ec::error() << e << endl;
	retval = false;
  }

  return retval;
}

bool
AxisHTTPAPI::getAutoIris() {
  bool retval;

  try {
	string val(queryPTZ("autoiris"));

	if ( val == "on" )
	  retval = true;

	else //if ( val == "off" )
	  retval = false;

  }

  catch (string e) {
	ec::error() << e << endl;
	retval = false;
  }

  return retval;
}

void
AxisHTTPAPI::setPan(float pan) {
  string query("pan=");

  ostringstream buffer;
  buffer << pan;

  query += buffer.str();

  sendPTZ(query);
}

void
AxisHTTPAPI::setTilt(float tilt) {
  string query("tilt=");

  ostringstream buffer;
  buffer << tilt;

  query += buffer.str();

  sendPTZ(query);
}

void
AxisHTTPAPI::setZoom(int zoom) {
  string query("zoom=");

  ostringstream buffer;
  buffer << zoom;

  query += buffer.str();

  sendPTZ(query);
}

void
AxisHTTPAPI::setAutoFocus(bool autofocus) {
  string query("autofocus=");

  if (autofocus)
	query += "on";

  else
	query += "off";

  sendPTZ(query);
}

void
AxisHTTPAPI::setAutoIris(bool autofocus) {
  string query("autofocus=");

  if (autofocus)
	query += "on";

  else
	query += "off";

  sendPTZ(query);
}

void
AxisHTTPAPI::goHome() {
  /*
   * This use a old API version because the Axis cameras
   * I use don't have the newer API (configurable home)
   */
  sendPTZ("pan=0&tilt=0&zoom=1");
}

