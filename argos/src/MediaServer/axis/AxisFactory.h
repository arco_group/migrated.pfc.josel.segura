// -*- mode: c++; coding: utf-8 -*-

#ifndef __AXIS_FACTORY_H
#define __AXIS_FACTORY_H

#include "Hesperia/AVStreams.h"
#include "Hesperia/DUO.h"

#include "Factory.h"

class AxisFactory : public Factory {
public:
  static AxisFactory* getInstance();

  virtual AVStreams::VDevPtr createVDev(const std::map<std::string, std::string>&,
					PropertyService::PropertySetDefFactoryPrx);

  virtual DUO::Container::RPtr createContainer(const std::map<std::string, std::string>&,
					       const Ice::ObjectAdapterPtr&,
					       PropertyService::PropertySetDefFactoryPrx);

private:
  static AxisFactory* _instance;

};

#endif
