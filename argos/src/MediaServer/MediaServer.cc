// -*- mode: c++; coding: utf-8 -*-

#include <iostream>

#include <ec/logger.h>

#include "MediaServer.h"


using namespace std;

using namespace std;

extern "C" {
  IceBox::Service*
  createMediaServer(Ice::CommunicatorPtr ic) {
    return new MediaServer(ic);
  }
}

MediaServer::MediaServer(const Ice::CommunicatorPtr& ic) {}

void
MediaServer::start(const std::string& name,
		   const Ice::CommunicatorPtr& ic,
		   const Ice::StringSeq& args) {
  ec::Logger::root.set_level(ec::Logger::ALL);
  
  ec::info() << "*** Starting MediaServer" << endl;
  ec::info() << "Development version " << name << endl;
  
  Ice::PropertiesPtr properties = ic->getProperties();

  try {
    ec::debug() << "Creating object adapter " << name << ".Adapter" << endl;
    _adapter = ic->createObjectAdapter(name + ".Adapter");
    _adapter->activate();
  }

  catch (IceUtil::Exception e) {
    ec::fatal() << e.what() << endl;
    ec::fatal() << name + ".Adapter" << endl;
    exit(1);
  }

  map<string, string> config;

  config["plugin"] = properties->getProperty(name + ".Plugin");
  config["ip"] = properties->getProperty(name + ".IP");
  config["user"] = properties->getProperty(name + ".User");
  config["pwd"] = properties->getProperty(name + ".Password");
  config["id"] = config["plugin"] + "." + properties->getProperty(name + ".IP");
  config["topic_mgr"] = properties->getPropertyWithDefault("IceStorm/TopicManager",
							   "IceStorm/TopicManager");

  int debug;
  debug = properties->getPropertyAsIntWithDefault("Logger.Debug", 0);

  if (debug == 0)
    ec::Logger::root.set_level(ec::Logger::ERROR);

  ec::debug() << config["id"] << std::endl;

  PropertyService::PropertySetDefFactoryPrx psf = \
    PropertyService::PropertySetDefFactoryPrx::
    checkedCast(ic->stringToProxy(properties->getProperty("PropertySetFactory")));

  string identity = config["id"];

  _mmdevcont = new MMDevContainer(config, _adapter, psf);

  Ice::ObjectPrx prx = _adapter->add(_mmdevcont,
				     ic->stringToIdentity(identity));

  ec::debug() << prx << std::endl;
}

void
MediaServer::stop() {
  ec::debug() << "*** Stopping Server" << endl;
  
  if (_adapter)
    _adapter->deactivate();
}
