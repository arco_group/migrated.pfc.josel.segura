// -*- mode: c++; coding: utf-8 -*-

#include <ec/logger.h>

#include "StreamEndPointI.h"

using namespace std;

StreamEndPointI::StreamEndPointI(PropertyService::PropertySetDefFactoryPrx psfactory) {

  // Set PropertySetDef to PropertyWrapper
  setPropertySetDef(psfactory->createPropertySetDef());
}

void
StreamEndPointI::stop(const ::AVStreams::FlowSpec& the_spec,
			 const Ice::Current& current) {

  ec::info() << "--- StreamEndPoint:stop" << endl;
  // _mmdev->stop();
}

void
StreamEndPointI::start(const ::AVStreams::FlowSpec& the_spec,
			  const Ice::Current& current) {

  ec::info() << "--- StreamEndPoint:start" << endl;
  // _mmdev->play();
}

void
StreamEndPointI::destroy(const ::AVStreams::FlowSpec& the_spec,
			    const Ice::Current& current) {
}

bool
StreamEndPointI::connect(const ::AVStreams::StreamEndPointPrx& responder,
			    const ::AVStreams::StreamQoS& req_qos,
			    const ::AVStreams::FlowSpec& the_pec,
			    ::AVStreams::StreamQoS& got_spec,
			    const Ice::Current& current) {

  return false;
}

void
StreamEndPointI::setFPStatus(const ::AVStreams::FlowSpec& the_spec,
				const ::std::string& fp_name,
				const ::AVStreams::SFPStatus& fp_settings,
				const Ice::Current& current) {
}

void
StreamEndPointI::setNegotiator(const ::AVStreams::NegotiatorPrx& new_negotiator,
				  const Ice::Current& current) {
}

void
StreamEndPointI::setKey(const ::std::string& flow_name,
			   const ::AVStreams::Key& the_key,
			   const Ice::Current& current) {
}

void
StreamEndPointI::setSourceId(::Ice::Long source_id,
				const Ice::Current& current) {
}

bool
StreamEndPointI::requestConnection(const ::AVStreams::StreamEndPointPrx& initiator,
				      bool is_mcast,
				      const ::AVStreams::FlowSpec& req_spec,
				      const ::AVStreams::StreamQoS& req_qos,
				      ::AVStreams::FlowSpec& got_spec,
				      ::AVStreams::StreamQoS& got_qos,
				      const Ice::Current& current) {

  return false;
}

bool
StreamEndPointI::modifyQoS(const ::AVStreams::FlowSpec& the_flows,
			      const ::AVStreams::StreamQoS& req_qos,
			      ::AVStreams::StreamQoS& got_qos,
			      const Ice::Current& current) {

  return false;
}

bool
StreamEndPointI::setProtocolRestriction(const ::AVStreams::ProtocolSpec& pSpec,
					   const Ice::Current& current) {

  return false;
}

void
StreamEndPointI::disconnect(const ::AVStreams::FlowSpec& the_spec,
			       const Ice::Current& current) {
}

::Ice::ObjectPrx
StreamEndPointI::getFep(const ::std::string& fep_name,
			   const Ice::Current& current) {

  return 0;
}

::std::string
StreamEndPointI::addFep(const ::Ice::ObjectPrx& the_fep,
			   const Ice::Current& current) {

  return ::std::string();
}

void
StreamEndPointI::removeFep(const ::std::string& fep_name,
			      const Ice::Current& current) {
}
