// -*- coding: utf-8 -*-h

#ifndef __MEDIA_SERVER_H__
#define __MEDIA_SERVER_H__

#include <Ice/Ice.h>
#include <IceBox/IceBox.h>
#include <IceStorm/IceStorm.h>

#include "Hesperia/PropertyService.h"
#include "Hesperia/ASDF.h"

#include "MMDevContainer.h"

class MediaServer : public IceBox::Service {
public:
  MediaServer(const Ice::CommunicatorPtr&);

  virtual void start(const std::string&, const Ice::CommunicatorPtr&,
					 const Ice::StringSeq&);

  void stop();

private:
  Ice::ObjectAdapterPtr _adapter;
  MMDevContainer* _mmdevcont;
};

#endif
