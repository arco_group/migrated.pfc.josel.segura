// -*- mode: c++; coding: utf-8 -*-

#ifndef __FACTORY_H__
#define __FACTORY_H__

#include "Hesperia/AVStreams.h"
#include "Hesperia/DUO.h"


class Factory {
public:
  virtual AVStreams::VDevPtr createVDev(const std::map<std::string, std::string>&,
					PropertyService::PropertySetDefFactoryPrx) = 0;

  virtual DUO::Container::RPtr createContainer(const std::map<std::string, std::string>&,
					       const Ice::ObjectAdapterPtr&,
					       PropertyService::PropertySetDefFactoryPrx) = 0;

};

#endif
