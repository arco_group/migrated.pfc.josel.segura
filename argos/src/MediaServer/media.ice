// -*- mode: c++; coding: utf-8 -*-

#include <Hesperia/DUO.ice>
#include <Hesperia/PropertyService.ice>

module Media {

  module Pulse {
	interface W extends DUO::Pulse::W,
	  DUO::Active::R, DUO::Active::W,
	  PropertyService::PropertySetDef {};
  };

  module IByte {
    interface W extends
      DUO::IByte::W,
      DUO::Active::R, DUO::Active::W,
      PropertyService::PropertySetDef {};

    interface RW extends W,
      DUO::IByte::R {};
  };

  module IBool {
    interface W extends
      DUO::IBool::W,
      DUO::Active::R, DUO::Active::W,
      PropertyService::PropertySetDef {};

    interface RW extends W,
      DUO::IBool::R {};
  };

  module IFloat {
    interface RW extends
      DUO::IFloat::R,
      DUO::IFloat::W,
      DUO::Active::R, DUO::Active::W,
      PropertyService::PropertySetDef {};
  };

  module IInt {
    interface RW extends
      DUO::IInt::R,
      DUO::IInt::W,
      DUO::Active::R, DUO::Active::W,
      PropertyService::PropertySetDef {};
  };

  module IByteSeq {
    interface R extends
      DUO::IByteSeq::R,
      // DUO::Active::R, DUO::Active::W,
      PropertyService::PropertySetDef {};
  };
};
