# -*- coding: utf-8 -*-
#!/usr/bin/python

from distutils.core import setup
import string

author = 'José Luis Segura Lucas'
author_email = 'JoseL.Segura@alu.uclm.es'
website = 'http://argos-m.blogspot.com'
version = file('debian/changelog').readline().split()[1][1:-1]

# ows_globals_in = string.Template(file('ows/ows_globals.py.in').read())
# ows_globals = ows_globals_in.substitute(author=author,
#                                         author_email=author_email,
#                                         website=website,
#                                         version=version)
#file('ows/ows_globals.py', 'wt').write(ows_globals)

setup(name         = 'gygos-sink',
      version      = version,
      description  = 'Argos video sink applet',
      author       = author,
      author_email = author_email,
      url          = website,
      license      = 'GPL v3 or later',
      data_files   = [('/usr/lib/gnome-applets',   ['src/RenderApplet/RenderApplet/RenderApplet.py']),
                      ('/usr/lib/bonobo/servers',
                       ['src/RenderApplet/data/GNOME_PythonRenderApplet.server']),
                      ('/usr/share/gyargos-sink',        ['src/RenderApplet/RenderApplet/GstreamerRender.py']),
                      #('/usr/share/gconf/schemas', ['data/ows.schemas']),
                      #('/usr/share/pixmaps',       ['images/ows.png']),
                      #('/usr/share/python-support/ows/ows', ['ows/ows_globals.py', 'ows/__init__.py']),
                      ],
      )

#http://docs.python.org/dist/module-distutils.core.html
