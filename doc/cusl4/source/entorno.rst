.. -*- coding: utf-8 -*-

============
 Requisitos
============

Además de los requisitos software expuestos en el apartado anterior,
**Argos** es más sencillo de utilizar si se despliega sobre un entorno
*IceGrid*.

Dado el estado actual de desarrollo, esta condición
