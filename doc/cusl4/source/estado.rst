.. -*- coding: utf-8 -*-

=====================
 Estado del proyecto
=====================

Actualmente **Argos** se encuentra en fase de desarrollo, ya que, como
se dijo en la introducción, se trata de un Proyecto Fin de Carrera que
se presentará en la convocatoria de Julio.

Por ello, algunos de los objetivos planteados para el proyecto en el
apartado anterior aún no se han realizado o su implementación no
está terminada del todo.

Entre los hitos que se han conseguido alcanzar durante esta fase de
desarrollo se encuentran los siguientes:

* *Axis Media Server*: servicio *IceBox* que permite distribuir, dentro
  del entorno **Argos**, una cámara IP de la familia `AXIS
  <http://www.axis.com/>`_.

* *Gstreamer Media Render*: servicio *IceBox* que permite recibir el
  un flujo de vídeo desde un *source*, renderizándolo a través de la
  librería *Gstreamer* a pantalla completa.

* *Applet Media Server*: permite activar un servicio en la máquina
  dónde está ejecutándose y mostrar en una ventana el vídeo que sea
  configurado desde un editor externo. Actualmente se está trabajando
  en esta aplicación. Por ahora **no** es un *applet* de escritorio de
  Gnome, si no una aplicación independiente.

* *Editor de configuración de flujos básico*: editor de configuración
  de flujos en línea de comando. Permite especificar los *proxies* a
  dos objetos de tipo ``MMDevice``. Este elemento del proyecto es el
  preludio de un editor en modo gráfico que, por razones de tiempo,
  no ha sido posible implementar aún, pero que será implementado como
  parte de mi proyecto fin de carrera.

Además, se pueden dar satisfechos algunos casos de usos que no quedan
ejemplificados en los anteriores elementos ya desarrollados, pero que
utilizan algunos de los objetivos expuestos, como son ambas
infraestructuras como servicios *IceBox* para fuentes y sumideros de
flujos.

