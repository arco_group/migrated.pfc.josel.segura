.. -*- coding: utf-8 -*-

================================
 Librerías y software utilizado
================================

El proyecto **Argos** utiliza una serie de librerías que deberán estar
instaladas en las diferentes máquinas para hacerlo funcionar.

ZeroC Ice
=========

*Ice* (Internet Communication Engine) es un *middleware* orientado a
objetos con soporte para multitud de lenguajes de programación. En
**Argos** se utiliza para la comunicación entre los diferentes
elementos del sistema.

Además, se utilizan algunos de los servicios implementados y
proporcionados bajo licencia libre por ZeroC, como *IceGrid*,
*IceStorm* o *IceBox*.

* *IceGrid*: es un servicio de localización y activación para
  aplicaciones Ice. Entre sus características más importantes,
  permite la transparencia de localización (uno de los puntales de
  los sistemas distribuidos) y la activación automática de
  servidores.

* *IceBox*: es una implementación del patrón *Service Configurator*,
  que permite implementar servicios como librerías cargadas de forma
  dinámica. Su principal ventaja es que permite paralelizar en un
  mismo servidor *IceBox* varios servicios, consumiendo menos
  recursos que por separado.

* *IceStorm*: proporciona, dentro de un servicio *IceBox*, la
  funcionalidad de canal de eventos (similar al patrón *Observer*,
  pero distribuido). Uno o varios *publishers* pueden enviar mensajes
  de cualquier tipo a un determinado *topic*, que a su vez reenvía
  esos mensajes a todos los *subscribers* que estén registrados
  (suscritos) a dicho canal.

Para más información y descargas: http://www.zeroc.com. En
distribuciones de GNU/Linux como Debian o Ubuntu está disponible el
paquete ``zeroc-ice33``, que instala todo lo necesario para
desarrollar y ejecutar aplicaciones que utilicen esta librería.

GTK+
====

Conjunto de herramientas para crear interfaces gráficas. Muy utilizada
(por ejemplo, por `Gnome <http://gnome.org>`_) y con librerías
disponibles para varios lenguajes de programación. En el caso de
**Argos**, nos importan las implementaciones en C (nativa de GTK+) y
en Python (`PyGTK <http://www.pygtk.org>`_).

Más información en http://www.gtk.org. Al tratarse de una librería de
uso muy extendido viene instalada en la mayoría de las distribuciones
de GNU/Linux. Será necesario, de todos modos, comprobar si la versión
para Python de la misma está instalada.

Gstreamer
=========

Es una librería para la construcción de aplicaciones multimedia desde
el reproductor más sencillo hasta aplicaciones complejas con
diferentes flujos de audio/video.

Más información en http://gstreamer.freedesktop.org. Al igual que
GTK+, está muy extendida y viene instalada de serie en casi todas las
distribuciones de GNU/Linux. Comprobar que los *bindings* para Python
de la librería están instalados.
