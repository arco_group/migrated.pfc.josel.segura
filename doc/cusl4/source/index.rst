.. Documentación proyecto Argos documentation master file, created by
   sphinx-quickstart on Sun Apr 11 20:20:36 2010.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentación proyecto Argos
=============================

Contents:

.. toctree::
   :maxdepth: 2

   intro.rst
   objetivos.rst
   estado.rst
   software.rst
   manual.rst

.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

