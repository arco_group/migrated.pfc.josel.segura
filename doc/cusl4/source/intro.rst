.. -*- coding: utf-8 -*-

==============
 Presentación
==============

**Argos** es un proyecto informático, aún en desarrollo, centrado en
el área de distribución de flujos multimedia.

Su nombre original y también oficial, ya que se trata del **Proyecto
Fin de Carrera** del que escribe estas líneas, es **Argos:
Infraestructura modular para distribución y control de flujos
multimedia en sistemas heterogéneos en red**.

Todo este proyecto está realizado con **software libre**: desde las
herramientas de desarrollo, como el software que se utiliza como
herramientas del propio proyecto, como las librerías utilizadas son
libres, por lo que el proyecto en si mismo también lo es.

Realicé está elección por la cantidad de ventajas que a un
desarrollador aporta la libertad del software. Poder estudiar y
modificar el código fuente de diferentes aplicaciones y
distribuirlo sin ningún tipo de cortapisa es algo muy importante,
sobre todo para desarrolladores que *acabamos de empezar* en este
mundo.
