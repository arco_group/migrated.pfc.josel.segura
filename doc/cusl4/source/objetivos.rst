.. -*- coding: utf-8 -*-

===========
 Objetivos
===========

El proyecto **Argos** tiene planteados los siguientes objetivos para
su completa finalización:

* Infraestructura extensible para desarrollo de diferentes fuentes
  multimedia como servicio *IceBox*.

* Especialización de la infraestructura anterior para distribuir el
  flujo de dispositivos con servidor de *streaming* propio (por
  ejemplo, cámaras IP con servidor RTSP propio, como las *Axis*).

* Especialización de la infraestructura anterior para distribuir el
  flujo de dispositivos que deben ser servidos al no disponer de su
  propio servidor de *streaming*. En este grupo se agruparían las
  cámaras USB o capturadoras de vídeo.

* Infraestructura extensible para desarrollo de diferentes sumideros
  multimedia como servicio *IceBox*.

* Especialización de la infraestructura anterior para renderizar el
  flujo en una ventana o a pantalla completa.

* Creación de sumideros/fuentes de flujos en dispositivos diferentes a
  un PC, como por ejemplo `Chumby <http://www.chumby.com>`_ o
  teléfonos Android.

* Aplicación de escritorio para configurar los flujos entre los
  diferentes fuentes y sumideros.

* Infraestructura para permitir la distribución *multicast* de flujos.
