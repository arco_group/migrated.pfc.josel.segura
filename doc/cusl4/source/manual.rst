.. -*- coding: utf-8 -*-

===================
 Manual de usuario
===================

**NOTA**: Dado que **Argos** aún está en desarrollo, no existen
instaladores ni paquetes bajo ningún sistema de control de paquetes
tipo *APT* o similar.

Enlace de descarga

En el enlace anterior se puede descargar los elementos utilizables a
fecha de 14 de Abril de 2010 para ser evaluados en el marco del
Concurso Universitario de Software Libre de Castilla-la Mancha.

Descarga: :download:`argos.tar.gz <files/argos.tar.gz>`

Dentro del ``tgz`` se encuentran los diferentes elementos en sus
respectivos directorios:

* ``axismediaserver``: contiene una librería de carga dinámica
  (``libAxisMediaServer.so``) y dos ficheros de configuración Ice
  (``*.cfg``).

* ``gstreamermediarender``: contiene varios ficheros Python que
  permiten arrancar un *Media Render* en la máquina dónde se ejecute.
  También incluye un fichero *slice* (definición de interfaces de Ice)
  necesario para la ejecución del programa.

* ``argos_config``: contiene un *script* Python y un fichero *slice*,
  necesario para su funcionamiento.

Todos los directorios contienen además un fichero ``README`` con las
dependencias necesarias para ejecutar cada elemento y las
instrucciones para su puesta en marcha.

Debido a las dependencias tan específicas de los elementos es posible
que los evaluadores no tengan la posibilidad de probarlos. Por este
motivo, en el `blog del proyecto <http://argos-m.blogspot.com>`_ se
colgará un `vídeo dónde se verá la puesta en marcha y funcionamiento de
los diferentes elementos
<http://argos-m.blogspot.com/2010/04/prueba.html>`_.
