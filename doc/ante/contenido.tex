%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:

\section{Introducción}
\label{sec:intro}

Durante los últimos años hemos vivido una progresión sin precedentes
en el área de las tecnologías multimedia. A día de hoy es cada vez más
habitual encontrarnos viviendas en las que los tradicionales
dispositivos de reproducción (reproductores de \textsl{DVD}, vídeos
tradicionales, equipos de audio \...) han dejado paso a un PC o
similar que sustituye a todos estos aparatos.

Desde hace un tiempo es común que las viviendas dispusieran de uno o
varios ordenadores personales en habitaciones o despachos, destinados
al trabajo y al  ocio, con conectividad a Internet pero sin apenas
interacción entre ellos. Ahora, lo normal es encontrar también
dispositivos que son capaces de conectarse a una red de área local
inalámbrica (red doméstica).

Entre los dispositivos que integran las redes domésticas cada vez hay
una mayor diversidad de dispositivos: videoconsolas, ordenadores
portátiles y teléfonos móviles son capaces de integrar la red doméstica,
añadiendo a sus funciones habituales la visualización de fotografías o
reproducción de música y vídeo entre otras. Además han aparecido
otros dispositivos, como marcos fotográficos digitales o incluso
despertadores, que en sus versiones tecnológicas actualizadas son
capaces también de utilizar recursos multimedia.

Además de en el ámbito doméstico, el educativo también se ha visto
inundado por las nuevas tecnologías. Las aulas con ordenadores se han
abierto paso en los centros educativos a todos los niveles.

De todo lo anterior se deriva un problema: tenemos gran cantidad y
variedad de dispositivos hardware conectados entre ellos por medio
de la red doméstica, pero este avance en el hardware y la conectividad
no se ha visto acompañado de un avance paralelo en cuanto a
software. La forma de acceder a los recursos multimedia se realiza
igual que hace unos años cuando no teníamos estos avances. Esta forma
de utilización de los recursos reduce drásticamente las posibilidades
que los mismos nos brindan, infrautilizando a su vez la conectividad
que nos brinda estar dentro de una misma red.

A su vez, en el ámbito educativo los ordenadores se han convertido en
una simple ventana a Internet, con poca o ninguna interacción entre
ellos y, lo que es peor, difíciles de supervisar por parte del
profesor en cuanto a dar una clase se refiere.

En este proyecto se pretende solucionar esta serie de problemas
dotando de una infraestructura que permita configurar flujos de
cualquier tipo de recurso multimedia entre los diferentes elementos de
una red. De esta forma se podría seleccionar como conectar diferentes
dispositivos productores o consumidores de dichos flujos de una forma
rápida y dinámica.

\section{Objetivos}
\label{sec:objetivos}
El objetivo primordial de este proyecto es crear un sistema en el que
se puedan abstraer las diferencias entre los diferentes dispositivos y
los métodos que utilicen para trasportar sus flujos de audio o vídeo,
de forma que se puedan definir de forma sencilla los caminos que deben
seguir dichos flujos entre los dispositivos.

Los elementos del sistema serán fuentes o sumideros multimedia. Las
fuentes (o \emph{sources}) serán aquellos que sean capaces de generar
flujos multimedia. Ese receptor del flujo será un sumidero (o
\emph{sink}).

En ningún momento estamos estableciendo ninguna clase de restricción a
lo que el sumidero deberá realizar con el flujo recibido. Tan válido
podría ser un sumidero que reproduce el flujo multimedia como el que
lo transforma y emite a través de otra fuente de datos o, por ejemplo,
el que utilice dicho flujo para análisis de imagen sin llegar a
representarla. Este tipo de artefactos que transforman un flujo serán
denominados \emph{filtros}.

Esta descripción de los elementos hace que cada dispositivo pueda ser
a la vez tanto sumidero como fuente de uno o de varios flujos. Podemos
pensar por ejemplo en una cámara IP, que tendría varios elementos
fuente (audio, vídeo, \...) o un ordenador personal, que puede ser
una fuente de audio (recogido mediante un micrófono) o de vídeo
(capturadoras de televisión, \emph{webcam}, \...) y a la vez un
sumidero tanto de audio como de vídeo, reproduciendo este tipo de
flujos o tratándolos de cualquier otra forma.

Para cumplir con los objetivos se seguirá alguno de los estándares
existentes para el modelado del sistema. Estos estándares se
estudiarán durante la elaboración del proyecto y su estado del arte.

Otro objetivo se centrará en poder realizar transmisiones
\emph{multidestino} de los flujos multimedia mediante canales de eventos.

Todo lo anterior se completará con el desarrollo de un editor gráfico
de servicios, que permita realizar las conexiones entre fuentes y
sumideros o grupos de éstos, así como definir los controles de cada
flujo y el método que se utiliza para su transmisión.

Como objetivos opcionales se estudiará la creación de un «source» y
un «sink» en algún dispositivo no estándar (diferente a un PC) y  la
inclusión de transformadores de flujos multimedia mediante
recodificación o filtros al vuelo.

\section{Método y fases de trabajo}
\label{sec:metodo}
Durante la primera fase del proyecto se realizará un estudio del
estado del arte. Se estudiarán diferentes alternativas ya existentes
en el ámbito del control de flujos multimedia.

A las alternativas que se estudien se les exigirá que resuelvan una
serie de problemas relacionados con la distribución de flujos
multimedia, como las negociaciones de protocolos y de la calidad de
servicio. Debe definir la forma en la que dos dispositivos negocian
entre ellos una conexión mediante llamadas remotas entre ambos, y
también una forma con la que poder controlar el flujo entre ellos.

Entre las alternativas que se han encontrado durante un estudio previo
se encuentran Audio/Video Streams \cite{a/vstreams} y CCM Streams,
ambos definidos por el Object Management Group \cite{omg}. Además, se
estudiarán otras alternativas que están definidas para distribución de
flujos a nivel local y su posible adaptación a entornos distribuidos,
como pudieran ser OpenMAX y Gstreamer \cite{gstreamer}.

La especificación Audio/Video Streams representa un flujo multimedia
(\emph{stream}) como un conjunto de flujos individuales
(\emph{flows}). Este estándar se centra en diversos aspectos de la
cuestión, entre los que destaca la configuración y liberación de los
flujos, topologías y descripción de los mismos y la calidad de
servicio.

CCM Streams, al igual que el anterior, maneja los conceptos de
\emph{stream} y conexión, pero a diferencia del mismo está orientado a
componentes. Define perfiles de transporte como estándar de
comunicación entre los extremos.

Como metodología de trabajo se usará el Rational Unified Proccess
(\textsl{RUP} \cite{RUP}).

Esta metodología es una de las más utilizadas actualmente. Fue
desarrollada por la empresa \emph{Rational Software} y, actualmente,
pertenece a \textsl{IBM}.

\textsl{RUP} está dirigido por casos de uso, que representan
funcionalidades que debe cumplir el sistema. Los casos de usos a su
vez se agrupan en iteraciones.

Una de las herramientas más destacables que utiliza \textsl{RUP} es el
lenguaje de modelado \textsl{UML} (Unified Modelling Language)
\cite{UML}. Este lenguaje permite especificar, visualizar, construir y
documentar todo tipo de sistemas software.

\section{Medios disponibles}
\label{sec:medios}
Se dispondrá de varios ordenadores personales, tanto sobremesa como
portátiles, con diversas configuraciones de hardware.

Se utilizarán también diversos tipos de cámaras:

\begin{itemize}
\item Cámaras IP estáticas, como las \textbf{AXIS 212 PTZ},
  proporcionada por el grupo \textsc{ARCO}.

\item Cámaras IP móviles, como las \textbf{AXIS 214 PTZ},
  proporcionada por el grupo \textsc{ARCO}.

\item Cámaras web estáticas tradicionales, como las integrada en el
  portátil.

\item Cámaras web móviles, como el modelo Orbit de Creative,
  proporcionada por el grupo \textsc{ARCO}.

\end{itemize}

Además, se pretende utilizar, si el tiempo lo permite, otro tipo de
dispositivos más específicos para dotar de la deseada heterogeneidad
al sistema implementado. Entre las posibilidades que se estudiará
implementar se encuentran:

\begin{itemize}
\item Teléfono móvil con sistema operativo Android \cite{android-dev}.

\item Chumby \cite{chumby-hw} (radio-despertador con conectividad
  inalámbrica y pantalla táctil).

\item Videoconsolas Nintendo DS y Nintendo Wii.
\end{itemize}

En cuanto a software, las herramientas y entornos de programación que
se utilizarán serán Software Libre, debido a las ventajas técnicas y
sociales que ello implica. En principio, se considerará la utilización
de las siguientes aplicaciones:

\begin{itemize}
\item GNU Make: herramienta para la generación automática de
  ejecutables \cite{make}.

\item GNU Compiler Collection: colección de compiladores del proyecto
  GNU \cite{gcc}.

\item Python e iPython: intérpretes del lenguaje de programación
  Python.

\item GNU Debugger (gdb): depurador estándar de GNU \cite{gdb}.

\item Winpdb: depurador independiente de plataforma para Python.

\item GNU Emacs: editor de textos extensible y configurable \cite{emacs}.

\item Rubber: sistema automatizado de confección de documentos en \LaTeX.

\end{itemize}

Los lenguajes que se pretende utilizar para la realización del
proyecto serán:

\begin{itemize}
\item C++: lenguaje de programación multiparadigma.

\item Python: Lenguaje de programación orientado a objetos
  interpretado \cite{python-reference}.

\item \LaTeX: lenguaje de marcado de documentos, utilizado para
  realizar este documento y, en un futuro, la memoria del proyecto
  \cite{latex}.

\item BIB\TeX: herramienta para generar las listas de referencias
  tanto de este documento como de la memoria del proyecto.

\end{itemize}

Para la implementación del sistema es imprescindible contar con un
middleware orientado a objetos y que proporcione herramientas
adecuadas para trabajar con los lenguajes de implementación comentados
anteriormente. Esta descripción encaja perfectamente con el middleware
\textbf{ZeroC Ice} \cite{zeroc, zeroc-man}, aunque se valorarán otras
alternativas como \textsc{RMI} \cite{rmi}, \textsc{CORBA}
\cite{corba-faq, corba-ps, corba-ns, corba-namings} o WebServices \cite{webservices,
  webservices-arch}.

Además de las herramientas anteriores se utilizará un sistema de
control de versiones. Entre los muchos disponibles se utilizará
Mercurial \cite{mercurial}, que ofrece un sistema de control de
versiones distribuido y la posibilidad de llevar ese control fuera de
línea, frente a los sistemas de control de versiones centralizados
como \textsc{CVS} \cite{cvs} o Subversion \cite{svn}.

\section{Licencia y propiedad intelectual}
\label{sec:licencia}
El software resultante de la elaboración de este proyecto fin de
carrera se distribuirá bajo la licencia GPL versión 3, tal y como se
define en la página de la \textbf{Free Software Foundation}
\cite{gplv3}.

Para que esta licencia pueda ser aplicable se tendrá en cuenta que
todas las herramientas y librerías utilizadas por el proyecto tengan
licencias compatibles.
