% -*- coding: utf-8 -*-

\chapter{Antecedentes, Estado de la cuestión}
\label{chap:art_state}

\drop{E}{n} este capítulo se realiza un estudio sobre las alternativas
ya implementadas en el mismo ámbito que el proyecto a
realizar. Concretamente, se evalúan alternativas, tanto de
infraestructuras, \emph{software}, librerías y protocolos que permitan
el intercambio de flujos multimedia entre diferentes dispositivos.

En dicho estudio se incluye, además, la presentación y análisis de las
librerías y protocolos que van a ser utilizados durante el proyecto,
así como una justificación de la elección de los mismos.



\section{\Mws\ orientados a objetos}
\label{sec:middlewares}

Un \mw\ de comunicaciones orientado a objetos es una librería o entorno de trabajo que
proporciona a un \emph{software} diferentes funcionalidades y servicios para el acceso a
la red.

Al contrario que otras librerías que permiten interactuar con otros
nodos de red, en lugar de realizarse a través de un intercambio de
paquetes de nivel de aplicación, esta comunicación se delega a la
librería, que proporciona al programador mecanismos que le permiten
abstraerse de las llamadas al sistema relacionadas con gestión de la
conexión y que se puedan realizar invocaciones a objetos remotos de la
misma forma que se realizarían sobre un objeto local. (ver figuras
\ y \ref{fig:middleware_invoke}.

En la figura \ref{fig:normal_invoke} se observa una llamada entre
objetos normal, siguiendo el paradigma de la programación orientada a
objetos. En ella se ve que el objeto de tipo \texttt{A} tiene una
referencia a un objeto de tipo \texttt{B}, y hace una llamada al
método \texttt{call()} de éste último.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.6\textwidth]{normal_invoke.pdf}
  \caption{Invocación a un método en programación orientada a objetos}
  \label{fig:normal_invoke}
\end{figure}

En cambio, en la figura \ref{fig:middleware_invoke} se realiza, desde
el punto de vista del programador, una llamada similar. Sin embargo,
ambos objetos en esta ocasión se encuentran en diferentes nodos de una
red, por lo que la invocación, antes de llegar al objeto que
proporciona el servicio, debe ser traducida a un protocolo de red y
ser enviada desde un nodo hasta el otro. Todo ese trabajo de
transformar la llamada local en paquetes comunicación de red conforme
a un protocolo dado y, en el otro extremo, de transformar el paquete
recibido en una llamada a un objeto de ese nodo, es el que realiza el
\mw\ de comunicación.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=\textwidth]{middleware_invoke.pdf}
  \caption{Invocación a un método de un objeto en sistemas distribuidos}
  \label{fig:middleware_invoke}
\end{figure}

Con esta aproximación, se establece para cada mensaje una arquitectura
\textbf{cliente-servidor}, en la que el cliente es el método que
realiza la llamada remota y el servidor es aquel que la atiende y, de
ser necesario, responde.

A diferencia de la aproximación clásica a la arquitectura
cliente-servidor, los roles son totalmente intercambiables entre ambas
entidades, pudiéndose tomar roles diferentes en cada invocación remota.

En ese ámbito existen muchas alternativas a valorar. Entre todas ellas
se destacaron varias que cumplían con los requisitos esbozados, a
priori, para la elaboración del proyecto con la elección del estándar
\avs: \textbf{ZeroC \Ice} y \textbf{Java RMI}.

\subsection{ZeroC \Ice}
\label{ssec:zeroc_ice}

\Ice\ \cite{ice} es un \mw\ de comunicación orientado a objetos
licenciado como software libre bajo la licencia \ac{GPLv2} por la
empresa ZeroC.

Entre las características más destacables de \Ice\ con respecto al
resto de \mws\ estudiados es la posibilidad de utilizarlo en múltiples
lenguajes y plataformas. Actualmente: \Ice\ está disponible para los
sistemas operativos GNU/Linux, Microsft Windows y MacOS X. Además,
también existen implementaciones del mismo para dispositivos con iOS
(iPhone, iPad\...), Android y Silverlight.

Quizás la característica más sobresaliente respecto al resto de
alternativas es la posibilidad de utilizar múltiples lenguajes: la
librería está disponible completa para C++, Java, Python y C\#, y con
limitaciones para Perl y PHP. Esto permite que diferentes desarrollos
puedan utilizar el lenguaje que mejor se adapte a sus necesidades,
manteniendo la interoperabilidad con el resto de \emph{software}
desarrollado usando \Ice.

Para conocer mejor \Ice, se explicarán brevemente algunos
conceptos de su arquitectura, ya que algunos de ellos serán utilizados
en el capítulo \ref{chap:development}.


\begin{description}
\item[\textbf{Cliente y servidor}] Ambos conceptos se alejan poco de
  su sentido habitual en cualquier arquitectura de este tipo: los
  clientes necesitan una funcionalidad o datos que provee un
  servidor. Ambos, cliente y servidor, serán objetos. La diferencia
  fundamental entre ambos estriba en que el servidor deberá ser
  añadido al \textbf{adaptador de objetos} para que sea accesible para
  el cliente a través del \mw.

\item[\textbf{Adaptador de objetos}] El adaptador de objetos es un
  mecanismo proporcionado por el \mw\ para hacer que los objetos
  necesarios puedan recibir invocaciones remotas. Dichos adaptadores
  de objetos deben tener asignada una o varias direcciones de red a
  nivel de transporte (\textsc{IP} y puerto), conocidas como
  \emph{endpoints}. Para representarlos se suele utilizar una
  representación en formato cadena de los mismos, donde aparecen
  indicados el protocolo de transporte, la \textsc{IP} y el puerto. Un
  ejemplo de esta representación sería el siguiente:

  \begin{center}
    \texttt{udp -h 192.168.1.2 -p 62543}
  \end{center}


\item[\textbf{Sirviente}] Es todo aquel objeto que es capaz de
  proporcionar un servicio, o lo que es lo mismo, de recibir una
  invocación remota. Esto será posible gracias al \mw\ que se
  encargará de proporcionar al cliente un objeto \textbf{proxy}
  a través del cual realizar las llamadas remotas. En el lado del
  servidor, el objeto sirviente deberá ser añadido al adaptador de
  objetos para poder ser accesible.

\item[\textbf{Identidad}] Es una cadena de caracteres que identifica
  unívocamente a un sirviente dentro del adaptador de objetos. Dicho
  de otro modo, el identificador, junto con la representación del
  adaptador de objetos en formato cadena, proporcionarán un modo de
  direccionar al sirviente, el cual será utilizado por los clientes
  para acceder a él.

\item[Proxy] En el contexto de \Ice\ se refiere al objeto que, en el
  lado del cliente, representa a un objeto remoto. Es utilizado para
  que puedan realizarse llamadas a métodos locales que el \mw\ se
  encarga de transformar en invocaciones remotas. Para obtener dichos
  objetos se utiliza una representación en formato de cadena de
  caracteres del sirviente al que van a representar en el
  cliente. Dicha cadena (o \emph{``stringfied proxy''}) se construye
  uniendo la identidad del objeto y la representación en cadena de
  caracteres del \emph{endpoint} en el que ha sido añadido. Un ejemplo
  de \emph{``stringfied proxy''} es:

  \begin{center}
    \texttt{IdentidadObjeto -t:tcp -h 192.168.1.3 -p 54345}
  \end{center}

  Aparte de la identidad del objeto (\texttt{IdentidadObjeto}) y el
  \emph{endpoint} (\texttt{tcp -h 192.168.1.3 -p 54345}), aparece
  entre ambos la cadena \texttt{-t:}. Esa cadena en concreto sirve
  para expresar que el modo de acceso al objeto en el \emph{endpoint}
  será \emph{two way} (el cliente esperará la respuesta del
  sirviente). Otra posibilidad que permite el \mw\ es utilizar
  \texttt{-o:} en lugar de \texttt{-t:}, indicando de esta forma que
  el modo de acceso es \emph{one way}.

\item[\textbf{\emph{Communicator}}] Es el elemento imprescindible del
  \mw\ para realizar la comunicación real entre los diferentes
  clientes y servidores en el sistema distribuido. Es el responsable,
  entre otras cosas, de crear los adaptadores de objetos o de
  proporcionar las utilidades necesarias para convertir un
  \emph{proxy} de su representación en formato cadena en un objeto
  utilizable por un cliente.

\item[\textbf{\ac{Slice}}] Posiblemente estemos ante una de las
  características más diferenciadoras entre los diferentes
  \mws: el lenguaje de especificación de las interfaces. Estos
  lenguajes definen el ``contrato'' entre un servidor y sus clientes,
  definiendo la interfaz pública que será accesible a través de un
  \emph{proxy} a un sirviente determinado.

  Dicho fichero con la descripción debe estar accesible en ambos
  extremos: el sirviente de la interfaz realiza, dependiendo del
  lenguaje de implementación utilizado, una herencia de la interfaz
  definida en él, implementado los métodos definidos. Por otro lado,
  el cliente es capaz de crear un \emph{proxy} gracias a la
  información que define el fichero \ac{Slice}.

  En el caso de \ac{Slice}, la sintaxis de los ficheros es muy
  legible y en cuanto a forma y palabras reservadas, similar a
  C++. Para convertir entre este tipo de ficheros y código utilizable
  por el compilador para generar los binarios es necesario un proceso
  de traducción dependiente del lenguaje. Los programas encargados de
  dicha traducción son los llamados \textbf{traductores}, habiendo uno
  de ellos por cada lenguaje soportado por el \mw\ \Ice.
\end{description}

\begin{figure}[!ht]
  \centering
  \includegraphics[width=\textwidth]{ice_structure.png}
  \caption{Estructura de comunicación cliente-servidor en \Ice}
  \label{fig:structure-ice}
\end{figure}

\subsubsection{Servicios avanzados del \mw\ ZeroC \Ice}
\label{sssec:art_advanced_services}

El \mw\ \Ice\ proporcionado por ZeroC proporciona una serie de
servicios ya implementados que están a disposición de los
programadores. Dichos servicios van desde funcionalidades externas a
los programas, como facilidades a la hora de desplegar a aplicaciones,
a herramientas para la construcción de las aplicaciones.

En los siguientes puntos se citarán dichos servicios, haciendo
especial hincapié en algunos de ellos que han sido utilizados o
podrían ser de utilidad en un futuro en el proyecto.

\begin{description}
\item[\textbf{IceGrid}] Según el manual del \mw, ``es el servicio
  de localización y activación para aplicaciones \Ice''. Permite
  la creación de \emph{grids} de ordenadores que interactúan entre
  ellos.

  El servicio de localización que IceGrid proporciona permite que un
  cliente encuentre de forma indirecta los sirvientes que necesita,
  sin necesidad de dar el \emph{proxy} completo para ello.

  Con IceGrid se puede realizar el despliegue de los diferentes
  servidores que conforman una aplicación, permitiendo guardarse la
  configuración y ser cargada en un entorno de red diferente. Esta
  funcionalidad es muy importante y ha sido utilizada para realizar el
  despliegue del proyecto y para su integración con el resto de
  prototipos del proyecto Hesperia.

  Otra funcionalidad utilizada durante el proyecto es la de consulta
  dinámica de objetos: a través de la interfaz \texttt{IceGrid/Query}
  un cliente puede solicitar una lista de objetos bien conocidos
  (``\emph{well known objects}'') seleccionados por la interfaz que
  implementan.

  Otras características interesantes de IceGrid son la posibilidad de
  configurar los servidores de un determinado \emph{grid} a través de
  diferentes programas de configuración, activación de servidores bajo
  demanda, distribución de aplicaciones a través de la aplicación
  \emph{IcePatch} y la replicación y balanceo de carga.

\item[\textbf{IceBox}] Esta herramienta es un \emph{framework} para la
  implementación de servicios \Ice\ utilizando el patrón de diseño
  \emph{Service Configurator} \cite{srv_conf}. Este patrón permite la
  configuración de los servicios y la centralización de su
  administración.

  En la práctica, los servicios se desarrollan como componentes de
  carga dinámica (librerías dinámicas en entornos GNU/Linux). Más tarde,
  un solo servidor \textbf{IceBox} es capaz de cargar tantos de estos
  componentes como sea necesario y en la combinación deseada, pudiendo
  configurar éstos a través de propiedades. De este modo se pueden
  sustituir una serie de servidores individuales por un solo servidor
  \textbf{IceBox} que cargue los componentes dinámicos.

  Entre las ventajas de utilizar esta herramienta caben destacar tres
  que afectan al desarrollo de este proyecto:

  \begin{itemize}
  \item La composición de una aplicación con varios servicios se
    realiza mediante configuración y no mediante compilación.

  \item Todos los servicios comparten una interfaz común, que permite
    una administración centralizada.

  \item Los servicios \textbf{IceBox} están totalmente soportados e
    integrados en \emph{IceGrid}, permitiendo un sencillo despliegue y
    configuración de los mismos.
  \end{itemize}

\item[\textbf{IcePatch}] Es una aplicación, altamente integrada con
  IceGrid, que permite la distribución de \emph{software} a los nodos
  que conforman el \emph{grid}. De este modo, cada aplicación
  desplegada en el nodo puede configurarse de manera que reciba la
  versión actualizada del servicio a través de esta aplicación en
  lugar de utilizando medios tradicionales.

\item[\textbf{IceStorm}] Servicio desarrollado para proporcionar
  canales de eventos al \mw. Un cliente del servicio puede crear un
  canal o \emph{topic} en el que otros clientes podrán publicar lo que
  deseen, siempre a través de invocaciones remotas. Dichas
  invocaciones serán enviadas a todos los subscriptores de dicho
  canal.

  Una de sus mayores ventajas es la federación de canales, lo cual
  dota a IceStorm de una gran flexibilidad para multitud de
  aplicaciones prácticas.

\item[\textbf{Freeze}] Proporciona una serie de servicios para la
  persistencia de objetos distribuidos.

\item[\textbf{Glacier2}] Es un servicio de \emph{firewall} que permite
  a los clientes y servidores comunicarse, de forma segura, a través
  de \emph{firewall} de red sin comprometer por ello la seguridad. El
  tráfico entre clientes y servidores se encripta utilizando \ac{SSL}.

\end{description}

\subsection{Java \ac{RMI}}
\label{ssec:java_rmi}

Java \ac{RMI} \cite{rmi} es un \mw\ de comunicación orientado a
objetos desarrollado por Sun Microsystems (adquirida recientemente por
Oracle) para aplicaciones escritas en lenguaje Java.

Su funcionamiento básico es el siguiente:

\begin{itemize}
\item Los servidores utilizan el \texttt{rmiregistry} para registrar
  los objetos que serán accesibles para el resto del
  entorno. Utilizando un sistema de nombres propio de \ac{RMI},
  denominado \emph{\ac{RMI}\'s simple naming facility}, el servidor es
  capaz de ubicar el objeto al que el cliente se ha referido en la
  invocación remota.

\item Un objeto registrado en el \texttt{rmiregistry} (el equivalente
  a un sirviente en \Ice) deberá implementar al menos la interfaz
  definida por el \mw\ \texttt{Remote}.

\item El \texttt{rmiregistry} le da al cliente una referencia a un
  objeto que puede utilizar de la misma forma que si el objeto fuera
  uno localmente definido.
\end{itemize}

Para especificar la interfaz entre el cliente y el servidor se
utilizan interfaces definidas en Java. Esto obliga a que todos los
clientes y servidores en una arquitectura \ac{RMI} estén desarrollados
en Java.

Este impedimento de utilizar otros lenguajes diferentes de Java fue un
handicap que pesó significativamente en contra de este \mw\ en
comparación con el propuesto por ZeroC.


\section{Negociación entre dispositivos multimedia}
\label{sec:art_std_negotiation}

En este apartado se consideraran dos alternativas de estándar
propuestas para solventar el problema de la negociación entre
dispositivos multimedia. Dichos estándares estudiados son \avs\ y
\textbf{Streams for CCM}, ambos definidos por el \ac{OMG}.

Se ofrecerá un breve resumen de ambos, poniendo un énfasis especial en
los detalles de \avs, que a la postre ha sido el elegido para la
implementación del proyecto.

\subsection{\avs}
\label{ssec:art_avstreams}

El estándar \avs\ \cite{avstreams_specif}, definido por el \ac{OMG}
\cite{omg}, especifica el control y gestión de flujos multimedia de
audio y vídeo.

El estándar define \emph{stream} como el conjunto de los flujos de
datos entre objetos, dónde un flujo de datos es una transmisión
continuada de conjuntos de datos (\emph{frames}) en un sentido. La
interfaz de un \emph{stream} es la agregación de uno o varios
productores o consumidores de flujos de datos multimedia en un solo
objeto.

Pese a que en la definición anterior pueden encajar cualquier tipo de
flujo de datos, el estándar se centra en las aplicaciones que trabajan
haciendo intercambios de audio y vídeo, mostrando interés a su vez en
restricciones relacionadas con la calidad de servicio.

Los problemas que aborda la especificación del estándar \avs\ son los
siguientes:

\begin{itemize}
\item Topologías para \emph{streams}.
\item Flujos múltiples dentro de un mismo \emph{stream}.
\item Descripción y formalización de \emph{streams} multimedia.
\item Identificación y referencia a la interfaces \emph{stream}.
\item Configuración y liberación de \emph{streams}.
\item Modificación de \emph{streams} ya establecidos.
\item Terminación de \emph{streams}.
\item Manejo de varios protocolos.
\item Calidad de servicio (\ac{QoS}).
\item Sincronización de flujos.
\item Interoporabilidad
\end{itemize}

Además, también se establecen algunas aproximaciones para afrontar el
problema de la seguridad.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=\textwidth]{avstreams_arch.png}
  \caption{Arquitectura en la que se basa \avs\ \cite{avstreams_specif}}
  \label{fig:avs_architecture}
\end{figure}

En la figura \ref{fig:avs_architecture} se muestra un \emph{stream}
con un solo flujo entre dos extremos, en el que uno de ellos actúa
como fuente de la transmisión y el otro como receptor. Se muestra como
en cada extremo existe un objeto para controlar al dispositivo
presente en ese lado de la conexión (\emph{stream interface control
  object}) y un elemento, fuera de dichos extremos, denominado
\emph{Control and Management Objects}, que se comunica con los mismos
para realizar el control y la gestión del flujo.

A su vez, cabe señalar que el flujo (\emph{flow} en la figura
\ref{fig:avs_architecture}) es transmitido \textbf{sin pasar} por el
\mw\ de comunicación (en este caso aparece con notación de
\ac{CORBA}).

\subsubsection{Componentes principales de \avs}
\label{sssec:avs_main_components}

El estándar propone una serie de interfaces para implementar un
\emph{framework} muy completo. De todas ellas, haré especial hincapié
en aquellas que serán más utilizadas en el desarrollo del proyecto:

\begin{description}
\item[\textbf{\texttt{MMDevice}}] Es la abstracción de un dispositivo
  multimedia, independientemente de que se trate de una fuente,
  consumidor o cumpla con ambos roles. Los objetos con dicha interfaz
  serán siempre los implicados en las conexiones multimedia de este
  estándar.

\item[\textbf{\texttt{VDev}}] Representa a un dispositivo multimedia
  virtual; es decir, el representante durante la conexión del
  \texttt{MMDevice}.

\item[\textbf{\texttt{StreamEndpoint}, \texttt{StreamEndpointA} y
    \texttt{StreamEndpointB}}] En el contexto de una conexión entre
  dispositivos multimedia, el \emph{stream endpoint} se encarga de
  contener y controlar cada flujo entre ambos extremos. Tanto
  \texttt{StreamEndpointA} como \texttt{StreamEndpointB} son
  especializaciones de \texttt{StreamEndpoint}. La diferencia entre
  ambas se hace por dos motivos: diferenciar entre los dos extremos de
  una conexión concreta y, en el caso de conexiones multicast,
  proporcionar una interfaz extendida al origen del flujo para su
  configuración.

\item[\textbf{\texttt{StreamCtrl}}] Es la abstracción del flujo de
  datos entre dos \texttt{MMDevices}. Los objetos que implementan esta
  interfaz son los encargados de iniciar la conexión entre dos
  \emph{MMDevices}, así como de proporcionar un control al programador
  (pausa, parada, vuelta a la reproducción\dots)

\item[\textbf{\texttt{MCastConfigIf}}] Esta interfaz es el equivalente
  a un \texttt{VDev} al inicializar las conexiones multicast. Cuando
  la fuente inicia su conexión \emph{multicast}, se empareja con un
  objeto con esta interfaz. A su vez, los consumidores del flujo se
  configurarán contactando con dicho objeto para recibir sus
  parámetros de configuración y ser capaces de conectar con el
  \texttt{VDev} de la fuente original.

\end{description}

Todas estas interfaces son las utilizadas en el perfil ligero del
estándar. El perfil completo es aquel en el que se es capaz de
controlar también la transmisión de los bloques de datos dentro de
cada flujo, lo que queda fuera del alcance propuesto para este
proyecto fin de carrera.

La mayor parte de las interfaces mencionadas anteriormente heredaban a
su vez de otra interfaz definida en el estándar \emph{Property
  Service} de \ac{OMG}: \texttt{PropertySetDef}.

\emph{Property Service} \cite{ps_spec} define una forma de añadir a
los objetos en un entorno distribuido atributos, ya que el tipo de los
objetos viene dado por la interfaz que implementan. De este modo, cada
objeto que implemente directamente las interfaces de este servicio
debe ser capaz de almacenar las propiedades de una forma parecida al
almacenamiento habitual en diccionarios clave-valor.

En un contexto normal, las funciones definidas para las interfaces del
estándar \emph{Property Service} se comportan siempre de la misma
forma, cambiando únicamente en la implementación la forma de guardar o
acceder a los valores de las propiedades. Por ello, dentro del grupo
\ac{ARCO} y en el ámbito del proyecto Hesperia, se desarrolló un
servicio que proporcionaba la funcionalidad definida por el estándar a
objetos que no quisieran implementar directamente las interfaces por
él definidas.

Dicho servicio, llamado de forma homónima al estándar (\emph{Property
  Service}), proporciona a todo aquel programa que lo utilice la
implementación del almacenamiento y recuperación, de forma remota, de
las propiedades que se deseen para cada objeto. De este modo,
conociendo el proxy del servicio se pueden almacenar las propiedades
de cada objeto en él sin un coste adicional de programación.

Además, otra gran ventaja de utilizar el \emph{Property Service}
implementado por \ac{ARCO} es la de ser independiente del lenguaje: al
estar implementado como un servicio remoto, los clientes pueden estar
implementados en cualquier lenguaje que soporte el \mw\ de comunicación.

Para utilizar éste estándar ha sido necesario un trabajo de traducción
de las interfaces definidas en el estándar \avs\ al lenguaje de
descripción de interfaces propio del \mw. Dicho trabajo se comenta en
el capítulo de desarrollo como primera iteración del proyecto
(apartado \ref{sec:it1}).

\subsubsection{Negociaciones \emph{unicast} y \emph{multicast}}
\label{sssec:negotiations}

El proceso de negociación en \avs\ es algo diferente en las conexiones
\emph{unicast} y \emph{multicast}, dado que en el segundo caso se
utiliza un tipo de objeto (definido por interfaz
\texttt{MCastConfigIf}) que no aparece en las negociaciones
\emph{unicast}.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=\textwidth]{avs_unicast.png}
  \caption{Diagrama de la negociación de conexión \emph{unicast} en
    \avs\ \cite{avstreams_specif}}
  \label{fig:unicast_diagram}
\end{figure}

En la figura \ref{fig:unicast_diagram} se ve el proceso de establecimiento
de conexión \emph{unicast} entre dos \texttt{MMDevices}. En ella se ve
como un objeto de tipo \texttt{StreamCtrl} recibe una invocación a su
método \texttt{bindDevs} que desencadena las llamadas a los dos
extremos del \emph{stream} (ambos de tipo \texttt{MMDevice}).

Dichos objetos de tipo \texttt{MMDevice} crean, a su vez, sendos
objetos \texttt{StreamEndPoint}. En el caso del lado considerado como
``A'', se creará un \texttt{StreamEndPointA}, y en el otro extremo, un
\texttt{StreamEndPointB}. Además se creará un \texttt{VDev} en cada
extremo.

Tras todo este proceso de inicialización de la conexión, el
\texttt{StreamCtrl} llamará a los dos \texttt{VDev} pasándoles como
argumento el \texttt{VDev} contrario, de forma que se conozcan y puedan
entre ellos invocarse a sus respectivas funciones de configuración
(\texttt{configure}).

El inicio, pausa y finalización de la transmisión se controlan a
través de los objetos \texttt{StreamEndPoint}, una vez que se haya
realizado la conexión entre ellos.

Para el caso de una conexión \emph{multicast}, lo primero sería hacer
que el extremo fuente del \emph{stream} quedara configurado. Para ello
se utiliza la interfaz \texttt{MCastConfigIf}, que es equivalente a un
\texttt{VDev} pero que en realidad no representa a ningún dispositivo
real. La conexión, desde el punto de vista del \texttt{MMDevice} es
completamente igual, salvo que a la hora de pasarle al \texttt{VDev}
su pareja, se invoca a \texttt{setMCastPeer} en lugar de a
\texttt{setPeer}.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=\textwidth]{avs_multicast_src.pdf}
  \caption{Diagrama de secuencia de la creación de una fuente \emph{multicast}}
  \label{fig:multicast_src_diagram}
\end{figure}

En la figura \ref{fig:multicast_src_diagram} aparecen todas las
invocaciones que se realizan para crear una fuente
\emph{multicast}. De todas ellas, las únicas que son locales son
aquellas de creación de objetos \emph{StreamEndPointA} y
\texttt{VDev} por parte de \texttt{MMDevice} y \texttt{MCastConfigIf}
por parte de \texttt{StreamCtrl}. Incluso estas podrían sustituirse
por invocaciones remotas si existiera algún servicio de factoría de
ese tipo de objetos.

Para configurar cada consumidor del \emph{stream} se realiza algo
similar: el \texttt{StreamCtrl} llama a la función \texttt{createB} en
el \texttt{MMDevice} receptor, creándose todos los objetos
necesarios. En este escenario, se llamaría al objeto
\texttt{MCastConfigIf} para comunicarle la conexión de un
\texttt{VDev}, enviándole éste la configuración al dispositivo
final. Por último, se llamaría a na función específica del
\texttt{StreamEndPointA} creado al inicializarse el \emph{stream}:
\texttt{connectLeaf}, que le permite al origen del \emph{stream}
mantener la información del estado de las diferentes conexiones. En el
diagrama de secuencia de la figura \ref{fig:multicast_sink_diagram} se
puede entender mejor este caso de uso.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=\textwidth]{avs_multicast_dst.pdf}
  \caption{Diagrama de secuencia de la conexión de un sumidero en una conexión \emph{multicast}}
  \label{fig:multicast_sink_diagram}
\end{figure}

\subsection{\emph{Streams for CCM}}
\label{ssec:ccm_streams}

El estándar \emph{Streams for CCM} \cite{ccmstreams_specif} está
definido en el contexto de otro estándar mayor: el llamado
\ac{CCM} \cite{ccm_spec}. Dicho estándar tiene como objetivo reducir
la complejidad de desarrollar grandes sistemas distribuidos, desde la
aproximación de un paradigma basado en componentes.

El modelo definido por \ac{CCM} soporta dos tipos diferentes de
comunicaciones: las operacionales síncronas y las asíncronas (por
eventos). Estos dos tipos de comunicación no son suficiente para una
amplia área de aplicaciones, entre ellas, las telecomunicaciones, la
seguridad o el problema que aquí nos ocupa: las transmisiones de audio
y vídeo.

Por ello, en el documento \emph{Streams for CCM} se define un tercer
modelo de comunicación concebido para ese tipo de necesidad. Dicho
modelo de comunicación, llamado ``flujo de datos continuo'', es
añadido a \ac{CCM}, extendiendo su modelo de comunicación y añadiendo
un sistema para clasificar este tipo de comunicaciones.

Entre las modificaciones que se realizan sobre \ac{CCM}, cabe
destacar, aparte de lo mencionado anteriormente, modificaciones en el
lenguaje de especificación de interfaces \ac{IDL} propio de dicho
estándar, añadiendo soporte para este tipo de comunicaciones.

Se descartó la utilización de dicho estándar ya que no encaja tan bien
como \avs\ en los objetivos del proyecto y no especifica una forma de
realizar la negociación entre dispositivos, si no la propia
transmisión de datos multimedia entre los dispositivos.

\section{Librerías de desarrollo multimedia}
\label{sec:multimedia_lib}

Este proyecto va a trabajar con flujos multimedia, por lo que es
lógico realizar un estudio de las librerías que permiten el manejo de
este tipo de datos que podría ser necesario utilizar.

En el mercado actual existe una multitud de librerías de manejo de
flujos multimedia, pero tras un rápido estudio previo, se decidió
descartar algunas de ellas por sus carencias en cuanto al manejo de
flujos en red o por problemas de licencia.

Finalmente se evaluaron dos librerías de desarrollo multimedia bien
conocidas y de uso bastante extendido: \textbf{VLC} y \textbf{GStreamer}.

\subsection{VLC}
\label{sec:multimedia:vlc}

\textbf{VLC} es un reproductor multimedia creado y mantenido por la organización
sin ánimo de lucro francesa \emph{VideoLAN} \cite{videolan}. Dicha
organización mantiene una gran cantidad de programas y librerías,
siendo VLC uno de los más conocidos.

Además del reproductor multimedia antes mencionado, VLC incluye una
librería de desarrollo que permite el control de audio, vídeo, listas
de reproducción y demás elementos del reproductor de forma
programática a través de una \ac{API} para el lenguaje C.

Los elementos destacados de esta librería que puedan beneficiar al
proyecto son:

\begin{description}
\item[\textbf{Licencia}] La licencia bajo la que se libera la librería
  es \ac{GPL}, por lo que se permite su utilización y redistribución.

\item[\textbf{Soporte de múltiples formatos}] La librería VLC es una
  de las que mayor cantidad de formatos y \emph{codecs} de audio y
  vídeo soporta en el panorama actual. Con ello nos aseguramos la
  compatibilidad con la mayor parte de dispositivos que emitan sus
  flujos por la red.

\item[\textbf{Reproducción a través de la red}] Esta librería es capaz
  de conectarse a un servidor de flujos multimedia (\ac{RTP},
  \ac{RTSP}) y tratarlo y reproducirlo de la misma forma que si fuera
  un fichero local.

\item[\textbf{Soporte para \emph{Video 4 Linux}}] \ac{V4L} \cite{vlc}
  es una   \ac{API} para el acceso a todo tipo de dispositivos de
  vídeo, como   cámaras web o capturadoras de televisión desde
  sistemas que ejecuten   un kernel Linux. Esto dota a la librería de
  capacidades para poder   compartir flujos multimedia desde este tipo
  de dispositivos.

\item[\textbf{Servidor de \emph{streaming}}] La librería permite la
  creación de flujos de \emph{streaming} en variedad de formatos que
  permiten configurar un servidor de flujos multimedia, lo cual la
  hace adecuada para poder compartir ficheros, dispositivos \ac{V4L}
  como los citados anteriormente, etcétera.

\end{description}

\subsection{GStreamer}
\label{sec:multimedia:gstreamer}

La librería \textbf{GStreamer} \cite{gstreamer_web} es una de las más
utilizadas en el ámbito del software libre, pudiéndose encontrar en
multitud de sistemas operativos productos desarrollados basándose en
esta librería.

A diferencia de VLC (\ref{sec:multimedia:vlc}), ésta es una librería
desarrollada teniendo en mente la extensibilidad.

La librería está basada en componentes individuales (\emph{elements})
\cite{gstreamer_app_manual}, que pueden ser productores, consumidores
o ambas cosas a la vez. Esta diferenciación reside en la presencia de
una serie de entradas y salidas (\emph{pads}). Dependiendo de si tiene
de un tipo, de otro o de los dos, así será el \emph{element}.

Cada flujo individual irá de un \emph{pad} fuente a un \emph{pad}
sumidero. Esto permite ir componiendo \emph{elements} de forma que se
construya la aplicación final que queramos conseguir.

Para desarrollar una aplicación, el programador tan sólo tiene que
crear un objeto especial de la librería, denominado \emph{pipeline}, y
añadir en él mismo todos los \emph{elements} que desee, conectándolos
entre ellos.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=\textwidth]{gst-simple-player.png}
  \caption{Ejemplo de la arquitectura de GStreamer con un reproductor \cite{gstreamer_app_manual}}
  \label{fig:gst_pipeline_example}
\end{figure}

En la figura \ref{fig:gst_pipeline_example} puede observarse un
pequeño ejemplo de la arquitectura interna de GStreamer. En ella puede
observarse un \emph{pipeline}, el cual tiene seis \emph{elements}
añadidos.

El primero de ellos (\emph{file-source}) tiene tan sólo un \emph{pad}
fuente que se conecta al \emph{pad} sumidero del siguiente elemento en
el \emph{pipeline}. En el ejemplo, el segundo \emph{pad} de la cadena
es un \emph{demuxer} que divide lo que el \emph{element} inicial le
pase en dos caminos diferentes.

Por último, aparecen en cada uno de los dos caminos credos por el
demultiplexor dos \emph{elements} sumideros, ya que sólo tienen
\emph{pads} de éste tipo.

Éste ejemplo concreto se trata de un reproductor para vídeos cuya
pista de audio esté codificada utilizando algún \emph{codec} de Vorbis
y, la de vídeo, utilizando el \emph{codec} de Theora.

Fijándonos en el diagrama podemos ver cómo, a nivel de
\emph{pipeline}, aparecen sendos iconos de control de
reproducción. Esto se debe a que en GStreamer dicho control se realiza
a nivel del \emph{pipeline} y no de los elementos concretos.

A continuación, se citarán las ventajas por las que se ha optado por
GStreamer cuando ha sido necesario realizar alguna aplicación
multimedia:

\begin{description}
\item[\textbf{Soporte de formatos}] Existen una multitud de
  \emph{elements} en GStreamer que permiten manejar un gran número de
  formatos.

\item[\textbf{Soporte de reproducción de \emph{streaming}}] Al igual
  que VLC, GStreamer soporta la reproducción de flujos multimedia
  provenientes de la red.

\item[\textbf{Creación nativa de servidores de \emph{streaming}}]
  Aunque de forma nativa GStreamer no permite la creación de
  servidores \ac{RTSP}, si permite la creación de servidores de
  \emph{streaming} a través de \emph{sockets} \ac{UDP} o utilizando el
  protocolo \ac{RTP}.

\item[\textbf{Posibilidades de extensión}] Una de las más importantes
  de todas las características citadas aquí es que GStreamer
  proporciona una \ac{API} para programadores de \emph{plugins}
  externos \cite{gstreamer_plugin_dev} que permite la creación
  sencilla de extensiones y nuevos \emph{elements} para la
  arquitectura.

\item[\textbf{\emph{Bindings} para otros lenguajes}] La librería está
  completamente desarrollada en C, pero dado que está desarrollada
  basándose en \textbf{GObject} \cite{gobject}, es orientada a
  objetos. Esto ha permitido que existan \emph{bindings} para otros
  lenguajes que sí tienen soporte nativo para objetos, como C++
  \cite{gstreamermm} o Python \cite{pygst}.
\end{description}
