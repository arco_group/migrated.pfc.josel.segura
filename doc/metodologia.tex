% -*- coding: utf-8 -*-

\chapter{Método de trabajo y herramientas}
\label{chap:metodology}

\drop{E}{n} este capítulo se describen las metodologías de trabajo
estudiadas para el desarrollo del proyecto, extendiéndose en la
alternativa elegida.

También se realiza un repaso de las herramientas, tanto
\emph{software} como \emph{hardware} utilizadas durante la elaboración
del proyecto y de su documentación.

\section{Metodología de trabajo}
\label{sec:metodologia}

Cómo se comentó en el capítulo \ref{chap:intro}, inicialmente este
proyecto fin de carrera fue parte del proyecto Hesperia. En ese
contexto, se necesitaba obtener prototipos funcionales en diferentes
fases del proyecto, de forma que dichos prototipos pudieran ser
utilizados por los socios de Hesperia como si del producto final se
tratara, sirviendo así para la captura de nuevos requisitos.

Teniendo en cuenta estas necesidades, se evaluaron diferentes
alternativas en cuanto a metodologías de desarrollo, fijándose
especialmente en \ac{XP}, \ac{RUP} y el
\emph{Prototipado Evolutivo}.

\subsection{Programación extrema}
\label{ssec:ext_pro}

La Programación Extrema se encuadra dentro de las denominadas
metodologías ágiles de desarrollo. Hace especial hincapié en el
trabajo de equipo, usando herramientas como el \emph{pair
  programming}, y en la comunicación directa y constante entre el
cliente y los desarrolladores, de forma que los posibles errores o
malentendidos en la especificación de requisitos sean detectados lo
antes posible. Otra de sus características más atractivas es la
retroalimentación en base a pruebas (normalmente pruebas de unidad),
que se realizan desde el mismo momento que empieza a codificarse las
aplicaciones.

Esta metodología fue descartada de forma inmediata, debido a que
algunas de sus técnicas de trabajo son incompatibles con la
realización de un proyecto individual (\emph{Pair programming}). Aún
así, algunas de sus directrices han sido tenidas en cuenta y aplicadas
en algunos de los componentes del proyecto.

\subsection{Proceso Unificado de Rational}
\label{ssec:rup}

El Proceso Unificado de Rational es un proceso de desarrollo
más tradicional. Con muchos años de experiencia a sus espaldas, es un
proceso de desarrollo iterativo basado en el desarrollo en
espiral. \ac{RUP} define el ciclo de vida de un proyecto, dividiéndolo
en cuatro fases (iniciación, elaboración, construcción y
transición). Durante las dos primeras se hace hincapié en comprender
el problema y buscar las tecnologías adecuadas a su resolución,
durante la construcción se codifica el programa y se realizan las
iteraciones necesarias, mientras que en la última fase se asegura que
el producto construido es viable para ser entregado al cliente.

El descarte de \ac{RUP} se debe a las necesidades concretas de este
proyecto, al encuadrarse dentro de Hesperia, de tener cada cierto
tiempo un prototipo funcional que entregar a los socios de dicho
proyecto.

\subsection{Prototipado evolutivo}
\label{ssec:prot_evol}

El prototipado evolutivo \cite{ingsw} es una metodología ágil de
desarrollo ideada para proyectos de envergadura mediana o pequeña
(nunca más de 500\,000 líneas de código). Es apropiada en proyectos
cuyos requisitos puedes cambiar mucho a lo largo del tiempo.

A partir de una especificación de requisitos, se analizan estos y se
realiza el desarrollo y pruebas del prototipo obtenido. Dicho
prototipo puede ser entregado al cliente para su evaluación, tras la
cual decidirá si es válido o no. Si no lo es, se volverán a evaluar
los requisitos, modificando o añadiendo aquellos que el usuario estime
conveniente. El esquema general de esta metodología puede verse en la
figura \ref{fig:prototipado_evolutivo}.

\begin{figure}[!h]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{prototipado_evolutivo.pdf}
    \caption{Diagrama de flujo del prototipado evolutivo}
    \label{fig:prototipado_evolutivo}
  \end{center}
\end{figure}


Al principio del desarrollo siguiendo esta metodología se necesitan
conocer los objetivos generales del proyecto, mientras que los
objetivos más específicos y funcionales del mismo pueden  no conocerse
o conocerse sólo parcialmente. Así, se comienzan las etapas de
análisis y diseño con un conocimiento parcial del sistema que se
quiere desarrollar, elaborando un prototipo que cumple con los
requisitos conocidos.

Dicho prototipo será entregado al cliente, de forma que con su uso
contribuirá al conocimiento de dicho sistema y del que se podrán
extraer nuevos requisitos funcionales desconocidos en un principio,
contribuyendo así, paulatinamente, a un refinado del prototipo al
final de cada iteración.

Como puede deducirse de lo anterior, surgen dos grandes inconvenientes
que no siempre es posible asumir:

\begin{itemize}
\item El cliente final debe implicarse activamente en el proceso de
  desarrollo del sistema, probando y evaluando los diferentes
  prototipos. Dependiendo del cliente, esto supone una ventaja
  o un gran inconveniente, ya que no siempre desarrollador y cliente
  se entienden.

\item Problema de mantenimiento del prototipo: la utilización del
  prototipado evolutivo tiene un gran inconveniente, y es que al
  reutilizarse continuamente en cada iteración el prototipo resultante
  de la anterior, se corre el riesgo de especializar demasiado el
  software y hacer que el mantenimiento futuro sea un problema para
  personas no familiarizadas con el proyecto.
\end{itemize}

Dado que la principal característica del prototipado evolutivo es la
construcción de prototipos funcionales desde los primeros estadios del
desarrollo, y que las necesidades del proyecto Hesperia exigían algo
similar, hizo que se eligiera esta metodología en lugar de otras.

Además, dado que durante el desarrollo del proyecto se adquirirá mayor
conocimiento tanto de las herramientas del \mw\ de comunicación como
del estándar en si mismo, será algo muy a favor usar esta metodología
para poder revisar los prototipos ya ``finalizados'' para añadir
nuevas funcionalidades en función de las nuevas características que
vaya adquiriendo el proyecto.

Dadas las características del proyecto, en el que según se puede leer
en el capítulo  \ref{chap:objetivos} (objetivos), se ha necesitado
adaptar un poco la metodología: en lugar de considerar la
infraestructura como un único prototipo que va siendo refinado a cada
iteración, se han utilizado estas para ir desarrollando nuevos
prototipos o modificando algunos desarrollados previamente.

El problema de la participación del cliente se solventó gracias a que
el proyecto Hesperia, como cliente de algunos prototipos, se mostró
participativo, requiriendo nuevas funcionalidades e identificando
claramente ciertos requisitos funcionales.

En cuanto al problema de mantenimiento, se ha tratado de paliar en la
mayor medida posible utilizando un sistema de control de versiones y
generando documentación sobre cada uno de los prototipos.

\section{Herramientas}
\label{sec:herramientas}

A continuación se adjunta una lista pormenorizada de lenguajes de
programación, herramientas software y hardware utilizadas durante el
desarrollo del proyecto y la escritura de esta documentación:

\subsection{Lenguajes de programación}
\label{sec:lenguajes}

\begin{itemize}
\item \textbf{Python}: lenguaje interpretado, creado por Guido van
  Roosum. Permite la utilización simultánea de diferentes paradigmas
  (orientación a objetos y programación estructurada o funcional). Se
  escogió debido a su gran flexibilidad, una librería muy amplia de
  módulos y funciones que añaden funcionalidad al lenguaje y, ante
  todo, la rapidez que ofrece a la hora de crear prototipos
  funcionales.

\item \textbf{C++}: lenguaje compilado creado por Bjarne
  Stroustrup. Es compatible con el lenguaje C, al que extiende y
  proporciona mecanismos de orientación a objetos.

\item \textbf{C}: lenguaje compilado desarrollado en la década de los
  70 en los \emph{Laboratorios Bell} por Dennis Ritchie. Corresponde
  al paradigma estructurado, ofreciendo algunas características de
  alto nivel de abstracción y muchas de bajo nivel.
\end{itemize}

\subsection{Plataformas software}
\label{ssec:plattforms}

\begin{description}
\item[\textbf{ZeroC Ice}] \mw\ de comunicaciones orientado a objetos,
  multi-lenguaje y multi-plataforma desarrollado por la empresa ZeroC.
\end{description}

\subsection{Aplicaciones de desarrollo}
\label{sec:aplicaciones}

\begin{description}
\item[\textbf{GNU Make}] herramienta para la generación de ejecutables
  u otros ficheros binarios partiendo de sus ficheros fuente.

\item[\textbf{GNU GCC}] compilador de GNU para C y C++.

\item[\textbf{GNU Emacs}] entorno de desarrollo de GNU. Además de como
  entorno de desarrollo y compilación se ha utilizado también para
  escribir esta documentación.

\item[\textbf{Mercurial}] sistema de control de versiones
  distribuido. Se ha utilizado este sistema tanto para el código del
  proyecto, sus herramientas asociadas y la documentación.

\item[\textbf{Subversion}] otro sistema de control de versiones,
  utilizado durante la participación en el IV Concurso de Software
  Libre de Castilla-La Mancha. También utilizado para descargar
  algunas librerías sólo disponibles desde repositorio de código.

\item[\textbf{EC++}] librería de utilidades para C++.

\item[\textbf{Curl}] librería de transferencia de archivos
  multiprotocolo.

\item[\textbf{PyGTK}] \emph{bindings} para Python de la librería de
  elementos gráficos GTK+.

\item[\textbf{Gstreamer}] plataforma para transmisión
  multimedia. Dispone de una librería para C y \emph{bindings} para
  Python.

\item[\textbf{LibGstRTSPServer}] librería que, apoyándose en
  Gstreamer, permite crear un servidor RTSP en C o Python.

\end{description}

\subsection{Documentación}
\label{sec:docu}


\begin{description}
\item[\textbf{\LaTeX}] sistema para maquetación de documentos de
  carácter técnico y científico \cite{latex}.

\item[\textbf{BibTex}] herramienta y un tipo de fichero asociado para la
  descripción de referencias para documentos escritos con \LaTeX.

\item[\textbf{Dia}] creador de diagramas utilizado para la elaboración de
  los diagramas \textsc{UML}.

\item[\textbf{Inkscape}] herramienta de edición gráfica vectorial,
  utilizada para retocar algunos diagramas e imágenes.

\end{description}

\subsection{Gestión de proyecto}
\label{ssec:management}

\begin{description}
\item[\textbf{Redmine}] Aplicación web para la gestión de proyectos.

\end{description}

\subsection{Herramientas \emph{hardware}}
\label{ssec:hardware_tools}

\begin{description}
\item[\textbf{Cámaras AXIS}] El laboratorio \ac{ARCO}, dentro del
  proyecto Hesperia, adquirió una serie de cámaras \ac{IP} del
  fabricante AXIS. En concreto, se utilizaron dos modelos diferentes:
  el \emph{212 PTZ} y el \emph{214 PTZ}. Ambos cuentan con controles
  de giro (\emph{pan} y \emph{tilt}) y de \emph{zoom}.

\item[Webcam Logitech Orbit] También gracias al laboratorio \ac{ARCO}
  se pudo contar con varias de estas cámaras web.

\item[\textbf{Diferentes PC}] Para el desarrollo del proyecto y la
  documentación se utilizaron diversos PC, tanto sobremesa como
  portátiles.
\end{description}

Desde estas líneas quiero aprovechar para agradecer al grupo \ac{ARCO}
el permiso para utilizar todos los recursos \emph{hardware} y
\emph{software} que se han utilizado durante la elaboración del
proyecto, antes, durante y después de trabajar con el grupo.
