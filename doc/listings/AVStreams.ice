// -*- mode: c++; coding: utf-8 -*-

//
// Slice definitions for AV streaming in Ice
//
//

#ifndef AV_STREAMS_ICE
#define AV_STREAMS_ICE

#include <Hesperia/PropertyService.ice>

module AVStreams {

    //
    // Definitions
    //

    struct QoS {
	string QoSType;
	PropertyService::Properties QoSParams;
    };

    sequence<QoS>    StreamQoS;
    sequence<string> FlowSpec;
    sequence<string> ProtocolSpec;
    sequence<byte>   Key;

    struct SFPStatus {
	bool isFormatted;
	bool isSpecialFormat;
	bool seqNums;
	bool timestamps;
	bool sourceIndicators;
    };

    enum FlowState {stopped, started, dead};
    enum DirType {dirIn, dirOut};

    struct FlowStatus {
	string    flowName;
	DirType   directionality;
	FlowState status;
	SFPStatus theFormat;
	QoS       theQoS;
    };

    //
    // Exceptions
    //

    exception NotSupported {};
    exception PropertyException {};
    exception FPError { string flowName; };
    exception StreamOpFailed { string reason; };
    exception StreamOpDenied { string reason; };
    exception NoSuchFlow {};
    exception QoSRequestFailed { string reason; };

    //
    // Interfaces
    //

    interface BasicStreamCtrl extends PropertyService::PropertySetDef {

	// Empty FlowSpec => apply operation to all flows
	void stop(FlowSpec theSpec)
	    throws NoSuchFlow;

	void start(FlowSpec theSpec)
	    throws NoSuchFlow;

	void destroy(FlowSpec theSpec)
	    throws NoSuchFlow;

	bool modifyQoS(FlowSpec      theSpec,
		       StreamQoS     reqQoS,
		       out StreamQoS gotQoS)
	    throws NoSuchFlow, QoSRequestFailed;

	// Called by StreamEndPoint when something goes wrong with a flow
	void pushEvent(PropertyService::Property theEvent);

	void setFPStatus(FlowSpec  theSpec,
			 string    fpName,
			 SFPStatus fpSettings)
	    throws NoSuchFlow, FPError;

	Object* getFlowConnection(string flowName)
	    throws NoSuchFlow, NotSupported;

	void setFlowConnection(string  flowName,
			       Object* flowConnection)
	    throws NoSuchFlow, NotSupported;
    };

    interface Negotiator {
	bool negotiate(Negotiator* remoteNegotiator,
		       StreamQoS   qosSpec);
    };

    interface VDev;
    interface MMDevice;
    interface StreamEndPoint;
    interface StreamEndPointA;
    interface StreamEndPointB;

    interface StreamCtrl extends BasicStreamCtrl {

	bool bindDevs(MMDevice*     aParty,
		      MMDevice*     bParty,
		      StreamQoS     reqQoS,
		      FlowSpec      theFlows,
		      out StreamQoS gotQoS)
	    throws StreamOpFailed, NoSuchFlow, QoSRequestFailed;

	bool bind(StreamEndPointA* aParty,
		  StreamEndPointB* bParty,
		  StreamQoS        reqQoS,
		  FlowSpec         theFlows,
		  out StreamQoS    gotQoS)
	    throws StreamOpFailed, NoSuchFlow, QoSRequestFailed;

	void unbindParty(StreamEndPoint* theEp,
			 FlowSpec        theSpec)
	    throws StreamOpFailed, NoSuchFlow;

	void unbind ()
	    throws StreamOpFailed;
    };

    interface MCastConfigIf extends PropertyService::PropertySetDef {

	bool setPeer(Object*       peer,
		     StreamQoS     reqQoS,
		     FlowSpec      theSpec,
		     out StreamQoS gotQoS)
	    throws QoSRequestFailed, StreamOpFailed;

	void configure(PropertyService::Property aConfiguration);

	void setInitialConfiguration(PropertyService::Properties initial);

	// Uses <formatName> standardized by OMG and IETF
	void setFormat(string flowName,
		       string formatName)
	    throws NotSupported;

	// Note, some of these device params are standardized by OMG
	void setDevParams(string                      flowName,
			  PropertyService::Properties newParams)
	    throws PropertyService::MultipleExceptions, StreamOpFailed;
    };

    interface StreamEndPoint extends PropertyService::PropertySetDef {

	void stop(FlowSpec theSpec)
	    throws NoSuchFlow;

	void start(FlowSpec theSpec)
	    throws NoSuchFlow;

	void destroy(FlowSpec theSpec)
	    throws NoSuchFlow;

	bool connect(StreamEndPoint* responder,
		     StreamQoS       reqQoS,
		     FlowSpec        theSpec,
		     out StreamQoS   gotQoS)
	    throws NoSuchFlow, QoSRequestFailed, StreamOpFailed;

	bool requestConnection(StreamEndPoint* initiator,
			       bool            isMCast,
			       FlowSpec        reqSpec,
			       StreamQoS       reqQoS,
			       out FlowSpec    gotSpec,
			       out StreamQoS   gotQoS)
	    throws StreamOpDenied, NoSuchFlow, QoSRequestFailed, FPError;

	bool modifyQoS(FlowSpec      theFlows,
		       StreamQoS     reqQoS,
		       out StreamQoS gotQoS)
	    throws NoSuchFlow, QoSRequestFailed;

	bool setProtocolRestriction(ProtocolSpec thePSpec);

	void disconnect (FlowSpec theSpec)
	    throws NoSuchFlow, StreamOpFailed;

	void setFPStatus(FlowSpec  theSpec,
			 string    fpName,
			 SFPStatus fpSettings)
	    throws NoSuchFlow, FPError;

	Object* getFep(string flowName)
	    throws NotSupported, NoSuchFlow;

	string addFep(Object* theFep)
	    throws StreamOpFailed;

	void removeFep(string fepName)
	    throws StreamOpFailed;

	void setNegotiator(Negotiator* newNegotiator);

	void setKey(string flowName,
		    Key    theKey);

	void setSourceId(long sourceId);
    };

    interface StreamEndPointA extends StreamEndPoint {

	bool multiconnect(FlowSpec      reqSpec,
			  StreamQoS     reqQoS,
			  out FlowSpec  gotSpec,
			  out StreamQoS gotQoS)
	    throws NoSuchFlow, QoSRequestFailed, StreamOpFailed;

	bool connectLeaf(StreamEndPointB* theEp,
			 StreamQoS        reqQoS,
			 FlowSpec         theFlows,
			 out StreamQoS    gotQoS)
	    throws StreamOpFailed, NoSuchFlow, QoSRequestFailed, NotSupported;

	void disconnectLeaf(StreamEndPointB* theEp,
			    FlowSpec         theSpec)
	    throws StreamOpFailed, NoSuchFlow;
    };

    interface StreamEndPointB extends StreamEndPoint {

	bool multiconnect(FlowSpec      reqSpec,
			  StreamQoS     reqQoS,
			  out FlowSpec  gotSpec,
			  out StreamQoS gotQoS)
	    throws StreamOpFailed, NoSuchFlow, QoSRequestFailed, FPError;
    };

    interface VDev extends PropertyService::PropertySetDef {

	bool setPeer(StreamCtrl*   theCtrl,
		     VDev*         thePeerDev,
		     StreamQoS     reqQoS,
		     FlowSpec      theSpec,
		     out StreamQoS gotQoS)
	    throws NoSuchFlow, QoSRequestFailed, StreamOpFailed;

	bool setMCastPeer(StreamCtrl*    theCtrl,
			  MCastConfigIf* aMCastConfigIf,
			  StreamQoS      reqQoS,
			  FlowSpec       theSpec,
			  out StreamQoS  gotQoS)
	    throws NoSuchFlow, QoSRequestFailed, StreamOpFailed;

	void configure(PropertyService::Property theConfigMesg)
	    throws PropertyException, StreamOpFailed;

	void setFormat(string flowName,
		       string formatName)
	    throws NotSupported;

	void setDevParams(string                      flowName,
			  PropertyService::Properties newParams)
	    throws PropertyException, StreamOpFailed;

	bool modifyQoS(StreamQoS     reqQoS,
		       FlowSpec      theSpec,
		       out StreamQoS gotQoS)
	    throws NoSuchFlow, QoSRequestFailed;
    };

    interface MMDevice extends PropertyService::PropertySetDef {

	StreamEndPointA* createA(StreamCtrl*   theRequester,
				 StreamQoS     reqQoS,
				 string        reqNamedVDev,
				 FlowSpec      theSpec,
				 out VDev*     theVDev,
				 out bool      metQoS,
				 out string    gotNamedVDev,
				 out StreamQoS gotQoS)
	    throws StreamOpFailed, StreamOpDenied, NotSupported,
	    QoSRequestFailed, NoSuchFlow;

	StreamEndPointB* createB(StreamCtrl*   theRequester,
				 StreamQoS     reqQoS,
				 string        reqNamedVDev,
				 FlowSpec      theSpec,
				 out VDev*     theVDev,
				 out bool      metQoS,
				 out string    gotNamedVDev,
				 out StreamQoS gotQoS)
	    throws StreamOpFailed, StreamOpDenied, NotSupported,
	    QoSRequestFailed, NoSuchFlow;

	StreamCtrl* bind(MMDevice*     peerDevice,
			 StreamQoS     reqQoS,
			 FlowSpec      theSpec,
			 out bool      metQoS,
			 out StreamQoS gotQoS)
	    throws StreamOpFailed, NoSuchFlow, QoSRequestFailed;

	StreamCtrl* bindMCast(MMDevice*     firstPeer,
			      StreamQoS     reqQoS,
			      FlowSpec      theSpec,
			      out bool      metQoS,
			      out StreamQoS gotQoS)
	    throws StreamOpFailed, NoSuchFlow, QoSRequestFailed;

	void destroy(StreamEndPoint* theEp,
		     string          vDevName)
	  throws NotSupported; /* ie VDev not found */

	string addFDev(Object* theFDev)
	    throws NotSupported, StreamOpFailed;

	Object* getFDev(string flowName)
	    throws NotSupported, NoSuchFlow;

	void removeFDev(string flowName)
	    throws NotSupported, NoSuchFlow;
    };

};


#endif
