/* -*- mode: c++; coding: utf-8 -*- */

#include <Ice/BuiltinSequences.ice>

module GstIce {
  class LightBuffer {
	string capabilities;
	Ice::ByteSeq data;
  };

  interface Receiver {
	void sendBuffer(LightBuffer buffer);
  };
};
