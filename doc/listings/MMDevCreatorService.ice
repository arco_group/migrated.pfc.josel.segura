// -*- mode: c++; coding: utf-8 -*-

#include <Hesperia/DUO.ice>
#include <Hesperia/PropertyService.ice>

module Argos {

  interface DelegatedApp {
	void configure(PropertyService::Property configuration);
	PropertyService::Property getConfiguration();

	void play();
	void stop();
  };

  interface MMDeviceCreator {
	void addServer(DelegatedApp* theApplication,
		       PropertyService::Properties capabilities);

	void addServerWithContainer(DelegatedApp* theApplication,
				    DUO::ObjectPrxDict objects,
				    PropertyService::Properties capabilities);

	void delServer(DelegatedApp* theApplication);

	void addRenderer(DelegatedApp* theApplication,
			 PropertyService::Properties capabilities);

	void addRendererWithContainer(DelegatedApp* theApplication,
				      DUO::ObjectPrxDict objects,
				      PropertyService::Properties capabilities);
	
	void delRenderer(DelegatedApp* theApplication);
  };
};
