#include <Ice/BuiltinSequences.ice>
#include <Ice/Identity.ice>

module DUO {

  module Pulse {
    interface W { void set(Ice::Identity oid); };
  };

  module IBool {
    interface R { bool get(); };
    interface W { void set(bool v, Ice::Identity oid); };
  };

  module IByte {
    interface R { byte get(); };
    interface W { void set(byte v, Ice::Identity oid); };
  };

  module IInt {
    interface R { int get(); };
    interface W { void set(int v, Ice::Identity oid); };
  };

  module ILong {
    interface R { long get(); };
    interface W { void set(long v, Ice::Identity oid); };
  };

  module IFloat {
    interface R { float get(); };
    interface W { void set(float v, Ice::Identity oid); };
  };

  module IString {
    interface R { string get(); };
    interface W { void set(string v, Ice::Identity oid); };
  };

  module IByteSeq {
    interface R { Ice::ByteSeq get(); };
    interface W { void set(Ice::ByteSeq v, Ice::Identity oid); };
  };

  module IObject {
    interface R { Object* get(); };
    interface W { void set(Object* v, Ice::Identity oid); };
  };

[...]

};