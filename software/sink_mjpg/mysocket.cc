/* -*- coding: utf-8 -*- */

#include <mysocket.hh>

using namespace std;
using namespace mysocket;

MySocket::MySocket ():
  _received(0){

  if ((_sock = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    throw new Error("Error creating socket");

}

void
MySocket::connect(const std::string &hostname, int port) {

  struct sockaddr_in pin;
  struct hostent *hp;

  if ((hp = gethostbyname(hostname.c_str())) == 0)
    throw new GAIError("Name or service not known");

  /* fill in the socket structure with host information */
  pin.sin_family = AF_INET;
  pin.sin_addr.s_addr = ((struct in_addr *)(hp->h_addr))->s_addr;

  pin.sin_port = htons(port);

  if (::connect(_sock,(struct sockaddr *)  &pin, sizeof(pin)) == -1) {
    throw new Error("Connection refused");
  }
}

int
MySocket::send(const string &message) {
  if (::send(_sock, message.c_str(), message.size(), 0) == -1) {
    throw new Error(0);
  }

  return message.size();
}

int
MySocket::recv(unsigned char *buf, int size) {
  return ::recv(_sock, buf, size, 0);
}



