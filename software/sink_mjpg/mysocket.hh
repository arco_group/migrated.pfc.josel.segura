/* -*- mode: c++; coding: utf-8 -*- */

#ifndef MYPYSOCK
#define MYPYSOCK

#include <iostream>
#include <string>
#include <stdexcept>

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>

namespace mysocket {

  class MySocket {
    int _sock;
    int _received;

  public:
    MySocket ();
    //~MySocket

    void connect(const std::string &hostname, int port);

    int send(const std::string &message);
    int recv(unsigned char *buf, int size);

  };

  class Error: public std::runtime_error {
  protected:
    int _code;
    std::string _message;

  public:
    Error (const std::string &message) : std::runtime_error(message) {}
    ~Error () throw() {}
  };

  class GAIError : Error {

  public:
    GAIError(const std::string message) : Error(message) {}

  };

}

#endif


