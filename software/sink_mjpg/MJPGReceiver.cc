/* -*- coding: utf-8 -*- */

#include <easyc++/string.h>
#include <easyc++/logger.h>
#include <string.h>

#include "MJPGReceiver.hh"

MJPGReceiver::MJPGReceiver(const std::string &hostname):
  _boundary("\r\n"), _received(0) {

  _sock = mysocket::MySocket();

  _sock.connect(hostname, 80);
  _sock.send("GET /axis-cgi/mjpg/video.cgi?resolution=320x240 HTTP/1.0\r\n\r\n");

  /* We have to search the boundary on the HTTP header*/
  std::vector<std::string> v;
  std::string old_readed;
  int received;

  /*
   * We can use C++ and C standard string functions and libeasyc++ library because
   * the HTTP header are in textual format and won't have a \0 character.
   */
  while (true) {
    v = ec::string::split(old_readed, "\r\n\r\n");

    /*HTTP header completely readed*/
    if (v.size() > 1) {
	  std::cout << "FOUND" << std::endl;
      v = ec::string::split(v.front(), "boundary=");

      /*boundary def founded*/
      if (v.size() > 1) {
		_boundary.append(v.at(1).append("\r\n"));
		break;
      }

      else
		throw 3;

    }

    /*
     * "boundary=" not founded, re-read socket
     */
    received = refreshBuffer();
    old_readed.append((const char *)_buf, received);

  }
}

int
MJPGReceiver::refreshBuffer() {
  _received = _sock.recv(_buf, BUFSIZE);
  if (_received < BUFSIZE)
    _buf[_received] = '\0'; // it's important if the _buf have only textual data

  else if (_received == 0)
    throw 4;

  return _received;

}

int
MJPGReceiver::getFrame(unsigned char **dst) {
  int imgLength, unreceived, imgPos;
  unsigned char *ptr;
  std::string old_readed;
  int received;
  std::vector<std::string> v;

  old_readed = std::string((char *)_buf);




  while (true) {
    v = ec::string::split(old_readed, _boundary);

    if (v.size() > 1) {
      v = ec::string::split(v.at(1), "\r\n\r\n");

      /* There is 2 image headers on the current readed string or
       * we readed the boundary definition first
       */
      if (v.size() > 1) {
	imgLength = atoi(ec::string::split(v.front(), "Content-Length: ").back().c_str());
	unreceived = imgLength;
	break;

      }
    }

    received = refreshBuffer();
    //_received = _sock.recv(_buf, BUFSIZE);
    //
    //if (_received < BUFSIZE)
    //  _buf[_received] = '\0';
    //
    //else if (_received == 0)
    //  throw 4;

    old_readed.append((char *)_buf, received);

  }

  old_readed = std::string((char *)_buf);
  imgPos = old_readed.find("\xff\xd8");

  *dst = (unsigned char *)malloc(imgLength);

  memcpy(dst[0], _buf+imgPos, _received-imgPos);

  ptr = dst[0];
  ptr += (_received - imgPos);
  unreceived = imgLength - _received + imgPos; // - (_received - imgPos)

  while (unreceived > 0) {
    _received = _sock.recv(ptr, unreceived);
    ptr += _received;
    unreceived -= _received;
  }

  received = refreshBuffer();

  return imgLength;

}
