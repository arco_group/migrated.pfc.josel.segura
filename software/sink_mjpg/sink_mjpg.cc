/* -*- coding: utf-8 -*- */

#include <easyc++/logger.h>
#include <sink_mjpg.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>

#include "MJPGReceiver.hh"

/*
 * Function to obtain a R5G6B5 representation of a row of the image
 */
int paintRowRGB(Byte *dest, Byte *src, int width) {
  int imgStartCol, imgEndCol, fbStartCol, fbEndCol;
  int imgBpp, fbBpp;

  imgBpp = 3;
  fbBpp = 2;

  //memset(dest, 0, 640);

  if (width < 320) {
    imgStartCol = 0;
    imgEndCol = width - 1;
    fbStartCol = (320 - width) / 2;
    fbEndCol = fbStartCol + width;
  }

  else {
    imgStartCol = (width - 320) / 2;
    imgEndCol = imgStartCol + 319;
    fbStartCol = 0;
    fbEndCol = 319;
  }

  /*
   * If the image is smaller than the screen, paint a black line to the
   * start of the image.
   * Else, calculate where the image is start to being painted
   */

  for (int i=0; i < fbStartCol; i++) {
    for(int j=0; j < fbBpp; j++)
      dest[(i*imgBpp)+j] = 0;
  }

  /*
   * Paint src to dest
   */
  for(; imgStartCol <= imgEndCol; imgStartCol++, src += imgBpp, dest += fbBpp) {
    Byte r, g, b;

    r = src[0] >> 3;
    g = src[1] >> 2;
    b = src[2] >> 3;

    dest[0] = (g << 5) | b;
    dest[1] = (r << 3) | (g >> 3);

  }

  /*
   * Paint, if necessary, a black line to the end
   */
  for (int i=fbEndCol; i < 320; i++) {
    for (int j=0; j < fbBpp; j++)
      dest[(i*imgBpp)+j] = 0;
  }

  return 1;

}

LOCAL(unsigned int)
jpeg_getc (j_decompress_ptr cinfo) {
  struct jpeg_source_mgr * datasrc = cinfo->src;

  if (datasrc->bytes_in_buffer == 0) {
    if (! (*datasrc->fill_input_buffer) (cinfo))
      return 0; //ERREXIT(cinfo, JERR_CANT_SUSPEND);
  }

  datasrc->bytes_in_buffer--;
  return GETJOCTET(*datasrc->next_input_byte++);

}

METHODDEF(boolean)
print_text_marker (j_decompress_ptr cinfo) {
  boolean traceit = (cinfo->err->trace_level >= 1);
  INT32 length;
  unsigned int ch;
  unsigned int lastch = 0;

  length = jpeg_getc(cinfo) << 8;
  length += jpeg_getc(cinfo);
  length -= 2;			/* discount the length word itself */

  if (traceit) {
    if (cinfo->unread_marker == JPEG_COM)
	  ec::error() << "Comment, length " << (long) length << std::endl;
    else			/* assume it is an APPn otherwise */
	  ec::error() << "APP" << cinfo->unread_marker - JPEG_APP0 << " length " << length << std::endl;
  }

  while (--length >= 0) {
    ch = jpeg_getc(cinfo);
    if (traceit) {
      /* Emit the character in a readable form.
       * Nonprintables are converted to \nnn form,
       * while \ is converted to \\.
       * Newlines in CR, CR/LF, or LF form will be printed as one newline.
       */
      if (ch == '\r') {
	ec::error() << std::endl;
      } else if (ch == '\n') {
	if (lastch != '\r')
	  ec::error() << std::endl;
      } else if (ch == '\\') {
	ec::error() << "\\\\";
      } else if (isprint(ch)) {
	putc(ch, stderr);
      } else {
	ec::error() << "\\%03o" << ch;
      }
      lastch = ch;
    }
  }

  if (traceit)
    ec::error() << std::endl;

  return TRUE;
}

void
showJpeg () {
  struct jpeg_decompress_struct cinfo;
  struct jpeg_error_mgr jerr;

  JDIMENSION num_scanlines;

  JSAMPARRAY buffer;
  JDIMENSION buffer_height;

  FILE *input_file;

  input_file = fopen(IMAGE, "rb");

  if (!input_file) {
    ec::error() << "Cannot open " << IMAGE << " r/o" << std::endl;
    return;
  }

  /* Initialize the JPEG decompression object with default error handling. */
  cinfo.err = jpeg_std_error(&jerr);
  jpeg_create_decompress(&cinfo);

  jpeg_set_marker_processor(&cinfo, JPEG_COM, print_text_marker);
  jpeg_set_marker_processor(&cinfo, JPEG_APP0+12, print_text_marker);

  jpeg_stdio_src(&cinfo, input_file);
  (void) jpeg_read_header(&cinfo, TRUE);

  //cinfo.scale_num = 1;
  //cinfo.scale_denom = 2;
  /* Calculate output image dimensions so we can allocate space */
  jpeg_calc_output_dimensions(&cinfo);

  /* Create decompressor output buffer. */
  JDIMENSION row_width;
  row_width = cinfo.output_width * cinfo.output_components;
  buffer = (*cinfo.mem->alloc_sarray)
    ((j_common_ptr) &cinfo, JPOOL_IMAGE, row_width, (JDIMENSION) 1);
  buffer_height = 1;

  // Determine resizing
  int imgRowStart, imgRowEnd;
  int fbRowStart, fbRowEnd;

  if (cinfo.output_height <= 240) {
    fbRowStart = (240 - cinfo.output_height) / 2;
    fbRowEnd = fbRowStart + cinfo.output_height -1;
    imgRowStart = 0;
    imgRowEnd = cinfo.output_height - 1;
  }

  else {
    fbRowStart = 0;
    fbRowEnd = 239;
    imgRowStart = (cinfo.output_height - 240) / 2;
    imgRowEnd = imgRowStart + 239;
  }

  // Open frame buffer
  int fb = open( FBDEV, O_RDWR );

  if (fb <= 0) {
	ec::error() << "Error: could not open frame buffer (errno=" << errno << ")" << std::endl;
    (void) jpeg_finish_decompress(&cinfo);
    jpeg_destroy_decompress(&cinfo);

    fclose( input_file );
    return;
  }

  Byte fbRow[640];
  int row = 0;

  Byte blackRow[640];
  memset(blackRow, 0, 640);

  /* Start decompressor */
  (void) jpeg_start_decompress(&cinfo);

  for(int i=0; i<fbRowStart; i++)
    write(fb, blackRow, 640);

  while(row < imgRowStart) {
  //for(row=0; row<imgRowStart; row++) {
    num_scanlines = jpeg_read_scanlines(&cinfo, buffer, buffer_height);
    row = cinfo.output_scanline;
  }

  /* Process data */
  while (cinfo.output_scanline < cinfo.output_height) {
  //for(; cinfo.output_scanline < cinfo.output_height; row++) {
    num_scanlines = jpeg_read_scanlines(&cinfo, buffer, buffer_height);
    row = cinfo.output_scanline;

    paintRowRGB(fbRow, buffer[0], cinfo.output_width);
    write(fb, fbRow, 640);

  }

  for(; row < 240; row++)
    write(fb, blackRow, 640);

  close( fb );

  (void) jpeg_finish_decompress(&cinfo);
  jpeg_destroy_decompress(&cinfo);

  fclose( input_file );
}


void saveToFile(Byte *img, int length) {
  FILE *tmp;

  tmp = fopen(IMAGE, "wb");
  fwrite(img, 1, length, tmp);
  fclose(tmp);


}

int
work(const std::string &hostname) {
  int imgSize;
  unsigned char *dst;

  try {
    MJPGReceiver::MJPGReceiver client (hostname);

    while (true) {
      imgSize = client.getFrame(&dst);
      saveToFile((Byte *)dst, imgSize);
      showJpeg();
    }
  }

  catch(int &e) {
    switch(e) {
    case 1:
      ec::error() << "Error creating socket" << std::endl;
      break;

    case 2:
      ec::error() << "Error connection socket" << std::endl;
      break;

    case 3:
      ec::error() << "Error sending message" << std::endl;
      break;

    case 4:
      ec::error() << "HTTP header bad constructed (missing boundary definition"
	   << std::endl;

    }
  }

  return 0;
}

int
main(int argc, char **argv) {

  if (argc > 2) {
	ec::error() << "Demasiados argumentos" << std::endl;
    //      help();
    exit(1);
  }

  else if (argc < 2) {
	ec::error() << "Debe pasar la dirección del flujo" << std::endl;
    //      help();
    exit(1);
  }

  //capturing controlC
  signal(SIGINT, keyboardInterrupt);

  work(std::string(argv[1]));

}

void keyboardInterrupt(int signum) {
  exit(0);
}



