/* -*- mode: c++; coding: utf-8 -*- */

#include <jpeglib.h>

/*
 * Default frambuffer device and image path
 */
#define FBDEV "/dev/fb0"
#define IMAGE "/tmp/image.jpg"

#define SMALLER 0x01
#define BIGGER 0x02

#define BOUNDARY_DEF 0
#define BOUNDARY_SEARCH 1
#define IMG_HTTP_HEAD 2
#define DOWNLOAD_IMG 3

#define MAX(a,b) ((a) > (b)? (a) : (b))
#define MIN(a,b) ((a) > (b)? (b) : (a))

typedef unsigned char Byte;

/*
 * Global
 */
void keyboardInterrupt(int signum);
int paintRowRGB(Byte* dest, Byte* src, int width);
LOCAL(unsigned int) jpeg_getc (j_decompress_ptr cinfo);
METHODDEF(boolean) print_text_marker (j_decompress_ptr cinfo);
void showJpeg();
void saveToFile(Byte *img, int length);






