/* -*- coding: utf-8 -*- */

#include <string>
#include <vector>

#include "mysocket.hh"

#define BUFSIZE 150

class MJPGReceiver {
  mysocket::MySocket _sock;
  std::string _boundary;
  unsigned char _buf[BUFSIZE];
  int _received;

  int refreshBuffer();

public:
  MJPGReceiver (const std::string &hostname);
  int getFrame(unsigned char **dst);

};
