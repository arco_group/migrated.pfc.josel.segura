/* -*- coding: utf-8 -*- */

#include <stdio.h>
#include <stdlib.h>

#include <gst/gst.h>
#include <gst/interfaces/xoverlay.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>

gboolean on_expose_cb(GtkWidget * widget, GdkEventExpose * event, gpointer data);
gboolean on_delete_cb(GtkWidget *widget, GdkEvent *event, gpointer user_data);
gboolean on_src_pad_added(GstElement *gstelement, GstPad *new_pad, gpointer user_data);
gboolean on_decoder_pad_added(GstElement *gstelement, GstPad *new_pad, gpointer user_data);

int
main(int argc, char **argv) {
  /* GTK vars */
  GtkWindow *win;

  /* Gstreamer vars */
  GstStateChangeReturn ret;
  GstElement *pipeline, *src, *decoder, *sink;

  gtk_init(NULL, NULL);
  gst_init(NULL, NULL);

  pipeline = gst_pipeline_new("my_pipeline");
  src  = gst_element_factory_make("rtspsrc", NULL);
  decoder = gst_element_factory_make("decodebin2", "decoder");
  sink = gst_element_factory_make("xvimagesink", "my_sink");

  g_object_set(G_OBJECT(src), "location", "rtsp://161.67.108.9/mpeg4/1/media.amp", NULL);

  g_object_set(G_OBJECT (sink), "sync", FALSE, NULL);
  g_object_set(G_OBJECT(sink), "force-aspect-ratio", TRUE, NULL);

  if (!sink)
	g_print ("output could not be found - check your install\n");

  gst_bin_add_many(GST_BIN (pipeline), src, decoder, sink, NULL);

  win = GTK_WINDOW(gtk_window_new(GTK_WINDOW_TOPLEVEL));

  gtk_window_set_decorated(win, FALSE);
  gtk_window_fullscreen(win);

  /* Connecting signals */
  g_signal_connect(G_OBJECT(src), "pad-added",
				   G_CALLBACK(on_src_pad_added), decoder);

  g_signal_connect(G_OBJECT(decoder), "pad-added",
				   G_CALLBACK(on_decoder_pad_added), sink);


  g_signal_connect(G_OBJECT(win), "expose-event",
				   G_CALLBACK(on_expose_cb), sink);

  g_signal_connect(G_OBJECT(win), "delete-event",
				   G_CALLBACK(on_delete_cb), pipeline);



  ret = gst_element_set_state (pipeline, GST_STATE_PLAYING);

  if (ret == GST_STATE_CHANGE_FAILURE)
	g_print ("Failed to start up pipeline!\n");

  gtk_widget_show_all(GTK_WIDGET(win));

  gtk_main();

  return 0;
}

gboolean
on_expose_cb(GtkWidget * widget, GdkEventExpose * event, gpointer data) {
  gst_x_overlay_set_xwindow_id(GST_X_OVERLAY(data), GDK_WINDOW_XWINDOW(widget->window));
  return TRUE;

}

gboolean
on_delete_cb(GtkWidget *widget, GdkEvent *event, gpointer user_data) {
  GstElement *pipeline = GST_ELEMENT(user_data);
  gst_element_set_state(pipeline, GST_STATE_PAUSED);

  gtk_main_quit();

  return TRUE;
}

gboolean
on_src_pad_added(GstElement *gstelement, GstPad *new_pad, gpointer user_data) {
  printf("New pad added to rtspsrc\n");

  GstElement *decoder = GST_ELEMENT(user_data);

  if (!gst_element_link(gstelement, decoder)) {
	g_print("Failed to link\n");
  }

  printf("Ok\n");

  return TRUE;
}

gboolean
on_decoder_pad_added(GstElement *gstelement, GstPad *new_pad, gpointer user_data) {
  printf("New pad added to decodebin\n");

  GstElement *sink = GST_ELEMENT(user_data);

  if (!gst_element_link(gstelement, sink)) {
	g_print("Failed to link\n");
  }

  return TRUE;
}


