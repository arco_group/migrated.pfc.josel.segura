#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys, time

import Ice

Ice.loadSlice('--all -I%s -I%s %s/Hesperia/PropertyService.ice' %
              ('/home/joseluis/hesperia/slice',
               Ice.getSliceDir(),
               '/home/joseluis/hesperia/slice'))

Ice.loadSlice('Hello.ice')
import Demo

class HelloI(Demo.Hello):
    def sayHello(self, delay, current=None):
        time.sleep(delay)
        print "hello world!"

    def shutdown(self, current=None):
        print 'bye'

import PropertyService
import P

class PSClient(Ice.Application):
    def run(self, argv):
        comm = self.communicator()

        ps_prx = comm.stringToProxy('PropertySetFactory @ AutoPropertyService.Adapter')
        print 'Property ids\n\t', ps_prx.ice_ids()

        adapter = comm.createObjectAdapterWithEndpoints("Test",
                                                        "tcp")
        adapter.activate()
        prx = adapter.addWithUUID(HelloI())
        identity = prx.ice_getIdentity()

        new_prx = ps_prx.ice_identity(identity)
        print new_prx
        print new_prx.ice_ids()

        comm.waitForShutdown()
        adapter.deactivate()


ps = PSClient()
ps.main(sys.argv)




