#!/usr/bin/python
# -*- coding: utf-8; mode: python -*-

import sys
import Ice

Ice.loadSlice('--all -I%s -I/usr/local/share/slice ../../MediaRender/renderer.ice' % Ice.getSliceDir())

import Argos

print 'Argos imported'

class MyRenderApp(Argos.RenderApplication):
    def configure(self, configuration, context=None):
        print 'configuration'


class MyApp(Ice.Application):
    def run(self, args):
        oa = self.communicator().createObjectAdapterWithEndpoints('Local', 'tcp -h localhost')
        oa.activate()

        obj = MyRenderApp()
        obj.prx = Argos.RenderApplicationPrx.uncheckedCast(oa.addWithUUID(obj))

        renderPrx = self.communicator().stringToProxy(
            'Deployer -t:tcp -h localhost -p 50000')

        renderPrx = Argos.SinkDeployerPrx.checkedCast(renderPrx)
        renderPrx.addRenderer(obj.prx)

        self.shutdownOnInterrupt()
        self.communicator().waitForShutdown()
        print 'algo'

        renderPrx.delRenderer(obj.prx)
        return 0

app = MyApp()
app.main(sys.argv)
