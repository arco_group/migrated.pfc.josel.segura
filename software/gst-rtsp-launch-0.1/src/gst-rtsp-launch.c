/* -*- coding: utf-8; mode: c -*- */

#include <string.h>

#include <glib-object.h>
#include <gst/gst.h>
#include <gst/rtsp-server/rtsp-server.h>

int
main (int argc, char **argv) {
  GMainLoop *loop;
  GstRTSPServer *server;
  GstRTSPMediaMapping *mapping;
  GstRTSPMediaFactory *factory;
  char* path;

  gst_init (&argc, &argv);

  if (argc != 4) {
    g_print ("usage: %s <port> <location> <launch line>\n"
			 "example: %s 8554 test \"( videotestsrc ! ffenc_h263 ! rtph263pay name=pay0 pt=96 "
			 ")\"\n", argv[0], argv[0]);
    return -1;
  }

  path = (char *) malloc(sizeof(char *) * (strlen(argv[2] + 2)));
  path = strcpy(path, "/");
  strcat(path, argv[2]);

  loop = g_main_loop_new (NULL, FALSE);

  /* create a server instance */
  server = gst_rtsp_server_new();

  /* gst_rtsp_server_set_port(server, port); */
  g_object_set_property( G_OBJECT(server), "service", argv[1]);

  /* get the mapping for this server */
  mapping = gst_rtsp_server_get_media_mapping (server);

  /* make a media factory for a test stream. The default media factory can use */
  factory = gst_rtsp_media_factory_new ();
  gst_rtsp_media_factory_set_launch (factory, argv[3]);

  /* attach the test factory to the given url */
  gst_rtsp_media_mapping_add_factory (mapping, path, factory);

  /* don't need the ref to the mapper anymore */
  g_object_unref (mapping);

  /* attach the server to the default maincontext */
  gst_rtsp_server_attach (server, NULL);

  /* start serving */
  g_main_loop_run (loop);

  return 0;
}
