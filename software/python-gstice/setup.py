#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
from distutils.core import setup

version = file('debian/changelog').readline().split()[1][1:-1]

if sys.argv[1] == 'version':
    print version.split('-', 1)[0]

else:
    setup(name         = 'python-gstice',
          version      = version,
          url          = 'http://argos-m.blogspot.com',
          packages     = ['gstice'],
          requires     = ['gobject', 'gst', 'Ice'],

          data_files   = [('share/python-gstice/slice',
                           ['gstice/slice/GstIce.ice'])],
          description   = 'Python library implementing Ice based elements ' \
              'for Gstreamer',
          author       = 'José Luis Segura Lucas',
          author_email = 'josel.segura@alu.uclm.es',
          license      = 'GPL v3 or later'
          )



