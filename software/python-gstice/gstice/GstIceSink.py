# -*- coding: utf-8; mode: python -*-

import gobject
import gst
import Ice

SLICE_DIR = '/usr/share/python-gstice/slice'

Ice.loadSlice('--all -I{0} {1}/GstIce.ice'.format(Ice.getSliceDir(), SLICE_DIR))
import GstIce

class GstIceSink(gst.BaseSink):
    __gproperties__ = {
        'communicator': (gobject.TYPE_PYOBJECT,
                         'Ice communicator',
                         'Ice communicator for this element',
                         gobject.PARAM_CONSTRUCT_ONLY | gobject.PARAM_READWRITE),

        'receiver-proxy': (gobject.TYPE_STRING,
                           'Ice Proxy to the receiver object',
                           'Ice Proxy to the receiver object',
                           '',
                           gobject.PARAM_READWRITE),

        'default-locator': (gobject.TYPE_STRING,
                            'Ice default locator for the distributed object',
                            'Ice default locator for the distributed object',
                            '',
                            gobject.PARAM_READWRITE),

        'debug': (gobject.TYPE_BOOLEAN,
                  'Verbose output mode',
                  'Verbose output mode',
                  False,
                  gobject.PARAM_READWRITE),

        'timeout': (gobject.TYPE_INT,
                    'Default timeout for the receiver proxy',
                    'Default timeout for the receiver proxy',
                    -1, 10000, -1, gobject.PARAM_READWRITE),

        'count': (gobject.TYPE_INT,
                  'Counter of frames sent',
                  'Counter of frames sent',
                  0, 1000000, 0, gobject.PARAM_READABLE)
        }

    __gsttemplates__ = (
        gst.PadTemplate("sink",
                        gst.PAD_SINK,
                        gst.PAD_ALWAYS,
                        gst.caps_new_any()),
        )

    def __init__(self, name='icesink0', communicator=Ice.initialize()):
        self._ic = None
        self._receiver = None
        self._debug = False
        self._timeout = -1

        self._counter = 0

        self.__gobject_init__(name=name, communicator=communicator)

    def do_get_property(self, prop):
        if prop.name == 'communicator':
            return self._ic

        elif prop.name == 'receiver-proxy':
            return str(self._receiver)

        elif prop.name == 'default-locator':
            return str(self._ic.getDefaultLocator())

        elif prop.name == 'debug':
            return self._debug

        elif prop.name == 'timeout':
            return self._timeout

        elif prop.name == 'count':
            return self._counter

    def do_set_property(self, prop, value):
        if prop.name == 'communicator':
            self._ic = value

        elif prop.name == 'receiver-proxy':
            try:
                self._receiver = GstIce.ReceiverPrx.uncheckedCast( \
                    GstIce.ReceiverPrx.uncheckedCast( \
                        self._ic.stringToProxy(value))).ice_timeout(self._timeout)

            except:
                raise AttributeError, 'error parsing the stringfied proxy'

        elif prop.name == 'default-locator':
            try:
                locator = Ice.LocatorPrx.checkedCast(
                    self._ic.stringToProxy(value))

                self._ic.setDefaultLocator(locator)

            except Ice.Exception:
                pass

        elif prop.name == 'debug':
            self._debug = value

        elif prop.name == 'timeout':
            self._timeout = value

            if self._receiver:
                self._receiver = GstIce.ReceiverPrx.uncheckedCast( \
                    self._receiver.ice_timeout(self._timeout))

    # method invoked by Gstreamer when the element is connected to a source
    def do_render(self, buf):
        self._counter += 1
        if self._debug:
            print('Received buffer')

        if self._receiver != None:
            my_buf = GstIce.LightBuffer()
            my_buf.data = buf.data

            caps = buf.get_caps()

            if caps:
                my_buf.capabilities = caps.to_string()

            try:
                self._receiver.sendBuffer(my_buf)

            except Ice.TimeoutException:
                if self._debug:
                    print('Timeout sending')


        return gst.FLOW_OK

gobject.type_register(GstIceSink)
