# -*- coding: utf-8; mode: python -*-

import threading

import gst
import gobject

import Ice

SLICE_DIR = '/usr/share/python-gstice/slice'
# SLICE_DIR = '/home/joseluis/uni/pfc/software/python-gstice/gstice/slice'

Ice.loadSlice('--all -I{0} {1}/GstIce.ice'.format(Ice.getSliceDir(), SLICE_DIR))

import GstIce

gobject.threads_init()

class GstIceSrc(gst.Element):
    __gstdetails = (
        'Ice source',
        'GstIceSrc.py',
        'Takes a buffer stream from Ice',
        'José Luis Segura <josel.segura@gmx.es>',
        )

    __srctemplate = gst.PadTemplate(
        'src',
        gst.PAD_SRC,
        gst.PAD_SOMETIMES,
        gst.caps_new_any(),
        )

    __gproperties__ = {
        'communicator': (gobject.TYPE_PYOBJECT,
                         'Ice communicator',
                         'Ice communicator for this element',
                         gobject.PARAM_CONSTRUCT_ONLY | gobject.PARAM_READWRITE),

        'receiver-proxy': (gobject.TYPE_STRING,
                           'Ice Proxy of the receiver object',
                           'Ice Proxy of the receiver object',
                           '',
                           gobject.PARAM_READABLE),

        'debug': (gobject.TYPE_BOOLEAN,
                  'Verbose output mode',
                  'Verbose output mode',
                  False,
                  gobject.PARAM_READWRITE),

        }

    __gsttemplates__ = ( __srctemplate )

    class ReceiverI(GstIce.Receiver):
        def __init__(self, callback):
            self._cb = callback

        def sendBuffer(self, buf, current):
            if self._cb:
                b = gst.Buffer(buf.data)

                if buf.capabilities:
                    caps = gst.Caps(buf.capabilities)
                    b.set_caps(caps)

                self._cb(b)

    def __init__(self, name='icesrc0', communicator=Ice.initialize()):
        self._ic = None
        self._debug = False

        self._receiver = GstIceSrc.ReceiverI(self._receive_buffer)
        self._receiver_proxy = None

        self.__gobject_init__(name=name, communicator=communicator)

        self._srcpad = gst.Pad(GstIceSrc.__srctemplate)

    def _receive_buffer(self, buf):
        if self._debug:
            print('Received buffer: {0}'.format(self.get_status()))

        if not self._srcpad.is_active():
            self._srcpad.set_active(True)
            self.add_pad(self._srcpad)

        gobject.idle_add(self._srcpad.push, buf)

    def do_get_property(self, prop):
        if prop.name == 'communicator':
            return self._ic

        elif prop.name == 'receiver-proxy':
            return str(self._receiver_proxy)

        elif prop.name == 'debug':
            return self._debug

    def do_set_property(self, prop, value):
        if prop.name == 'communicator':
            self._ic = value
            adapter = self._ic.createObjectAdapterWithEndpoints(
                'GstIceAdapter', 'default')

            adapter.activate()

            self._receiver_proxy = adapter.addWithUUID(self._receiver)

        elif prop.name == 'debug':
            self._debug = value

gobject.type_register(GstIceSrc)
