#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import time
import threading

import gtk
import gobject
import gext
import arconotes

# Quick access to this singletons
KB = gext.kb
MS = gext.ms

ARGOS_ROOT_SRC = "%s/pfc/src" % os.environ['HOME']

class NotesThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def execute(self, func, args):
        gobject.idle_add(func, *args)

    def run(self):
        print threading.currentThread()
        gtk.gdk.threads_init()
        gtk.main()

    def stop(self):
        gtk.main_quit()


def onPrepare():
    terminal = gext.launchApp("gnome-terminal --working-directory="
                              "%s/pyrenderapplet/RenderApplet" %
                              ARGOS_ROOT_SRC, quiet=True)
    KB.generateKeyEvents("icegridadmin --Ice.Config=/home/josel.segura/src/"
                         "method_ice/config_ice -u mauchly -p ylhcuam -e \""
                         "server start AXIS.161.67.106.120\"<Enter>")
    KB.generateKeyEvents("./GstreamerRender.py --Ice.Config=config<Enter>"
                         "<Alt><F9>")
    
    tp = gext.launchApp("tp --geometry=960x678+30+30", wait=0,
                        quiet=True)

    tp_window = gext.Window("Twin Panel")
    tp_window.waitTillExists()
    
    return [terminal]

def onRecording():
    notes = arconotes.Notes(sys.argv[1])

    nt = NotesThread()
    nt.start()

    tp_geometry = gext.Window("Twin Panel").getGeometry().next()

    MS.moveAbsTo(tp_geometry[2] + 911, tp_geometry[3] + 77)
    MS.leftClick()
    MS.moveRelTo(-64, 50)
    MS.leftClick()
    MS.moveRelTo(88, -47)
    MS.leftClick()
    
    # ASDA
    nt.execute(notes.nextNote, ())
    time.sleep(5)
    nt.execute(notes.nextNote, ())
    time.sleep(5)
    nt.execute(notes.hideNote, ())

    MS.moveRelTo(-250, 60)
    nt.execute(notes.nextNote, ())
    time.sleep(6)
    MS.leftDoubleClick()
    nt.execute(notes.nextNote, ())
    time.sleep(5)
    nt.execute(notes.hideNote, ())

    # Close TP
    MS.moveRelTo(258, -150)
    MS.leftClick()

    terminal = gext.launchApp("gnome-terminal --geometry=100x30+30+30 "
                              "--working-directory=%s/CliConfigurator" %
                              ARGOS_ROOT_SRC)

    nt.execute(notes.nextNote, ())
    time.sleep(4)
    nt.execute(notes.nextNote, ())

    KB.generateKeyEvents("./argos_config --Ice.Config=config "
                         "\"AxisMediaMMDeviceContainer -t @ AxisMediaServer\""
                         " \\<Enter>", mode=KB.MODE_NATURAL, wait=3)

    nt.execute(notes.nextNote, ())

    KB.generateKeyEvents(" \"GstreamerMediaRender -t:tcp -h 161.67.106.119 -p"
                         " 50001\"", mode=KB.MODE_NATURAL, wait=3)

    nt.execute(notes.nextNote, ())
    KB.generateKeyEvents("<Enter>")
    time.sleep(10)

    nt.stop()

def onFinish(processes=[]):
    # cierra la ventana de Gstreamer
    # win = gext.window.getFocusWindow()
    # KB.generateKeyEvents("<Alt><F4>")
    # win.waitTillNotExists()

    # # cierra la terminal
    # win = gext.window.getFocusWindow()
    # KB.generateKeyEvents("<Control>C<Control>D")
    # win.waitTillNotExists()

    # paramos el MediaServer y cerramos este terminal
    terminal = gext.launchApp("gnome-terminal")
    KB.generateKeyEvents("icegridadmin --Ice.Config=/home/josel.segura/src/"
                         "method_ice/config_ice -u mauchly -p ylhcuam -e \""
                         "server stop AXIS.161.67.106.120\"<Enter><Alt><F4>")

    import signal
    for p in processes:
        os.kill(p.pid, signal.SIGTERM)

    
def main():
    processes = onPrepare()
    record = gext.launchApp("recordmydesktop --width 1024 --height 768 -x 0 -y 0"
                            " --no-sound -o %s/video.ogv" % os.environ['HOME'])
    onRecording()
    import signal
    os.kill(record.pid, signal.SIGTERM)
    
    onFinish(processes)
    

main()
